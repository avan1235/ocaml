(* funkcja tworzaca funkcje permutujaca dany zbior  *)
(* w ll znajduja sie listy permutacji  *)
let buduj_permutacje ll =
  fun x ->
    let f (b, y) l = if b
                       then (b, y)
                       else(
                         let g (prev, (b, y)) a =
                           if b then (prev, (b, y))
                           else if x = prev then (a, (true, a))
                           else (a, (b, y))
                         in
                         match l with
                         | [] -> (b, y)
                         | _ -> List.fold_left g (List.hd (List.rev l), (b, y)) l |> snd
                       )
    in
    List.fold_left f (false, x) ll |> snd;;

let buduj_permutcje_lepiej ll =
  List.fold_left
    (fun f l ->
       match l with
       | [] -> f
       | _ ->
       List.fold_left
         (fun (f, prev) a ->
            ((fun x -> if x = prev
                       then a
                       else f x), a)
         ) (f, (List.hd (List.rev l))) l |> fst
    )
    (fun x -> x) ll;;

