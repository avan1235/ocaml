(* funkcje wyzszych rzedow na drzewach *)

(* definicja typu drzewa *)
type 'a tree = Node of 'a * 'a tree list;;


(* odpowiednik fold_list dla drzewa - przegladanie drzewa od dolu *)
(* funkcja f dostaje aktualna wartosc w Node oraz liste wynikow   *)
(* dla poddrzew danego drzewa i zwraca wartosc w Node             *)
let rec fold_tree f (Node(x, l)) =
  f x (List.map (fold_tree f) l);;


(* wyliczanie liczby wierzcholkow w drzewie *)
let size t =
  fold_tree (fun _ l -> (List.fold_left (fun a x -> a + x) 1 l)) t;;


(* wyznaczanie glebokosci drzewa *)
let depth t =
  fold_tree (fun _ l -> (List.fold_left max 0 l)) t;;


(* suma elementow drzewa *)
let sum t =
  fold_tree (fun x l -> (List.fold_left (+) x l)) t;;


(* funkcja map dla drzewa ktora jako wynik zwraca drzewo o         *)
(* niezmienionej strukturze, ale o zmienionych przez f wartosciach *)
let map f t =
  fold_tree (fun x l -> (Node(f x, l))) t;;


(* definicja typu drzewa binarnego *)
type 'a tree =
  | Leaf
  | Node of 'a tree * 'a * 'a tree;;


(* odpowiednik fold_list dla  drzewa binarnego - od dolu przetwarza liscie  *)
(* przekazuje je do gory, tak by Nody mogly je przetworzyc z wykorzystaniem *)
(* wartosci w Node [a - akumulator startowy dla liscia]                     *)
let rec fold_bin_tree f a t =
  match t with
  | Leaf -> a
  | Node(l, x, r) -> f x (fold_bin_tree f a l) (fold_bin_tree f a r);;


(* suma wartosci w drzewie binarnym *)
let bin_sum t =
  fold_bin_tree (fun x al ap -> x + al + ap) 0 t;;


(* czy drzewo jest drzewem bst - czy dla kazdego Noda zachodzi wlasnosc ze   *)
(* na lewo od niego znajduja sie tylko wartosci od niego wieksze, a na prawo *)
(* tylko wartosci wieksze                                                    *)
(* musimy patrzec czy lewe poddrzewo bylo bst, czy prawe drzewo bylo bst     *)
(* czy maksymalna wartosc jaka pojawila sie w lewym poddrzewie jest mniejsza *)
(* od wartosci w Node i analogicznie dla prawego poddrzewa                   *)

let is_bst t =
  let f x (vl, bl) (vr, br) = (* value_left, bool_left itd. *)
    let b = bl && br in
    let (b, cmin) =
      match vl with
      | None -> (b, x)
      | Some(lmin, lmax) -> (b && lmax <= x, lmin)
    in
    let (b, cmax) =
      match vr with
      | None -> (b, x)
      | Some(rmin, rmax) -> (b && rmin >= x, rmax)
    in
    (Some(cmin, cmax), b)
  in
  fold_bin_tree f (None, true) t |> snd;;


(* wersja krotsza sprawdzania czy drzewo jest bst z wykorzystaniem funkcji jako *)
(* akumulatorow, ktore zwracaja minimum lub maskimum *)

let id x = x;;

let is_bst_better t =
  fold_bin_tree (fun x (bl, (fll, flr)) (br, (frl, frr)) ->
      (bl && br && x = flr x && x = frl x, ((min (fll x)), (max (frr x)))))
                (true, (id, id))
                t |> fst;;

(* przechodzenie drzewa binarnego w kolejnosci infiksowej *)

(*
             1
            / \
           2   5
          /   / \
         4   7   8

 Prawidlowym wynikiem jest 4, 2, 1, 7, 5, 8
*)

let infix t =
  let rec scan t a =
    match t with
    | Leaf -> a
    | Node(l, x, r) -> scan l (x::(scan r a))
  in scan t [];;


(* przechodzenie drzewa w kolejnosci infiksowej z wykorzytaniem jako akumulatora funkcji *)
let infix t =
  (fold_bin_tree (fun x fl fr ->
      fun a -> fl (x::(fr a)))
    (fun a -> a)
    t) (* w ten sposob mamy funkcje i musimy ja do czegos zastosowac - do wstepnego a *)
    [];;

(* funkcja fit ktora jest fit: int -> int list -> int
   daje min |c+x_i+x_j| dla wywolania fit c [x_1, x_2, ..., x_m] *)
(* wykorzystana technika iterowania po liscie *)

let fit c l =
  let rec scan ll rl a =  (* ll - lewa lista, rl - prawa lista (odwrocona lista ll)*)
    match ll, rl with
    | [],_ | _,[] -> a
    | hl::tl, hr::tr -> let s = c + hr + hr in
                        if s = 0 then 0
                        else(
                        if s > 0 then scan ll tr (min a s)
                        else scan tl rl (min a (-s))
                      )
  in
  scan l (List.rev l) (abs (c + 2 * (List.hd l)));;


(* funkcja dolek - taki index 1<i<n ze (max(x_1, x_2, ..., x_i-1)) > x_i < (max(x_i+1, x_i+2, ...)) 
glebokos dolka to minimum z roznicy tych wartosc 
   znalezc maksymalna glebokosc dolka *)

(*
utworzymy listy 
[x1, max(x1,x2), ... max(x1, ..., xn)]
[max(x1, ..., xn), ..., max(x_n-1, x_n), x_n]
*)

let dolek l =
  if l = [] then 0
  else(
    let max_build l = List.fold_left (fun a x -> ((max (List.hd a) x)::a)) ([List.hd l]) (List.tl l) in
    let max_pref = List.rev (max_build l) 
    and max_suf = max_build (List.rev l) in
    let f ((m_pref, m_suf), max_g) x = ((List.tl m_pref, List.tl m_suf), max (max_g)
                                          (min ((List.hd m_pref) - x) ((List.hd m_suf) - x))
                                       ) in
    List.fold_left f
                   ((max_pref, max_suf |> List.tl |> List.tl), 0)
                   (l |> List.tl |> List.rev |> List.tl |> List.rev)
                   |> snd
    );;


(* levels - uciete kolejne poziomy drzewa w liscie list *)
let levels t =
  let rec scan t a =
    match t with
    | Leaf -> a
    | Node(l, x, r) -> let a = if a = [] then [[]] else a in
                       (x::(List.hd a))::(scan l (scan r (List.tl a)))
  in
  scan t [[]];;


(* sposob dzialania jest nastepujacy:                                                     *)
(* naszym wynikiem bedzie kazdorazowo funkcja - funkcja ktora jak dostanie akumulator     *)
(* to dolaczy do niego gornego Noda, i w ten sposob utworzona liste dolaczy do listy list *)
(* wartosci na kolejnych poziomach *)

let levels_better t =
  (fold_bin_tree (fun x fl fr ->
      fun a -> let a = if a = [] then [[]] else a
               in
               (x::(List.hd a)::(fl (fr (List.tl a))))
    )
    (fun a -> a)
    t) [];;


(* prefiksy listy konczace sie zerem posortowane wedlug dlugosci *)

let prefiksy l =
  List.fold_left (fun (acc_l, al)  x -> if x = 0
                                        then (x::acc_l, (x::acc_l)::al)
                                        else  (x::acc_l, al)) ([], [[]]) l
  |> snd
  |> List.filter (fun x -> x <> [])
  |> List.map (fun l -> List.rev l)
  |> List.rev;;

(* dzilenie listy na fragmenty monotoniczne *)

type mon = Rosnie | Maleje | Stala | Poczatek;;

let monotons l =
  match l with
  | [] -> 0
  | [x] -> 1
  | hd::tl ->
    let mon_type a l =
      if a = l then Stala
      else if a > l then Rosnie
      else Maleje
    in
    List.fold_left (fun ((acc, lt), lv) x -> if lt = Poczatek then ((acc, (mon_type x lv)), x)
                     else if (mon_type x lv) = Stala then ((acc, lt), x)
                     else if (mon_type x lv) = lt then ((acc, (mon_type x lv)), x)
                     else ((acc+1, (mon_type x lv)), x))
      ((0, Poczatek), hd) tl |> fst |> fst;;


(* procedura skosnosc przeksztalca dane drzewo binarne w taki sposob, by jego skosnosc byla jak najmniejsza *)
(* za kazdym razem sprawdza minimalna skosnosc w danym Node i wten sposob konstruje nowe drzewo *)
let skosnosc t =
  fold_bin_tree (fun x (sl, tl) (sr, tr) ->
      let max1 = (max (2*sr) (2*sl+1))
      and max2 = (max (2*sl) (2*sr+1)) in
      if max1 < max2
      then (max1, Node(tr, x, tl))
      else (max2, Node(tl, x, tr)))
    (0, Leaf)
    t |> snd;;
