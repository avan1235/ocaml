(* slupki - na wejsciu lista slupkow [x1;x2;x3;...]  *)
(* jako wynik liczba uderzen dla wszystkich slupkow  *)
let slupki l =
  let l = List.sort compare l in
  let median = List.nth l (List.length l / 2) in
  List.fold_left (fun a x -> a + (abs (x - median))) 0 l;;


(* elementy: dla dwoch list [x0, x1, ..., xn-1] i [y0, y1, ..., ym-1] *)
(* dostajemy liste [x_y0, x_y1, ..., x_ym-1]                          *)

let element lx ly = (* zlozonosc m*n *)
  List.map (List.nth lx) ly;;

(* rozwiazanie polega na                                              *)
(* posortowaniu tablicy z indeksami, ale zapamietujac poczatkowe      *)
(* polozenia y w parach                                               *)
let element lx ly =
  let rec scan i lx a lyj =
    match lyj with
    | [] -> a
    | (y, j)::tyj ->
      if y = i
      then scan i lx ((j, List.hd lx)::a) tyj
      else scan (i+1) (List.tl lx) a lyj
  in
  ly |> List.mapi (fun j y -> (y, j)) |> List.sort compare |> scan 0 lx [] |> List.sort compare |> List.map snd

(* dla danej listy sprawdzamy, czy dla danej listy [x1, ..., xn] istnieja xi, xj, xk takie ze xi + xj > xk *)
let trojki l =
  match l with
  | [] | _::[] | _::_::[] -> false
  | _ ->
    let l = List.sort compare l in
    snd (List.fold_left (fun ((fs, sn), res) x ->
        match fs, sn with
        | None, None -> ((None, Some x), res)
        | None, Some(sn) -> ((Some sn, Some x), res)
        | Some(fs), Some(sn) -> ((Some sn, Some x), fs + sn > x || res)
                   ) ((None, None), false) l)


(* rozwiazanie z wykorzystaniem liczb fibbonacciego*)


(* dla listy [x1, ..., xn] i stalej r zwrocic takie c ze |{i: |xi - c| <= r}| jest najwieksza *)
let przedzial l r =
  let l = List.sort compare l in
  let rec pom li lj k best =
    match li, lj with
    | xi::ti, xj::tj ->
      if xj - xi <= 2*r
      then pom li tj (k+1) (max best (k, ((xi+xj)/2)))
      else pom ti lj (k-1) best
    | _ -> best
  in
  pom l l 0 (0, List.hd l) |> snd;;
