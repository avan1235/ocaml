let ostatki ll =
  List.filter (fun l -> l <> []) ll |> List.fold_left (fun a l -> List.hd (List.rev l)::a) [] |> List.rev;;

ostatki [[3;4;5]; [1;5;6]; []; [2]];;

let podciag a =
  let mn = ref (-2) in
  let n = Array.length a in
  for i = 0 to n-1 do
    if a.(i) = !mn + 2 then mn := !mn + 2;
  done;
  let res = ref [] in
  for i = n-1 downto 0 do
    if a.(i) = !mn then (res := i::!res; mn := !mn - 2);
  done;
  !res;;

podciag [|3;5;0;2;1;2;5;6;8;4;6;9;7;8;0;3;4;2;6;5|];;

let pref a =
  let n = Array.length a in
  let p = Array.make (n+1) 0 in
  let pj = ref 0 in
  for i = 2 to n do
    while (!pj > 0) && (a.(!pj) <> a.(i-1)) do
      pj := p.(!pj)
    done;
    if a.(!pj) = a.(i-1) then pj := !pj + 1;
    p.(i) <- !pj
  done;
  p;;

let s =  [|'k'; 'a'; 'j'; 'a'; 'k'; 'a'; 'd'; 'l'; 'o'|];;

let prepalindrom a =
  let n = Array.length a in
  let aa = Array.init (2*n) (fun i -> if i < n then a.(i) else a.(2*n-1-i)) in
  (pref aa).(2*n);;

prepalindrom s;;

let string_to_array s =
  Array.init (String.length s) (fun i -> String.get s i);;

let dable s =
  let p = pref (string_to_array s) in
  let w = ref 0 in
  for i = 1 to Array.length p - 1 do
    if 2*p.(i) = i then w := max !w p.(i);
  done;
  !w;;

dable "kokokla";;

pref (string_to_array "okreokreokreokrehjuk");;

let powt s =
  let a = string_to_array s in
  let p = pref a in
  let mxindx = ref 0 in
  for i = 1 to Array.length a do
    if p.(i) > p.(!mxindx) then mxindx := i;
  done;
  while p.(!mxindx) mod (p.(!mxindx) - p.(p.(!mxindx))) > 0 do
    mxindx := !mxindx - 1
  done;
  (p.(!mxindx) - p.(p.(!mxindx)));;


powt "okreookreookreookrehjuk";;

let ciag n =
  let dp = Array.make (n+1) (0, []) in
  let lista_fib k =
    let w = ref [] in
    let i = ref 1 in
    let j = ref 1 in
    while !i + !j <= k do(
      let t = !j in
      j := !j + !i;
      i := t;
      w := !j::!w;
    ) done;
    !w
  in
  let lst = lista_fib n in 
  for l = 1 to n do(
    dp.(l) <- (fst dp.(l-1) + 1, l::(snd dp.(l-1)));
    List.iter (fun f -> if l mod f = 0 then dp.(l) <-
                    (if fst dp.(l) > fst dp.(l/f)
                     then (fst dp.(l/f) + 1, f::(snd dp.(l/f)))
                     else (dp.(l)))) lst;
  ) done; 
  snd dp.(n);;

ciag 42;;


type symbol =
  | Liczba of int
  | Plus | Minus | Razy | Podziel
  | Nawias_Otw | Nawias_Zam;;


let znajdz a n =
  let r = Array.length a in
  let h = ref (int_of_float ((log (float_of_int r))/.(log (2.)))) in
  let i = ref 0 in
  while !i < r && a.(!i) <> n do
    print_int !i;print_newline ();
    if a.(!i) > n then (incr i; h := max 1 (!h - 1);) else
    if a.(!i) < n then (i := !i + !h; h := max 1 (!h - 1););
  done;
  if !i >= r then -1 else !i;;

znajdz [|5;3;1;2;4;7;6|] 6;;
znajdz [|20;10;5;2;7;15;30;25;34|] 25;;


let rec kalkulator l =
  let rec scan al acc =
    match al with
    | [] -> acc
    | hd::tl ->
      match hd with
      | Nawias_Otw -> (fun _ -> acc (kalkulator tl))
      | Nawias_Zam -> scan tl acc
      | Liczba (n) -> scan tl (fun _ -> acc n)
      | Plus -> scan tl (fun x -> x + acc 0)
      | Minus -> scan tl (fun x -> (acc 0) - x)
      | Razy -> scan tl (fun x -> x * (acc 0))
      | Podziel -> scan tl (fun x -> (acc 0) / x)
  in
  (scan l (fun x -> x)) 0;;

kalkulator [Liczba 2; Razy; Nawias_Otw; Liczba 5; Plus; Liczba 4; Razy; Liczba 2; Plus; Liczba 3; Nawias_Zam];;

type kolor = Czerwona | Zielona;;

let kulki l =
  let ile_cz = List.fold_left (fun a v -> if v = Czerwona then 1+a else a) 0 l in
  let ile_ziel = List.fold_left (fun a v -> if v = Zielona then 1+a else a) 0 l in
  if ile_cz = 0 || ile_ziel = 0 then 0 else(
    let mniej = min ile_ziel ile_cz in
    let dlugosc_naprzemienneie = 2*mniej in
    let (_, cz, z) = List.fold_left (fun (i, cz, ziel) k -> if (i < dlugosc_naprzemienneie && i mod 2 = 0 && k = Czerwona) then (i+1, cz, ziel+1) else
                                                              if (i < dlugosc_naprzemienneie && i mod 2 = 1 && k = Zielona) then (i+1, cz+1, ziel) else (i+1, cz, ziel)) (0,0,0) l in
    max cz z
  );;

kulki [Czerwona; Zielona; Czerwona; Czerwona; Zielona; Zielona; Zielona; Zielona];;

let autobusy p a =
  let n = Array.length a in
  let pas_raz = Array.make n 0 in
  let czas = ref 0 in
  let ost_czas = ref 0 in
  for i = 0 to n-1 do
    czas := a.(i);
    for j = !ost_czas to !czas do
      pas_raz.(i) <- pas_raz.(i) + p.(j)
    done;
    ost_czas := a.(i) + 1;
  done;
  let ile_aut = n in
  let sum = ref 0 in
  for i = 0 to n-1 do
    sum := !sum + pas_raz.(i)
  done;
  let min_poj = (!sum/ile_aut) in
  let wynik = ref min_poj in
  let czy_dadza_rade poj =
    let nadwyzka = ref 0 in
    for i = 0 to ile_aut-1 do(
      if pas_raz.(i) > poj then nadwyzka := !nadwyzka + pas_raz.(i) - poj else
      if pas_raz.(i) < poj && !nadwyzka > 0 then nadwyzka := max 0 (!nadwyzka - (poj - pas_raz.(i)));
    )
    done;
    !nadwyzka <= 0
  in
  let mam_wynik = ref false in
  while not !mam_wynik do
    mam_wynik := czy_dadza_rade !wynik;
    if not !mam_wynik then wynik := !wynik + 1;
  done;
  !wynik;;

autobusy [|1;0;2;1;2;0;1;2;6;0;6;0|] [|2;6;8;11|];;
