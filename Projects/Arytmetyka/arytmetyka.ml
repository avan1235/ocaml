(****************************************************************************************************)
(***************************************** MODUŁ ARYTMETYKA *****************************************)
(****************************************************************************************************)

(*--------------------------------------------------------------------------------------------------*)
(* Autor: Maciej Procyk                                                                             *)
(* Recenzujaca: Alicja Ziarko                                                                       *)
(*--------------------------------------------------------------------------------------------------*)

(*--------------------------------------------------------------------------------------------------*)
(* Wartosc reprezentowana jako przedzial liczb                                                      *)
(* Istnieja dwa mozliwe przypadki jest to albo przedzial                                            *)
(* w postaci [left, right] badz tez w postaci (-inf, left]U[right, +inf)                            *)
(* gdzie w pierwszym przypadku left moze byc neg_infinity a right infinity                          *)
(* w przypadku otrzymywania wynikow nan po operacjach na wartosciach                                *)
(* do odpowiedniego rekordu wpisywana jest wartosc nan i istnieje funkcja                           *)
(* pomocnicza do sprawdzania czy wartosc zawiera nieokreslonosc                                     *)
(*--------------------------------------------------------------------------------------------------*)


(****************************************************************************************************)
(****************************************** DEFINICJA TYPU ******************************************)
(****************************************************************************************************)

type wartosc = {left  : float;
                right : float; (* dla postaci wartosci [left, right] -> is_in = true                *)
                is_in : bool}  (* natomiast dla (-inf, left]U[right, +inf) -> is_in = false         *)
                                

(****************************************************************************************************)
(**************************************** FUNKCJE POMOCNICZE ****************************************)
(****************************************************************************************************)

(* Sprawdzanie czy wartosci zawieraja nan lub infinity *)
let flt_nan x =
  compare x nan = 0

let flt_any_inf x =
  (((compare x infinity) = 0) || ((compare x neg_infinity) = 0))
                    
let wartosc_nan (w : wartosc) =
  (flt_nan w.left || flt_nan w.right)

let any_wartosc_nan (w1 : wartosc) (w2 : wartosc) =
  (wartosc_nan w1 || wartosc_nan w2)

(* Tworzenie wyniku wartosc nan *)
let ret_nan =
  {left  = nan;
   right = nan;
   is_in = true}

(* Funkcje do debugowania przebiegu programu w graficzny sposob *)
let string_of_wartosc (w : wartosc) =
  if wartosc_nan w
  then "nan"
  else(
    if w.is_in
    then "[" ^ string_of_float w.left ^ ", " ^ string_of_float w.right ^ "]"
    else "[-inf, " ^ string_of_float w.left ^ "]U[" ^ string_of_float w.right ^ ", inf]"
  )

let print_wartosc (w : wartosc) =
  print_string (string_of_wartosc w)

(* Funkcja dla x = -0. daje 0., inaczej x | pomocna przy zapisywaniu wynikow dzialan *)
let norm_zero x =
  if x = (-0.)
  then 0.
  else x


(****************************************************************************************************)
(******************************************* KONSTRUKTORY *******************************************)
(****************************************************************************************************)

(* wartosc_dokladnosc x p = x +/- p% *)
(* war.pocz.: p > 0                  *)
let wartosc_dokladnosc x p =
  if p < 0.
  then failwith "p < 0 has been given"
  else
    let c = p /. 100. *. abs_float x
    in {left  = (norm_zero x) -. c;
        right = (norm_zero x) +. c;
        is_in = true}

(* wartosc_od_do x y = [x,y]         *)
(* war.pocz.: x <= y                 *)
let wartosc_od_do x y =
  if x <= y
  then {left  = (norm_zero x);
        right = (norm_zero y);
        is_in = true}
  else if (flt_nan x) || (flt_nan y)
  then {left  = nan;
        right = nan;
        is_in = true}
  else failwith "bad order of given parameters"

(* wartosc_dokladna x = [x,x]        *)
let wartosc_dokladna x =
  {left  = (norm_zero x);
   right = (norm_zero x);
   is_in = true}

(* wartosc_dopelnienie x y = (-inf, x]U[y, +inf) *)    
let wartosc_dopelnienie x y =
  if x <= y
  then {left = (norm_zero x);
        right = (norm_zero y);
        is_in = false}
  else failwith "bad order of given parameters"

(* Sprawdzanie czy dwie warosci sa te same - do testow *)
let compare_wartosc (w1 : wartosc) (w2 : wartosc) =
  if (w1.left = w2.left) && (w1.right = w2.right) && (w1.is_in = w2.is_in)
  then true
  else false
         
               
(****************************************************************************************************)
(********************************************* FUNKCJE **********************************************)
(****************************************************************************************************)

(* in_wartosc w x = x \in w *)
let in_wartosc (w : wartosc) x =
  (* pomocnicza funkcja check sprawdza czy v \in [l; r]                                             *)
  (* wznosi wyjatek w momencie gdy probujemy operowac na nieoznaczonosci                            *)
  let check l r v =
    if ((flt_nan l) || (flt_nan r) || (flt_nan v))
    then failwith "nan value given as parameter"
    else(
      if ((l < v) && (v < r)) || (v = l) || (v = r)
      then true
      else false
    ) in                        (* 2 przypadki: podwojny i pojedynczy przedzial *)
  match w.is_in with
  | true -> check w.left w.right x
  | false -> ((check neg_infinity w.left x) || (check w.right infinity x))

(* min_wartosc w = najmniejsza mozliwa wartoscD w,   *)
(* lub neg_infinity jesli brak dolnego ograniczenia.*)
let min_wartosc (w : wartosc) =
  match w.is_in with
  | true -> w.left
  | false -> neg_infinity (* obejmuje przypadek nieoznaczonych koncow przedzialu *)

(* max_wartosc w = najwieksza mozliwa wartosc w,    *)
(* lub infinity jesli brak gornego ograniczenia     *)
let max_wartosc (w : wartosc) =
  match w.is_in with
  | true -> w.right
  | false -> infinity     (* obejmuje przypadek nieoznaczonych koncow przedzialu *)

(* srodek przedzialu od min_wartosc do max_wartosc, *)
(* lub nan jesli min i max_wartosc nie sa okreslone *)
let sr_wartosc (w : wartosc) =
  if (w.is_in = false) || (w.left = neg_infinity && w.right = infinity)
  then nan
  else (w.left +. w.right) /. 2.

(****************************************************************************************************)
(************************************* OPERACJE NA WARTOSCIACH **************************************)
(****************************************************************************************************)


(* Funkcja dodaje brzegi przedzialow i patrzy czy nie powstaje zbior liczb rzeczywistych poprzez    *)
(* nakladanie sie dwoch przedzialow                                                                 *)
let plus (w1 : wartosc) (w2 : wartosc) =
  if any_wartosc_nan w1 w2
  then ret_nan
  else(
    match w1.is_in, w2.is_in with
    | true, true -> wartosc_od_do (w1.left +. w2.left) (w1.right +. w2.right)
    | false, false -> wartosc_od_do neg_infinity infinity
    | false, true -> let l = w1.left +. w2.right
                     and r = w1.right +. w2.left in
                     if l < r
                     then wartosc_dopelnienie l r
                     else wartosc_od_do neg_infinity infinity
    | true, false -> let l = w1.right +. w2.left
                     and r = w1.left +. w2.right in
                     if l < r
                     then wartosc_dopelnienie l r
                     else wartosc_od_do neg_infinity infinity                                                             
  )

(* W odejmowaniu wykorzystany fakt ze odejmowanie to dodawanie liczby przeciwnej                    *)
let minus (w1 : wartosc) (w2 : wartosc) =  
  let temp_w = if w2.is_in
               then wartosc_od_do ((-1.) *. w2.right) ((-1.) *. w2.left)
               else wartosc_dopelnienie ((-1.) *. w2.right) ((-1.) *. w2.left)
  in
  plus w1 temp_w
        
(*--------------------------------------------------------------------------------------------------*)
(* Funkcje pomocnicze w mnozeniu i dzieleniu przedzialow                                            *)
(* Sprawdzaja one czy przedzial jest caly powyzej zera, ponizej zera                                *)
(* czy raczej zawiera zero ale nie na brzegach przedzialu lub zawiera zero gdziekolwiek             *)
(*--------------------------------------------------------------------------------------------------*)

(* czy przedzial zawiera zero ale nie na krancach *)
let has_zero (w : wartosc) = 
  (in_wartosc w 0.) && (w.left <> 0.) && (w.right <> 0.)

(* wartosc moze miec zero na brzegach *)
let has_zero_in (w : wartosc) =            
  in_wartosc w 0.

(* czy przedział jest is_in i od neg_inf do inf -> [-inf, inf] *)
let is_all (w : wartosc) =
  if w.is_in && (w.left = neg_infinity) && (w.right = infinity)
  then true
  else false
             
(* czy caly przedzial wartosci powyzej zera *)
let is_up_zero (w : wartosc) =
  if w.is_in = false
  then failwith "cannot check if interval is upper zero if it is (-inf,a]U[b,inf)"
  else(
    if ((w.right > 0.) && (w.left > 0.))
    then true
    else false
  )

(* czy caly przedzial wartosci ponizej zera *)
let is_down_zero (w : wartosc) =
  if w.is_in = false
  then failwith "cannot check if interval is under zero if it is (-inf,a]U[b,inf)"
  else(
    if ((w.left < 0.) && (w.right < 0.))
    then true
    else false
  )

(* czy przedzial wartosci zaczyna sie od zera *)
let starts_with_zero (w : wartosc) =
  if w.is_in = false
  then failwith "cannot check if interval starts with zero if it is (-inf,a]U[b,inf)"
  else(
    if w.left = 0.
    then true
    else false
  )

(* czy przedzial wartosci konczy sie zerem *)
let ends_with_zero (w : wartosc) =
  if w.is_in = false
  then failwith "cannot check if interval ends with zero if it is (-inf,a]U[b,inf)"
  else(
    if w.right = 0.
    then true
    else false
  )

(* czy przedzial jest przedzialem [0,0] *)
let is_zero (w : wartosc) =
  (ends_with_zero w) && (starts_with_zero w)

                          
let rec razy (w1 : wartosc) (w2 : wartosc) =
  if any_wartosc_nan w1 w2
  then ret_nan
  else(
    (* iloczyny par koncow przedzialow *)
    let l1l2 = w1.left *. w2.left
    and l1r2 = w1.left *. w2.right
    and r1l2 = w1.right *. w2.left
    and r1r2 = w1.right *. w2.right
    in
    if (flt_nan l1l2) && (flt_nan l1r2) && (flt_nan r1l2) && (flt_nan r1r2)
    then wartosc_dokladna 0. (* obejmuje przypadek [0,0]*(-inf,inf) *)
    else ( (* jezeli nie sa wszystkie na raz nan to operujemy na liczbach, nie nioznaczonosciach    *)
      match w1.is_in, w2.is_in with
      | true, true -> (* dwie funkcje inf4 i sup4 zwracaja najwiekszy i najmniejszy z argumentow    *)
                      (* ale biora pod uwage jedynie te argumenty, ktore nie maja wartosci nan      *)       
                      let check check_f x1 x2 x3 x4 =           (* funkcja porownujaca x1 x2 x3x x4 *)
                        let look a b =
                          match flt_nan a, flt_nan b with
                          | false, false -> check_f a b
                          | false, true -> a
                          | true, false -> b
                          | true, true -> nan
                        in
                        look (look x1 x2) (look x3 x4)
                      in
                      let inf4 x1 x2 x3 x4 =
                        check min x1 x2 x3 x4 
                      and sup4 x1 x2 x3 x4 =
                        check max x1 x2 x3 x4
                      in
                      wartosc_od_do (inf4 l1l2 l1r2 r1l2 r1r2) (sup4 l1l2 l1r2 r1l2 r1r2)                    
      | false, false -> (* zadne z pol rekordow na pewno nie jest nieskonczonoscia                  *)
                        (* -> brak nan w wynikach -> mozna uzyc min i max                           *)
                        let mx = max l1r2 r1l2
                        and mn = min l1l2 r1r2 in
                        if (has_zero w1) || (has_zero w2) || (mx = mn) 
                        then wartosc_od_do neg_infinity infinity (* czy w1 lub w2 posiada dowolnie mala wartosc *)
                        else wartosc_dopelnienie mx mn                       
      | true, false -> (* mnozenie przedzialow w1*w2 <=> [a,b]*(-inf,c]U[d,inf) *)
                       if has_zero w1 
                       then wartosc_od_do neg_infinity infinity
                       else(
                         (* Funkcja liczaca dla przedzialow [a,b] i [c,d] ich sume [a,b]U[c,d]      *)
                         (* Moze zostac wykorzystana do dowolnego typu przedzialow (is_in dowolne)  *)
                         (* Przy wejsciu do sum dbamy o to, by w1.left <= w2.left                   *)
                         (* jak nie jest tak, to wywolujemy funkcje z zamienionymi argumentami      *)
                         let rec sum (w1 : wartosc) (w2 : wartosc) =               
                           if (any_wartosc_nan w1 w2) || (w1.is_in = false) || (w2.is_in = false)
                           then failwith "cannot sum two intervals which are nan or not 'in' type"
                           else if w1.left > w2.left
                           then sum w2 w1
                           else(
                             if (w2.left <= w1.right) && (w2.right >= w1.right)
                             then wartosc_od_do w1.left w2.right
                             else if (w2.left <= w1.right) && (w2.right <= w1.right)
                             then wartosc_od_do w1.left w1.right
                             else wartosc_dopelnienie w1.right w2.left (* gdy przedzialy nie maja czesci wspolnej *)
                           )
                         in (* zauwazmy ze [a,b] * [-inf, c]U[d, inf] = ([a,b] * [-inf, c]) U ([a,b] * [d, inf])  *)
                         sum (razy w1 (wartosc_od_do neg_infinity w2.left))
                             (razy w1 (wartosc_od_do w2.right infinity))                           
                       )                                           
      | false, true -> razy w2 w1
    )
  )

(* Operacje dzielenia implementujemy jako pomnozenie przez wartosc odwrotna do danego przedzialu              *)
let podzielic (w1 : wartosc) (w2 : wartosc) =
  if (any_wartosc_nan w1 w2) || (w2.is_in && (is_zero w2)) (* skrajne przypadki z wynikami nan                *)
  then ret_nan
  else if (is_all w1) && (is_all w2) (* przypadek obu przedzialow rownych zbiorom liczb rzecywistych          *)
  then wartosc_od_do neg_infinity infinity
  else(
    let one_over (w : wartosc) =                                    (* na wejsciu 'w' nie jest na pewno [0,0] *)
      if w.is_in = true
      then(                                                                   (* przypadki dla 'w' typu [a,b] *)
        if is_down_zero w                                                                   (* kiedy a < b < 0*)
        then wartosc_od_do (1. /. w.right) (1. /. w.left)
        (*----------------------------------------------------------------------------------------------------*)
        else if ends_with_zero w                                                           (* kiedy a < b = 0 *)
        then wartosc_od_do neg_infinity (1. /. w.left)
        (*----------------------------------------------------------------------------------------------------*)
        else if starts_with_zero w                                                         (* kiedy 0 = a < b *)
        then wartosc_od_do (1. /. w.right) (infinity)
        (*----------------------------------------------------------------------------------------------------*)
        else if is_up_zero w                                                               (* kiedy 0 < a < b *)
        then wartosc_od_do (1. /. w.right) (1. /. w.left)
        (*----------------------------------------------------------------------------------------------------*)
        else wartosc_dopelnienie (1. /. w.left) (1. /. w.right) (* przypadek przedzialu [a,b] t. ze a < 0 < b *)
      )
      else( (* if w.is_in = false *)                               (* przypadki dla 'w' typu (-inf,a]U[b,inf) *)
        if w.right = 0.                                                                        (* kiedy b = 0 *)
        then wartosc_od_do (1. /. w.left) infinity
        (*----------------------------------------------------------------------------------------------------*)
        else if w.left = 0.                                                                    (* kiedy a = 0 *)
        then wartosc_od_do neg_infinity (1. /. w.right)
        (*----------------------------------------------------------------------------------------------------*)
        else if (w.left < 0.) && (w.right > 0.)                                        (* kiedy a < 0 i b > 0 *)
        then wartosc_od_do (1. /. w.left) (1. /. w.right)
        (*----------------------------------------------------------------------------------------------------*)
        else wartosc_dopelnienie (1. /. w.right) (1. /. w.left)       (* (a < 0 && b < 0) || (a > 0 && b > 0) *)      
      )
    in                         (* zauwazmy ze [a,b] / [c,d] = [a,b] * [1,1] / [c,d] = [a,b] * ([1,1] / [c,d]) *)
    razy w1 (one_over w2)
  )


        

(****************************************************************************************************)
(********************************************** TESTY ***********************************************)
(****************************************************************************************************)

(* Odkomentowac w razie potrzeby testow 
        
(* Funkcje pomocnicze do testowania wynikow dzialan *)
(* w sposob czytelny mowia ktore dzialanie jest bledne *)
let test_wynik nr (w : wartosc) (result : string) =
  if (compare (string_of_wartosc w) result) = 0
  then print_string ("TEST " ^ (string_of_int nr) ^ " OK")
  else print_string ("TEST " ^ (string_of_int nr) ^ " ERROR: wartosc : " ^ (string_of_wartosc w) ^ (", what should be: " ^ result));;

let test_float nr (returned : float) (result : float) =
  if ((abs_float (returned -. result)) <= 0.00000001) || (compare returned result) = 0
  then print_string ("TEST " ^ (string_of_int nr) ^ " OK")
  else print_string ("TEST " ^ (string_of_int nr) ^ " ERROR: returned : " ^ (string_of_float returned) ^ (", what should be: " ^ (string_of_float result)));;

let test_in_wartosc nr (returned : bool) (result : bool) =
  if (returned || result) && not (returned && result)
  then print_string ("TEST " ^ (string_of_int nr) ^ " ERROR: returned : " ^ (string_of_bool returned) ^ (", what should be: " ^ (string_of_bool result)))
  else print_string ("TEST " ^ (string_of_int nr) ^ " OK");;  

(*--------------------------------------------------------------------------------------------------*)

(* TESTY OPERACJI *)
test_wynik 1 (razy (wartosc_od_do (neg_infinity) (-2.)) (wartosc_od_do 3. infinity)) "[-inf, -6.]";;
test_wynik 2 (podzielic (wartosc_dokladna 3.0) (wartosc_od_do (-1.0) 1.0)) "[-inf, -3.]U[3., inf]";;
test_wynik 3 (plus (podzielic (wartosc_dokladna 1.0) (wartosc_od_do (-1.0) 1.0)) (wartosc_dokladna 3.0)) "[-inf, 2.]U[4., inf]";;
test_wynik 4 (podzielic (wartosc_od_do 0. 1.) (wartosc_od_do 0. 1.)) "[0., inf]";;
test_wynik 5 (podzielic (wartosc_dokladna 1.) (wartosc_od_do (-1.) (5.))) "[-inf, -1.]U[0.2, inf]";;
test_wynik 6 (podzielic (wartosc_od_do 3. 5.) (wartosc_dokladna (-0.))) "nan";;
test_wynik 7 (razy (wartosc_dopelnienie 2. 4.) (wartosc_od_do (-2.) (-1.))) "[-inf, inf]";;
test_wynik 8 (minus (podzielic (podzielic (wartosc_od_do 3. 7.) (wartosc_od_do (-2.) 5.)) (wartosc_od_do (-2.) 5.)) (wartosc_dokladna 3.)) "[-inf, -3.3]U[-2.88, inf]";;
test_wynik 9 (razy (wartosc_dokladna 0.) (wartosc_od_do neg_infinity infinity)) "[0., 0.]";;
test_wynik 10 (podzielic (wartosc_dopelnienie 3. 5.) (wartosc_od_do 0. (-0.))) "nan";;
test_wynik 11 (podzielic (wartosc_od_do 1. 2.) (wartosc_od_do 4. 8.)) "[0.125, 0.5]";;
test_wynik 12 (podzielic (wartosc_dokladna 1.0) (plus (podzielic (wartosc_dokladna 1.0) (wartosc_od_do (-1.0) 1.0)) (wartosc_dokladna 3.0))) "[-inf, 0.25]U[0.5, inf]";;
test_wynik 13 (razy (wartosc_dopelnienie (-5.) (3.)) (wartosc_dopelnienie (-2.) (1.))) "[-inf, -5.]U[3., inf]";;
test_wynik 14 (razy (wartosc_dopelnienie (-2.) (-1.)) (wartosc_od_do 3. 4.)) "[-inf, -6.]U[-4., inf]";;
test_wynik 15 (minus (wartosc_dopelnienie 2. 4.) (wartosc_od_do 0.5 1.5)) "[-inf, 1.5]U[2.5, inf]";;
test_wynik 16 (razy (wartosc_dopelnienie (-3.) (0.)) (wartosc_od_do 0. infinity)) "[-inf, inf]";;
test_wynik 17 (razy (wartosc_dopelnienie (-3.) (0.)) (wartosc_od_do 1. infinity)) "[-inf, -3.]U[0., inf]";;
test_wynik 18 (podzielic (wartosc_od_do 3. 5.) (wartosc_od_do (-1.) 1.)) "[-inf, -3.]U[3., inf]";;

(* TESTY KONSTRUKCJI *)
test_wynik 101 (wartosc_dokladna (-0.)) "[0., 0.]";;
test_wynik 102 (wartosc_od_do (-0.) infinity) "[0., inf]";;
test_wynik 103 (wartosc_dokladnosc (-1.) 25.) "[-1.25, -0.75]";;

(* TESTY SELEKTOROW *)
test_float 201 (min_wartosc (wartosc_od_do neg_infinity infinity)) neg_infinity;;
test_float 202 (max_wartosc (wartosc_od_do neg_infinity infinity)) infinity;;
test_float 203 (sr_wartosc (wartosc_od_do 0. infinity)) infinity;;
test_float 204 (sr_wartosc (wartosc_dopelnienie (-5.) (-3.))) nan;;
test_float 205 (sr_wartosc (wartosc_od_do 3. 7.)) 5.;;
test_float 206 (min_wartosc (wartosc_dopelnienie 2. 4.)) neg_infinity;;
test_float 207 (sr_wartosc (wartosc_od_do (-0.) (0.))) 0.;;
test_float 208 (max_wartosc (wartosc_od_do 1. infinity)) infinity;;
test_float 209 (min_wartosc (wartosc_dokladnosc (-10.) (30.))) (-13.);;
test_float 210 (sr_wartosc (wartosc_dokladnosc 3.123 3.)) 3.123;;
test_float 211 (sr_wartosc (podzielic (wartosc_dokladna 1.) (wartosc_od_do 0.0 1.0))) infinity;;
test_in_wartosc 212 (in_wartosc (wartosc_od_do (0.) (-0.)) (-0.)) true;;
test_in_wartosc 213 (in_wartosc (wartosc_dopelnienie 2. 6.) 6.0001) true;;
test_in_wartosc 214 (in_wartosc (wartosc_od_do neg_infinity (-0.)) 0.) true;;
test_in_wartosc 215 (in_wartosc (wartosc_dokladna 4.)  4.001) false;;
test_in_wartosc 216 (in_wartosc (razy (wartosc_dokladna 1.0) (wartosc_dokladna 0.0)) 0.0) true;;
test_in_wartosc 217 (in_wartosc (razy (wartosc_dokladna 0.) (wartosc_od_do 1.0 10.0)) 0.0) true;;
test_in_wartosc 218 (in_wartosc (wartosc_dokladna 3.) 3.) true;;
test_in_wartosc 219 (in_wartosc (podzielic (wartosc_dokladna (-1.)) (wartosc_od_do (-2.) (2.))) (-3.)) true;;

*)

