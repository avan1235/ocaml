(* wykorzystanie wbudowanych funkcji                                                                   *)
(* fold_left f a [x1; x2; x3; ...]  - polega na przetworzeniu kolejnych elementow listy i zastosowaniu *)
(* funkcji f na nie i zapisani wyniku do akukulatora                                                   *)
(* funkcja f przyjmuje dwa parametry: akumultor i aktualny element listy - f = fun a x                 *)
(* ford_right f [x1; x2; x3; ...] a - analogicznie jak fold_left tylko nie ogonowo i od ostatniego     *)
(* elemntu listy funkcja f przyjmuje dwa parametry: aktualny alement listy i akumulator - f = fun x a  *)
(* map f [x1; x2; x3; ...] - zastosowanie funkcji f do kolejnych elementow listy l i stworzenie nowej, *)
(* przetworzonej listy                                                                                 *)
(* filter p [x1; x2; x3; ...] - stworzenie listy jedynie z tych elementow wejsciowej listy, ktore      *)
(* spelniaja predykat p                                                                                *)

(* sprawdzanie czy na liscie znajduje sie element spelniajacy predykat *)
let exists p l =
  List.fold_left (fun a x -> a || p x) false l;;

(* wersja funkcji exists ktora nie przechodzi wszystkich elementow listy *)
let exists_faster p l =
  try
    List.fold_left (fun a x -> if p x then failwith "true" else false) false l
  with
    _ -> true;;

(* zanegowanie wartosci funkcji logicznej - stworzenie funkcji przeciwnej *)
let non f =
  function x -> not (f x);;

(* for_all - sprawdzenie czy predykat zachodzi dla wszystkich elementow listy *)
(* aby sie nie napisac zauwazamy ze   V x. p(x) <=> ~E x. ~p(x) *)
let for_all l p =
  not (exists_faster (fun x -> not (p x)) l);;

(* implementacja funkcji append przy pomocy fold_right *)
let append u v =
  List.fold_right (fun x a -> x::a) v u;;

(* zlozenie funkcji f1 f2 f3 ... z danej listy funkcji *)
let compose l =
  fun x -> List.fold_right (fun f a -> f a) l x;;

(* heads - zlozenie z listy list listy glow - gdy liscie list jest lista pusta to musi zostac odfiltrowana *)
let heads ll =
  ll |> List.filter (fun l -> if l = [] then false else true) |> List.map List.hd;;

let heads_simpler ll =
  ll |> List.flatten |> List.map List.hd;;

(* zwraca liste sum prefiksowych dla danej listy -> musimy przetwarzac liste i jednoczesnie trzymac w *)
(* akumulatorze zarowno liste prefiksow jak i ostatnia sume dlatego jako akumulator zostala uzyta para *)
let prefix_sum l =
  List.fold_left (fun (a, ls) x -> ((ls + x)::a, ls + x)) ([], 0) l |> fst |> List.rev;;

(* zwraca ostatnie lementy niepustych list zawartych w liscie ll *)
let ostatki ll =
  List.fold_right (fun l a -> ((List.hd (List.rev l))::a)) (List.filter (fun x -> x<>[]) ll) [];;

(* zwraca co drugi element list zaczynajac od drugiego *)
let co_drugi l =
  l |> List.fold_left (fun (s, l) x -> if s then (not(s), x::l) else (not(s), l)) (false, []) |> snd |> List.rev;;

(* podzial listy w taki sposob by kolejne powtarzajace sie elementy listy trafily d innych list *)
let podzial = function
  | [] -> []
  | hd::tl -> let (_, ll, l) = List.fold_right (fun x (lx, al, ll) -> if x = lx then (x, [x], al::ll) else (x, x::al, ll))
                  tl
                  (hd, [], [])
    in
    ll::l;;

(* zwrocenie prextremow danaj listy - elementow wiekszych lub mniejszych od wszystkich elementow je poprzedzajacych *)
let thr(_, _, x) = x;;

let prextrema l =
  match l with
  | [] -> []
  | [x] -> [x]
  | f::s::tl -> tl
    |> List.fold_left (fun (mx, mn, l) x -> if x > mx || x < mn then (max x mx, min x mn, x::l) else (mx, mn, l))
                      (max f s, min f s, if f = s then [f] else [s;f] )
    |> thr
    |> List.rev;;

(* zwraca najdluzszy scisle rosnacy fragment listy l *)
let wzrost = function
  | [] -> []
  | [x] -> [x]
  | hd::tl -> let (lmx, best, act, alen, _) = List.fold_left
                  (fun (lmx, best_l, alist, alen, lv) x -> if x > lv then (lmx, best_l, x::alist, alen+1, x)
                    else (if alen > lmx then (alen, alist, [x], 1, x)
                          else (lmx, best_l, [x], 1, x)))
                  (0, [], [hd], 1, hd)
                  tl in
    if alen > lmx then List.rev act else List.rev best;;


let count typ l = List.length (List.filter (fun x -> if x = typ then true else false) l);;


(* od_konca_do_konca wyznacza minimum z wartosci bezwzglednej roznicy elementow koncowych listy *)
let od_konca_do_konca l =
  match l with
  | [] | [_] -> 0
  | _ -> let l = List.fold_left2
                       (fun a l1 l2 -> (abs (l1 - l2))::a)
                       []
                       l (List.rev l)
    in
    List.fold_left (fun a x -> if x < a then x else a) (List.hd l) (List.tl l);;


(* usrednienie listy w postaci srenich dwoch kolejnych elementow listy *)
let usrednienie l =
  match l with
  | [] | [_] -> []
  | hd::tl -> List.fold_left (fun (a, lx) x -> ((((lx +. x) /. 2.)::a), x)) ([], hd) tl |> fst |> List.rev;;


(* zwraca liste sufiksow dla danej listy *)
let tails l =
  List.fold_right (fun x a -> (x::List.hd a)::a) l [[]];;


(* malo zwraca min |x_i + x_j| gdzie w posortowanej tablicy i < j *)
let malo l =
  let revl = List.rev l in
  let rec scan ll lr minv =
    match ll, lr with
    | [], _ | _, [] -> minv
    | hl::tl, hr::tr -> let new_val = hl + hr in
      if new_val > 0
      then scan ll tr (min (abs new_val) minv)
      else scan tl lr (min (abs new_val) minv)
  in
  scan l revl (abs (2*(List.hd revl)) + 1);;

(* avl_shaped spradza, czy drzewo ma ksztalt drzewa avl *)
(* czyli czy roznica wysokosci poddrzew jest nie wieksza niz 1 *)
type 'a tree = Node of 'a tree * 'a * 'a tree | Leaf;;

let rec fold_bin_tree f a t =
  match t with
  | Leaf -> a
  | Node(lt, v, rt) -> f v (fold_bin_tree f a lt) (fold_bin_tree f a rt);;

let avl_shaped t =
  fold_bin_tree (fun _ (lh, lb) (rh, rb) -> ((max rh lh) + 1, lb&&rb&&((abs (lh - rh)) <= 1)))
    (-1, true)
    t |> snd;;

(* fit zwraca min |c + x_i + x_j| dla danej listy [x_1; ...] i stalej c *)
let fit c l =
  let rec scan ll rl min_v =
    match ll, rl with
    | [], _ | _, [] -> min_v
    | hl::tl, hr::tr ->
      let new_v = c + hl + hr in
      let new_min = min (abs new_v) min_v in
      if new_v > 0
      then scan ll tr new_min
      else scan tl rl new_min
  in
  scan l (List.rev l) (abs (abs (2*(List.hd l)) + 1 + c));;
