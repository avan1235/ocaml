(* Normalne drzewa z dowolna liczba dzieci *)

type 'a tree =
    Node of ('a * ('a tree) list);;

let rec size (Node (_, l)) =
  let rec loop l acc =
    match l with
    | [] -> acc
    | hd::tl -> loop tl (acc + size hd)
  in loop l 1;;

let rec depth (Node (_, l)) =
  let rec loop l =
    match l with
    | [] -> 0
    | hd::tl -> max (1 + depth hd) (loop tl)
  in loop l;;

(* sprytne wyznaczanie glebokosci drzewa - bez petli podzieciach *)
let rec better_depth (Node(x, l)) =
  match l with
  | [] -> 0
  | hd::tl -> max (1 + depth hd) (depth (Node(x, tl)));;

let rec preorder (Node (x,l)) =
  let rec loop l =
    match l with
    | [] -> []
    | hd::tl -> (preorder hd)@(loop tl)
  in
  x::(loop l);;
  
(*
             1
          / | \ \
         2  5  6 3
        /  / \
       4  7   8
*)

let print_list l =
  let rec scan l =
    match l with
    | [] -> ""
    | hd::tl -> (string_of_int hd )^" "^(scan tl)
  in
  print_string (scan l);;
