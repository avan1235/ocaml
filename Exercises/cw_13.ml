(* przyklady imperatywnego programowania *)
let a = ref 0;;
let b = a;;
a := !a + 1;;
!b;;

(* listy jednokierunkowe *)
type 'a elem = {v : 'a; mutable next : 'a lista}
and 'a lista = 'a elem option

(* zadanie petla - mamy zwrocic zapetlona, odwrocona jednokierunkowa liste *)
(* zdefiniowana wczesniej jako 'a elem *)
let petla l =
  if l = None then () else
    let get o = match o with | Some x -> x | None -> assert false in
    let cur = ref l in
    let next = ref ((get l).next) in
    while !next <> None do
      let e = get (!next) in
      let n = e.next in(
        e.next <- !cur;
        cur := !next;
        next := n)
    done;
    (get l).next <- !cur;;


(* stworzenie modulu bedacego licznikiem o podanej sugnaturze *)
module type COUNTER =
  sig
    type counter
    val make: unit -> counter
    val increase: counter -> int (* zwraca aktualny, zwiekszony licznik *)
    val reset: unit -> unit (* zeruje liczniki *)
  end;;


module Counter : COUNTER =
struct    
    type counter = {mutable value : int; mutable update_time : int}
    let time = ref 0
    let make () = {value = 0; update_time = !time}
    let reset () = time := !time + 1
    let increase c =
      (if !time  > c.update_time
      then (c.value <- 1; c.update_time <- !time)
      else (c.value <- c.value + 1));
      c.value
  end
