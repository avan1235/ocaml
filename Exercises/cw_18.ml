type 'a elem = {v : 'a; mutable next : 'a lista; mutable prev : 'a lista}
and 'a lista = 'a elem option;;

let codrugi l =
  let get o = match o with | Some (x) -> x | None -> assert false in
  if l = None || (get l).next = None then None else
    begin
      let act_p = ref ((get l).next) in
      let new_l = {v = (get !act_p).v; next = None; prev = None} in
      let new_p = ref (Some new_l) in
      let old_p = ref l in
      let i = ref 1 in
      act_p := ((get (!act_p)).next);
      while !act_p <> None
      do
        if !i mod 2 = 1
        then
          begin
            (get (!old_p)).next <- !act_p;
            (get (!act_p)).prev <- !old_p;
            old_p := !act_p;
            act_p := (get (!act_p)).next;
          end
        else
          begin
            (get (!new_p)).next <- !act_p;
            (get (!act_p)).prev <- !new_p;
            new_p := !act_p;
            act_p := (get (!act_p)).next;
          end;
         i := !i + 1;
      done;
      (get (!old_p)).next <- None;
      (get (!new_p)).next <- None;
       Some new_l
    end;;

(* ------------- TESTY ------------- *)

let l2 = {v = 2;
          prev = None;
          next = None
         };;

let l1 = {v = 1;
          prev = None;
          next = Some l2
         };;

l2.prev <- Some l1;;

let l3 = {v = 3;
          prev = Some l2;
          next = None
         };;

l2.next <- Some l3;;

let l4 = {v = 4;
          next = None;
          prev = Some l3
         };;

l3.next <- Some l4;;

let rec print_special x =
    begin
      print_int x.v;
      print_string " - ";
      match x.next with
      | None -> print_newline ()
      | Some(l) -> print_special l
    end;;

let l1 = Some l1;;

let get o = match o with
  | None -> assert false;
  | Some(x) -> x;;


print_special (get (codrugi l1));;
print_special (get l1);;


