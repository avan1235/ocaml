let max a b =
  if a > b
  then a
  else b;;

let min a b =
  if a < b
  then a
  else b;;


(* Crossing rectangles *)


let if_crossing_rectangle xld1 yld1 xru1 yru1 xld2 yld2 xru2 yru2 =
  if (((min (yru1) (yru2)) > (max (yld1) (yld1))) && ((min (xru1) (xru2)) > (max (xld1) (xld2))))
  then true
  else false;;

(* Example of crossing rectangles *)
if_crossing_rectangle 0 4 7 7 4 0 9 8;;
(* Example of non-crosing rectangles *)
if_crossing_rectangle (-1) (-1) 1 1 2 2 3 3;;


(* Crossing lines *)


let sgn x =
  if x > 0 then(
    1
  )
  else(
    if x = 0
    then 0
    else (-1)
  );;

let vector x1 y1 x2 y2 =
  (x1*y2)-(x2*y1);;

let side_of_vector vx1 vy1 vx2 vy2 px py =
  sgn (vector (vx2-vx1) (vx2-vx1) (px-vx1) (py-vy1));;
