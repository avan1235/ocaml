(* Definition of binary tree *)                                                                                                                                       
type 'a tree =
    Node of (('a tree) * ('a) * ('a tree)) | Leaf;;                                                                                                                   


(* Drzewo jest symetryczne gdy oboje dzieci maja te same wartosci              *)
(* i odpowiednio podwieszone drzewa sa rowniez symetryczne - procedura check   *)
let rec symetric t =                                                                                                                                     
  match t with                                                                                                                                                       
  | Leaf -> true                                                                                                                                                     
  | Node (lt, _, rt) -> let rec check a b =                                                                                                                              
                     match a,b with                                                                                                                                  
                     | Leaf, Leaf -> true                                                                                                                            
                     | Node(la, xa, ra), Node(lb, xb, rb) ->                                                                                                         
                       xa = xb && (check la rb) && (check ra lb)                                                                                                     
                     | _ -> false                                                                                                                                    
    in                                                                                                                                                               
    check lt rt;;

(* rozwarzajac dany node zastanawiamy sie, czy dluzsza jest aktulna glebokosc  *)
(* od dolu czy tez raczej jakas obecna srednica czyli                          *)
(* suma dwoch glebokosci plus 2                                                *)
let long_path t = (* poszukiwanie najdluzszej srednicy w drzewie binarnym      *)
  let rec scan t = (* zwraca pare (glebokosc, srednica)                        *)
    match t with
    | Leaf -> (-1, 0)
    | Node (lt, _, rt) -> let (gl, sl) = scan lt
                          and (gr, sr) = scan rt in
                          (1 + max gl gr, max (max sl sr) (gl + gr + 2))
  in
  snd (scan t);;



(* chcemy przeciac drzewo w miejscu, w ktorym liczba wezlow bedzie rowna polowie *)
(* wszystkich wezlow lub jak najblizsza tej wartosci                             *)
let cut tree =
  let rec size t =
    match t with
    | Leaf -> 0
    | Node (lt, _, rt) -> 1 + (size lt) + (size rt)
  in
  let n = size tree in
  let rec rebuild_tree t = (* zwraca ((najlepsza_odl, punkt_ciecia), roz_poddrzewa) *)
    match t with
    | Leaf -> ((-1, Leaf), 0)
    | Node (lt, _, rt) ->
                         let (result_l, size_l) = rebuild_tree lt
                          and (result_r, size_r) = rebuild_tree rt in
                          let size = 1 + size_r + size_l in
                          let len = abs (size - (n/2)) in
                          let result = (len, t) in
                          (min result (min result_r result_l), size)
  in
  snd (fst (rebuild_tree tree))
    
      
(* wyrzzucenie z drzewa tyc nodow dla ktorych predykat p zwraca true                  *)
(* -> zwrocenie listy drzew                                                           *)
let cut_predicted t p =
  let rec scan t a =
(* funkcja scan zwraca drzewo t pociete na mniejsze drzewa jako list gdzie glowa listy  *)
(* zawiera drzewo z korzeniem t (badz Nil jezeli krawedz do ojca z t ma zostac odcieta) *)
(* to drzewo zostaje zwrocone do akumulatora @a                                         *)
    match t with
    | Leaf -> Leaf::a
    | Node (l, x, r) -> let hl::a' = scan l a in
                        let hr::a'' = scan r a' in
                        let a''' = (Node(hl, x, hr))::a'' in
                        if p x
                        then Leaf::a'''
                        else a'''
  in
  let res = scan t [] in (* wyznaczenie listydla drzewa *)
  if List.hd res = Leaf  (* i sprawdzeniee czy predykat nie zachodzil dla korzenia *)
  then List.tl res
  else res;;
