(*****************************************************************************************)
(***************************** TESTS FOR TOPOLOGICAL SORTING *****************************)
(*****************************************************************************************)


(* module of int set made for tests                                                      *)
module IntSet = Set.Make(
    struct
      let compare = Pervasives.compare
      type t = int
    end
    );;


(* function for printing bad list in topological order                                   *)
let print_list l =
  print_string "\n Bad result is: ";
  let rec scan l =
    match l with
    | [] -> print_newline ()
    | hd::tl ->
      print_int hd;
      print_string " - ";
      scan tl
  in
  scan;;


(* function for comparing results of list l in topological order and list of lists       *)
(* check_l which has in one lists numbers of nodes which can be given in any order       *)
let compare_results l check_l =
  let res_a = Array.of_list l in
  let a = Array.of_list check_l in
  let n = Array.length a in
  let checked = ref 0 in
  let result = ref true in
  try
    begin
      for i = 0 to n-1
      do
        let set = List.fold_left (fun s x -> IntSet.add x s) IntSet.empty a.(i) in
        for j = !checked to !checked + IntSet.cardinal (set) - 1 do
          if not (IntSet.mem res_a.(j) set) then result := false;
        done;
        checked := !checked + IntSet.cardinal (set);
      done;
      !result
    end
  with invalid_index_exception -> false;;


(* function for printing test result                                                     *)
let print_test_res no b =
  print_string "TEST ";
  print_int no;
  print_string (if b then " OK\n" else " ERROR\n");;


(* function for testing cyclic graphs                                                    *)
let test_cyclic no l =
  let result =
    try
      let _ = Topol.topol l in false
    with Topol.Cykliczne -> true
  in
  print_test_res no result;;
  


print_string "=========== BASIC TESTS ===========\n";;

let test1 = [(1,[2;3;4]); (2,[5]); (3,[5])];;
let test1_res = [[1]; [2;3;4]; [5]];;

print_test_res 1 (compare_results (Topol.topol test1) test1_res);;

let test2 = [(1,[2;3;4;5])];;
let test2_res = [[1]; [2;3;4;5]];;

print_test_res 2 (compare_results (Topol.topol test2) test2_res);;

let test3 = [(1,[]); (2,[]); (3,[])];;
let test3_res = [[1;2;3]];;

print_test_res 3 (compare_results (Topol.topol test3) test3_res);;

let test4 = [(2,[1]); (3,[1]); (4,[1])];;
let test4_res = [[2;3;4]; [1]];;

print_test_res 4 (compare_results (Topol.topol test4) test4_res);;

let test5 = [(1, [2;3]); (2, [4; 5]); (3, [5; 6]); (4, [7]); (5, [7; 8]); (6, [8]); (7, [9]); (8, [9])];;
let test5_res = [[1]; [2; 3; 4; 5; 6; 7; 8]; [9]];;

print_test_res 5 (compare_results (Topol.topol test5) test5_res);;

print_string "=========== CYCLIC TESTS ===========\n";;

let test6 = [(1, [2]); (2, [1])];;
test_cyclic 6 test6;;

let test7 = [(1, [2]); (2, [3]); (3, [4]); (4, [1])];;
test_cyclic 7 test7;;

let test8 = [(1, [2; 3]); (2, [1]); (3, [1])];;
test_cyclic 8 test8;;

let test9 = [(1, [2]); (2, [3]); (3, [7; 4]); (4, [5]); (5, [6]); (6, [3]); (7, [1])];;
test_cyclic 9 test9;;

let test10 = [(1, [2; 3]); (2, [4; 5]); (3, [5; 6]); (4, [1]); (6, [1])];;
test_cyclic 10 test10;;
