type vector = {x: float;
               y: float;
               z: float};;

let add_vec a b = {x = a.x +. b.x;
                   y = a.y +. b.y;
                   z = a.z +. b.z};;

let len a  = sqrt ((a.x * a.x) +. (a.y * a.y) +. (a.z * a.z));;

let scalar_prod (a: vector) (b: vector) = a.x*b.x + a.y*b.y + a.z*b.z;;
let vec_prod (a: vector) (b: vector) = {x = a.y*b.z - a.z*b.y;
                    y = a.z*b.x - a.x*b.z;
                    z = a.x*b.y - a.y*b.z};;


let print_vector (a: vector) =
  print_string "x = ";
  print_float a.x;
  print_newline;
  print_string "y = ";
  print_float a.y;
  print_newline;
  print_string "y = ";
  print_float a.y;
  print_newline;
  
                           

let (vec: vector) = {x=1.; y=1.; z=1.};;
print_float (len vec);;
print_vector vec;;
