(* Funcja parzystosc wyznacza najwiekszy wykladnik "i" potegi liczby dwa dla ktorej liczba jest podzielna przez 2^i *)
(* Wykorzystana zostala w przykladzie rekurencja ogonowa *)

let parzystosc x =
  if x = 0
  then -1
  else
    let rec help l p i =
      if l mod p = 0
      then help l (p*2) (i+1)
      else i-1
    in
      help x 1 0;;

(* Funkcja zwraca liczbe tak, ze ma ona cyfry w odwrotnej kolejnosci *)

let odwr x =
  let rec help l a =
    if l > 0
    then help (l/10) (a*10+(l mod 10))
    else a
  in
    help x 0;;
   
(* Wyznaczanie podlogi pierwiastka na podstawie spostrzezenia ze (k+1)^2 - k^2 = 2k + 1 *)

let sqrt x =
  let rec help s k =
    let s = s+k+k+1 in
      if s > x
      then k
      else help s (k+1)
  in
    help 0 0;;

let xor a b = ((a||b)&&(not(a&&b)));;

let check_right_nth_bit num n =
  let checker = 1 lsl (n-1) in
    if ((num land checker) > 0)
    then true
    else false;; 

let change_right_nth_bit_to b num n =
  let temp = check_right_nth_bit num n
  in
    if (xor b temp) (* Nieprawda ze wartosci temp (terazniejsza bitu) i b (nowa bitu) sa takie same -> sa one rozne*)
    then (num lxor (1 lsl (n-1)))
    else num;;

let change_right_nth_bit_to_true number n =
  change_right_nth_bit_to true number n;;

let change_right_nth_bit_to_false number n =
  change_right_nth_bit_to false number n;;

let repair_number_from_right_nth_bit number n =
  if n > 1
  then(
    let rec help num n =
      if n > 0
      then help (change_right_nth_bit_to_false (num) (n)) (n-1)
      else num
    in
      help (change_right_nth_bit_to_true number n) (n-1)
  )
    else change_right_nth_bit_to_true number n;;

let get_bin_string num =
  let rec help a i pd =
    if pd > num
    then a
    else(
      if (check_right_nth_bit num (i+1))
      then help ("1"^a) (i+1) (pd*2)
      else help ("0"^a) (i+1) (pd*2)
    )
  in
    help "" 0 1;;

let how_many_bits number =
  let rec help num a =
    if num > 0
    then help (num lsr 1) (a+1)
    else a
  in
    help number 0;;

let print_bin n = 
  print_string (get_bin_string n);;
  
let higher_rare_number number =
  let rec check_bit num act_i high_i lbv =
    if (act_i > high_i)
    then num
    else(
      let abv = check_right_nth_bit num act_i
      in
	if ((abv && lbv) && (not (check_right_nth_bit num (act_i+1))))
	then check_bit (repair_number_from_right_nth_bit num (act_i+1)) (act_i+2) (high_i) true	  
	else check_bit num (act_i+1) high_i abv
    )
  in
    let new_number = check_bit number 1 (how_many_bits number) false
  in
    if (new_number = number)
    then check_bit (number+1) 1 (how_many_bits (number+1)) false
    else new_number;;

let how_many_zeros_in_end_of_number number =
  let rec help num a =
    let bool_value = (num land 1)
    in
      if (bool_value = 0)
      then help (num lsr 1) (a+1)
      else a
  in
    help number 0;;

let fib n =
  let rec help a b n =
    if n = 0
    then a
    else help b (a+b) (n-1)
  in
    help 0 1 n;;
		       
let sum_of_n_fib n = ((fib (n+1))-1);; (* +1 z powodu ze to suma wiec potrzebna wieksza fib_num *)
   

let where_is_second_1_from_left_in_number number =
  let rec check_bits_from_left act_i =
    if (act_i = 0)
    then (-1)
    else(
      let checker = (1 lsl (act_i-1))
      in
	if ((number land checker) > 0)
	then act_i
	else check_bits_from_left (act_i-1)
    )
  in
    check_bits_from_left ((how_many_bits number)-1);;

let has_second_1 number =
  let rec help act_i =
    if (act_i > 0)
    then(
      if (check_right_nth_bit number act_i)
      then true
      else help (act_i-1)
    )
    else false
  in
    help ((how_many_bits number)-1)

let how_many_smaller_rare_numbers number =
  let next_rare_number = higher_rare_number number
  in
    if (not (has_second_1 next_rare_number))
    then sum_of_n_fib ((how_many_zeros_in_end_of_number next_rare_number)+1)
    else(
      let zeros = how_many_zeros_in_end_of_number next_rare_number
      and second_1 = where_is_second_1_from_left_in_number next_rare_number
      and bits = how_many_bits next_rare_number
      in
	if (zeros = (second_1 - 1))
	then ((sum_of_n_fib (zeros+1)) + 1 + (sum_of_n_fib bits))
	else ((sum_of_n_fib (zeros+1))+1 + (sum_of_n_fib second_1)+1 + (sum_of_n_fib bits))
    );;

