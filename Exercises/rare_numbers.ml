let xor a b = if a <> b then true else false;;

let check nth num =
  if (num land (1 lsl nth)) > 0
  then true
  else false;;

let change_to bv nth num =
  if xor bv (check nth num)
  then num lxor (1 lsl nth)
  else num;;

let repair_from nth num =
  let rec help n c_i =
    if c_i >= 0
    then help (change_to false c_i n) (c_i-1)
    else n
  in
    help (change_to true nth num) (nth-1);;

let bit_count num =
  let rec help n a =
    if n > 0
    then help (n lsr 1) (a+1)
    else a
  in
    help num 0;;

let higher_rare num =
  let new_num = num + 1 in
  let h_i = bit_count new_num in
  let rec check_bits n nbv cbv lbv c_i =
    if c_i < h_i
    then(
      if (cbv && lbv && (not nbv))
      then check_bits (repair_from (c_i + 1) n) (check (c_i + 3) n) (check (c_i + 2) n) true (c_i + 2)
      else check_bits n (check (c_i + 2) n) nbv cbv (c_i + 1)
    )
    else n
  in check_bits (num + 1) (check 1 new_num) (check 0 new_num) false 0;;

assert ((higher_rare 42) = 64);;
assert ((higher_rare 31) = 32);;
assert ((higher_rare 64) = 65);;
assert ((higher_rare 5) = 8);;
assert ((higher_rare 41) = 42);;

