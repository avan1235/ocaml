let sum l =
  let rec help l acc =
    match l with
      | hd::tl -> help tl (acc+hd)
      | [] -> acc
  in
    help l 0;;

let append l1 l2 = l1@l2;;

let rec nth l n =
  match (l, n) with
  | (hd::tl, 0) -> hd
  | (hd::tl, n) -> nth tl (n-1)
  | ([], _) -> failwith "empty list";;

let rev l =
  let rec help l nl =
    match l with
      | hd::tl -> help tl (hd::nl)
      | [] -> nl
  in
    help l [];;

let first_n n =
  let rec help acc n =
    match n with
      | 0 -> acc
      | x -> help (x::acc) (n-1)
  in
    help [] n;;

let rec double_list l =
  match l with
    | [] -> []
    | hd::tl -> hd::hd::(double_list tl);;

let rec schuffle l1 l2 =
  match (l1, l2) with
    | (_, []) -> l1
    | ([], _) -> l2
    | (hd1::tl1, hd2::tl2) -> hd1::hd2::(schuffle tl1 tl2);;

let rec schuffle2 l1 l2 =
  match l1 with
    | [] -> l2
    | hd::tl -> hd::(schuffle l2 tl);;

let rec tails l =
  let rec help l =
    match l with
      | [] -> [[]]
      | _::tl -> l::(help tl)
  in
    rev (help l);;

let minmax l =
  let improve (on, ox) (nn, nx) =
    ((min on nn), (max ox nx))
  and
      op x y = if x < y then (x, y) else (y, x)
  in
  let rec help l acc =
    match l with
      | x::y::tl -> help tl (improve acc (op x y))
      | [x] -> improve acc (x, x)
      | [] -> acc
  in
    match l with
      | [] -> failwith "empty list"
      | [x] -> (x, x)
      | x::y::tl -> help tl (op x y);;

let min_rest l comp =
  let rec help cn l nl =
    match l with
    | [] -> (cn, nl)
    | hd::tl -> ( if (comp hd cn) < 0
                  then help hd tl (cn::nl)
                  else help cn tl (hd::nl)
                )
  in
  match l with
  | [] -> failwith "empty list"
  | hd::tl -> help hd tl [];;

let sort l comp =
  let rec help l =
    match l with
    | [] -> []
    | tl -> let cn = min_rest tl comp
            in (fst cn)::(help (snd cn))
  in
  help l;;
