open ISet;;


let iset_from_list l =
  List.fold_left (fun a x -> add x a) empty l;;


let print_ok n =
  print_string "TEST " ; print_string (string_of_int n) ; print_string " OK\n";;


let print_error n err =
  print_string "TEST " ; print_string (string_of_int n) ; print_string " ERROR: " ; print_string err ; print_newline ();;


let test_bool n b eb =
  if (eb || b) && not (eb && b)
  then print_error n "bools not matched"
  else print_ok n;;


let test_list res expect =
  try
    List.fold_left2 (fun a (l1, r1) (l2, r2) -> a && l1 = l2 && r1 = r2) true res expect
  with
  | Invalid_argument _ -> false;;


let test_elements n elem expect =
  if test_list (elements elem) expect
  then print_ok n
  else print_error n "lists not matched"


let print_intvl (l, r) =
  print_string "(" ; print_int l ; print_string ", " ; print_int r ; print_string ")";;

let rec print_list = function
  | [] -> ()
  | e::l -> print_intvl e ; print_string " " ; print_list l

let empt = empty;;
let not_empt = add (1, 1) empty;;

test_bool 1 (is_empty empt) true;;
test_bool 2 (is_empty not_empt) false;;

let all = add (min_int, max_int) empty;;
test_elements 3 all [(min_int, max_int)];;

let not_all = remove (-1, 1) all;;
test_elements 4 not_all [(min_int, -2); (2, max_int)];;

let many_list =
  let rec create acc n =
    if n > 1000
    then acc
    else create (n::acc) (n+3)
  in create [] (-1000) |> List.map (fun v -> (v, v + 1));;

let many_set = iset_from_list many_list;;
let many_list = List.sort compare many_list;;
test_elements 5 many_set many_list;;

let holes_set =
  let rec del n set =
    if n <= 1000
    then del (n + 3) (remove (n, n) set)
    else set
  in del (-998) (add (-1000, 1000) empty);;

test_elements 6 holes_set many_list;;
  
let (sl, _, sr) = split 1 many_set;;
let nl = List.filter (fun (_, r) -> r < 1) many_list;;
let nr = List.filter (fun (l, _) -> l > 1) many_list;;

test_elements 7 sl nl;;
test_elements 8 sr nr;;

let moved_list = List.map (fun (l, r) -> (l - 1000, r - 1000)) many_list;;
let moved_set = fold (fun (l, r) s -> add (l - 1000, r - 1000) (remove (l, r) s)) many_set empty;;

test_elements 9 moved_set moved_list;;

let singletons_set =
  let rec add_s set n =
    if n <= 1000
    then add_s (add (n, n) set) (n + 1)
    else set
  in
  add_s empty (-1000);;

test_elements 10 singletons_set [(-1000, 1000)];;
