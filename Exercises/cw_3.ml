let pot_easy a n =
  let rec pot a n acc =
    if n > 0
    then pot a (n-1) (acc*a)
    else acc
  in
    pot a n 1;;

let check nth num =
  if (num land (1 lsl nth)) > 0
  then true
  else false;;

(* potegowanie z uzyciem sprawdzania kolejnych bitow potegi *)
let pot a n =
  let rec p n acc b =
    if n > 0
    then(
      if check 0 n
      then p (n/2) (acc*b) (b*b)
      else p (n/2) acc (b*b)
    )
    else acc
  in
    p n 1 a;;

(* szybkie potegowanie z wykorzstaniem mod *)
let pot a n =
  let rec help am n acc =
    if n = 0
    then acc
    else if (n mod 2) = 1
    then help (am*am) (n/2) (acc*am)
    else help (am*am) (n/2) (acc)
  in
  help a n 1;;


let m_sub list =
  let rec help p k sum max_s =
    if k = (List.length list)-1
    then max_s
    else (
      if sum >= 0
      then help p (k+1) (sum + List.nth list (k+1)) (max max_s (sum + List.nth list (k+1)))
      else help (p+1) k (sum - List.nth list p) max_s
    )
  in
    (help 0 0 (List.nth list 0) (List.nth list 0));;

let _ = m_sub [4;1;-5;3;-4;2;1;];;]
