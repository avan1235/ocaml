type 'a elem = {v : 'a; mutable next : 'a lista}
and 'a lista = 'a elem option;;


let rec print_lista l =
  match l with
  | None -> print_newline ()
  | Some x ->
    begin
      print_int x.v;
      print_string " - ";
      print_lista x.next;
    end;;


let warkocz l1 l2 l3 =
  let get o =
    match o with
    | Some x -> x
    | None -> assert false
  in
  let point1 = ref l1 in
  let point2 = ref l2 in
  let point3 = ref l3 in
  let old1 = ref None in
  let old2 = ref None in
  let old3 = ref None in
  while !point1 <> None && !point2 <> None && !point3 <> None
  do
    let temp1 = (get (!point1)).next in
    let temp2 = (get (!point2)).next in
    let temp3 = (get (!point3)).next in
    if temp1 <> None && temp2 <> None && temp3 <> None then
      begin
        (get (!point1)).next <- temp3;
        (get (!point2)).next <- temp1;
        (get (!point3)).next <- temp2;
      end;
    old1 := !point1;
    old2 := !point2;
    old3 := !point3;
    point1 := temp2;
    point2 := temp3;
    point3 := temp1;
  done;
  if (!point1 = None && !point2 = None)
  || (!point1 = None && !point2 = None)
  || (!point1 = None && !point2 = None) then () else
    begin
      let (np1, np2) = if !point1 = None then (ref !old1, ref !old3) else
        if !point2 = None then (ref !old2, ref !old1) else
        if !point3 = None then (ref !old2, ref !old3) else assert false in
      while !np1 <> None && !np2 <> None
      do
        let temp1 = (get (!np1)).next in
        let temp2 = (get (!np2)).next in
        if temp1 <> None && temp2 <> None then
          begin
            (get (!np1)).next <- temp2;
            (get (!np2)).next <- temp1;
          end;
        np1 := temp1;
        np2 := temp2;
      done;
    end;;


let przeplot l1 l2 =
  let get o =
  match o with
    | Some x -> x
    | None -> assert false
  in
  let p1 = ref l1 in
  let p2 = ref l2 in
  while !p1 <> None && !p2 <> None
  do
    let temp1 = (get !p1).next in
    let temp2 = (get !p2).next in
    (get !p1).next <- !p2;
    if temp1 <> None then (get !p2).next <- temp1;
    p1 := temp1;
    p2 := temp2;
  done;;


type 'a tree =
  | Node of 'a * 'a tree * 'a tree * 'a tree ref
  | Leaf;;

let potomek t =
  let rec fold_tree t =
    match t with
    | Leaf -> (-1, Leaf)
    | Node (v, lt, rt, rfr) ->
      let (ldep, lres) as lr = fold_tree lt in
      let (rdep, rres) as rr= fold_tree rt in
      if ldep = -1 && rdep = -1 then (0, t) else
        begin
          let (maxdep, maxres) = max lr rr in
          rfr := maxres;
          (maxdep+1, maxres)
        end
  in
  ignore (fold_tree t);;


let trojkaty a =
  Array.sort compare a;
  let n = Array.length a in
  if n < 2 then [||] else
    begin
      let sprawdz i j k = a.(i) + a.(j) > a.(k) in
      let k = ref 2 in
      let max_pair (p1, l1) (p2, l2) = if l1 >= l2 then (p1, l1) else (p2, l2) in
      let wyn = ref (0, 0) in
      for i = 0 to n-3 do
        while sprawdz i (i+1) !k do k := !k + 1 done;
        wyn := max_pair (i, !k-1-i) !wyn;
      done;
      let w = Array.init (snd !wyn + 1) (fun i -> a.(i + fst !wyn)) in
      w
    end;;

type elem = {x : int; mutable prev : elem list};;
type lista = elem list;;

let ustaw (l : lista) =
  match l with
  | [] -> ()
  | _ ->
    let a = Array.of_list l in
    let n = Array.length a in
    let rec nth_tail n l =
      if n > 0 then nth_tail (n-1) (List.tl l) else l
    in
    for i = 0 to n-1
    do
      a.(i).prev <- nth_tail (n-1-i) l
    done;
    ignore (List.fold_left (fun i v -> (v.prev <- a.(i).prev; (i+1))) 0 l);;


let 
