(****************************************************************************************)
(************************************ MODUL ORIGAMI *************************************)
(****************************************************************************************)

(*--------------------------------------------------------------------------------------*)
(* Author: Maciej Procyk                                                                *)
(* Reviewer: Kamil Poniewierski                                                         *)
(*--------------------------------------------------------------------------------------*)

(***************************************************************************************)
(**************************************** TYPES ****************************************)
(***************************************************************************************)

(* a point on the plane represented as a pair of two coordinates of float numbers      *)
type point = float * float


(* functional type which returns the number of how many times the point p would pierce *)
(* the sheet in certain place                                                          *)
type kartka = point -> int
                         
                         
(* help type in defining a line from the equation a*x+b*y+c=0                          *)
type line = {a : float; b : float; c : float}


(* type representing vector in 3 dimensional space                                     *)
type vector = {x : float; y : float; z : float}


(***************************************************************************************)
(************************************* CONSTRUCTORS ************************************)
(***************************************************************************************)

(* constructor of the line from two points                                             *)
(* line is represent using the equation a*x+b*y+c=0 for points (x,y)                   *)
let line_of_points ((x1, y1) : point) ((x2, y2) : point) =
  {a = y2 -. y1; b = x1 -. x2; c = (x2 *. y1) -. (x1 *. y2)}


(* constructor for vector from p1 to p2 point                                          *)
let vec_of_points ((x1, y1) : point) ((x2, y2) : point) =
  {x = x2 -. x1; y = y2 -. y1; z = 0.}


(* constructor of karka of the rectangle shape that starts in left down corner p1      *)
(* and ends in right up corner p2                                                      *)
let prostokat ((x1, y1) : point) ((x2, y2) : point) =
  (fun ((x, y) : point) ->
    if x >= x1 && x <= x2 && y >= y1 && y <= y2 then 1 else 0)


(* constructor of kartka in the shape of circle of the center in point (xc, yc) and    *)
(* of the radius equal to r                                                            *)  
let kolko ((xc, yc) : point) (r : float) =
  (fun ((x, y) : point) -> if (x -. xc)**2. +. (y -. yc)**2. <= r**2. then 1 else 0)


(***************************************************************************************)
(*************************************** CONSTANTS *************************************)
(***************************************************************************************)

(* constant for aproximation of equations                                              *)
let epsilon = 100. *. epsilon_float 


(***************************************************************************************)
(************************************* COMPARATORS *************************************)
(***************************************************************************************)
                
(* float equal comparator with the specified epsilon error that can be made in calcs   *)
let (#=) (f1 : float) (f2 : float) =
  f1 < f2 +. epsilon && f1 > f2 -. epsilon


(***************************************************************************************)
(*************************************** FUNCTIONS *************************************)
(***************************************************************************************)

(* cross product of two vectors v1 x v2                                                *)
let cross_product (v1 : vector) (v2 : vector) =
  {x = v1.y *. v2.z -. v1.z *. v2.y;
   y = v1.z *. v2.x -. v1.x *. v2.z;
   z = v1.x *. v2.y -. v1.y *. v2.x}


(* checks if point p is on the right of the line from p1 to p2                         *)
let is_on_right (p1, p2) p =
  (cross_product (vec_of_points p1 p) (vec_of_points p1 p2)).z > (0. +. epsilon)


(* p @= l checks if point is on the line l                                             *)
let (@=) ((x, y) : point) (l : line) =
  ((l.a *. x +. l.b *. y +. l.c) #= 0.)


(* creates a line perpendicular to the given line which consists point p               *)
let perp_line (l : line) ((x, y) : point) =
  {a = l.b; b = (-1.)*.l.a; c = l.a*.y -. l.b*.x}


(* returns the point where two lines crosses                                           *)
let cross_point (l1 : line) (l2 : line) =
  ((l2.c*.l1.b -. l1.c*.l2.b) /. (l1.a*.l2.b -. l2.a*.l1.b),
   (l2.c*.l1.a -. l1.c*.l2.a) /. (l1.b*.l2.a -. l2.b*.l1.a))


(* returns the symmetric point to the point p relative to line l                       *)
let sym_point ((x, y) : point) (l : line) =
  let pl = perp_line l (x, y) in
  let (xc, yc) = cross_point pl l in
  (2.*.xc -. x, 2.*.yc -. y)
  

(* operation zloz that creates the function which will check how many times kartka    *)
(* is pierced in new coordinate system                                                *)
let zloz (p1 : point) (p2 : point) (k : kartka) =
  (fun (p : point) ->
     let l = line_of_points p1 p2 in
     if p @= l then k p else
     if is_on_right (p1, p2) p then 0 else
       let sp = sym_point p l in
       (k p + k sp)
  )


(* operation zloz defined by pairs of points in the list l which is used for the      *)
(* initial kartka k                                                                   *)
(* [skladaj [(p1_1, p2_1); ...; (p1_n, p2_n)] k =                                     *)
(*                                   zloz p1_n p2_n (zloz ... (zloz p1_1 p2_1 k) ...) *)
let skladaj l k =
  List.fold_left (fun k (p1, p2) -> zloz p1 p2 k) k l
