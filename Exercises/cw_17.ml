(* zadanie rejs - tomek chcial poplynac w k-dniowy rejs  i chcial ze soba zabrac kolegow *)
(* na liscie dane pary kedy dany kolega moze wyplynac i kiedy musi wrocic                *)
(* sprawdzic ilu kolegow najwiecej moze zabrac na rejs                                   *)
let rejs k l =
  let pocz = -1 and kon = 1 in
  let lista_zdarzen = List.map (fun (a, b) -> [(a, pocz); (b, kon)]) l |> List.flatten |> List.sort compare in
  let tab_zdarzen = Array.of_list lista_zdarzen in
  let n = Array.length tab_zdarzen in
  let sprawdz r =
    let akt_liczba = ref 0 in
    let ost_wsiadl = ref 0 in
    let wynik = ref false in      
    for i = 0 to n-1 do
      let (t, zdarz) = tab_zdarzen.(i) in
      if zdarz = pocz
      then (wynik := !wynik || (!akt_liczba >= r && t - !ost_wsiadl >= k);
            akt_liczba := !akt_liczba + 1; ost_wsiadl := t;)
      else (
        wynik := !wynik || (!akt_liczba >= r && t - !ost_wsiadl >= k);
        akt_liczba := !akt_liczba - 1;
      )
    done;
    !wynik
  in
  let p = ref (1 + List.length l) in
  let l = ref 0 in
  while !l + 1 < !p do
    let m = (!l + !p) / 2 in
    if sprawdz m then l := m else p := m
  done;
  !l;;


let print_pair (a, b) =
  print_int a;
  print_string ", ";
  print_int b;
  print_newline ();;

let print_a a =
  Array.iter (fun x -> print_pair x) a;;

rejs 20 [(12, 36); (48, 100); (28,70); (50, 80); (0, 69); (25, 30)];;

(* wybranie z listy xl elementow wedlug indeksow na liscie yl *)
let elements xl yl =
  let xl = Array.of_list xl in
  let yl = Array.of_list yl in
  let m = Array.length yl in
  let result_list = ref [] in
  for i = m-1 downto 0 do
    result_list := (xl.(yl.(i)))::(!result_list);
  done;
  !result_list;;


(* dla danej listy l nalezy zwrocic taka jej permutacje, by suma wartosci bezwzgldnych *)
(* roznic kolejnych par w tablicy byla jak najwieksza                                  *)
let przekladaniec l =
  let rosnaca = List.sort compare l in
  let malejaca = List.sort (fun a b -> compare b a) l in
  let n = List.length l in
  let stworz l1 l2 =
    let rec scan i l1 l2 wyn =
      if i > n then wyn else
        scan (i+1) l2 (List.tl l1) ((List.hd l1)::wyn)
    in
    scan 1 l1 l2 []
  in
  let kand_1 = stworz rosnaca malejaca in
  let kand_2 = stworz malejaca rosnaca in
  let policz l =
    let rec scan l wyn =
      match l with
      | v1::v2::_ -> scan (List.tl l) (wyn + abs (v1 - v2))
      | _ -> wyn
    in
    scan l 0
  in
  if policz kand_1 > policz kand_2 then kand_1 else kand_2;;


(* na monotonicznej liscie znajduje wartosc nablizsze co do wartosci bezwzglednej zeru *)
let blisko_zera a =
  let l = ref 0 in
  let r = ref (Array.length a) in
  let lv = a.(0) in
  let rv = a.(!r-1) in
  let min = ref max_int in
  while !l + 1 < !r do
    let m = (!l + !r) / 2 in
    print_int m;
    if abs (a.(m)) < !min then (min := abs (a.(m)));
    if a.(m) > 0
    then (if rv < lv then l := m else r := m)
    else (if rv < lv then r := m else l := m)
  done;
  !min;;


(* znajdz najdluzszy fragment scisle rosnacy na liscie                                 *)
let wzrost l =
  let l = List.rev l in
  let ((lv, curLen, curL), (len, longL)) =
    List.fold_left (fun ((lv, curLen, curL), (len, longL)) v ->
      if v < lv
      then ((v, curLen+1, v::curL), (len, longL))
      else (let (len, longL) = if curLen > len then (curLen, curL) else (len, longL) in
           ((v, 1, [v]), (len, longL)))      
    )
    ((max_int, 0, []), (0, [])) l in
  if curLen > len then curL else longL;;


(* dla danej tablicy permutacji liczb od 0 do n-1 znajdz najdluzszy cykl po indeksach *)
let cykl a =
  let n = Array.length a in
  let odwiedzone = Array.make n false in
  let przejdz_cykl i =
    let rec scan i dl =
      if odwiedzone.(i) then dl else (odwiedzone.(i) <- true; scan (a.(i)) (dl+1))
    in
    scan i 1
  in
  let max_v = ref 0 in
  for i = 0 to n-1 do
    if not (odwiedzone.(i))
    then (odwiedzone.(i) <- true; max_v := max (!max_v) (przejdz_cykl (a.(i))))
  done;
  !max_v;;

(* czy w tablicy dla ktorej ciag roznic x_i+1 - x_i jest rosnacy znajduja sie rozne elementy *)
let rozne a =
  let n = Array.length a in
  let roznice = Array.make (n-1) 0 in
  for i = 0 to n-2 do
    roznice.(i) <- a.(i+1) - a.(i)
  done;
  let l = ref 0 in
  let r = ref (n-1) in
  while !l + 1 < !r do
    let m = (!l + !r) / 2 in
    if roznice.(m) > 0 then r := m else l := m
  done; (* now we have that roznice.(l) < 0 and roznice.(l+1) > 0 *)
  let li = ref (!l) in
  let ri = ref (!l + 1) in
  let lsum = ref (abs (roznice.(!li))) in
  let rsum = ref (roznice.(!ri)) in
  ri := !ri + 1;
  li := !li -1;
  let czy_rozne = ref true in
  while !li > -1 && !r < n-1 do
    if !lsum = !rsum then (czy_rozne := false; li := -1) else
    if !lsum > !rsum then (rsum := !rsum + roznice.(!ri); ri := !ri + 1) else
      (lsum := !lsum + abs (roznice.(!li)); li := !li - 1)
  done;
  !czy_rozne;;

(* przestawia wartosci w niemalejacej tablicy w tski sposob, aby ciag |x_1|, ... ,|x_n| *)
(* byl niemalejacy                                                                      *)
let przestaw a =
  let n = Array.length a in
  let change_i = ref (-1) in
  let min_abs = ref (abs (a.(0))) in
  for i = 0 to n-1 do
    if abs (a.(i)) < !min_abs then (min_abs := abs (a.(i)); change_i := i)
  done;
  if !change_i = -1 then (
    if a.(0) < 0 then(
      let pom_a = Array.copy a in
      for i = 0 to n-1 do
        a.(i) <- pom_a.(n-1-i)
      done;
    )
  ) else(
    let pom_a = Array.copy a in
    let ri = ref (!change_i + 1) in
    let li = ref (!change_i - 1) in
    let i = ref 1 in
    a.(0) <- pom_a.(!change_i);
    print_int !change_i;
    while !i < n && !li > -1 && !ri < n do
      let next_one = if pom_a.(!ri) < abs (pom_a.(!li))
        then (ri := !ri + 1; pom_a.(!ri-1))
        else (li := !li - 1; pom_a.(!li + 1))
      in
      a.(!i) <- next_one;
      i := !i + 1;
    done;
    while !i < n && !li > -1 do
      a.(!i) <- pom_a.(!li);
      i := !i + 1;
      li := !li -1;
    done;
    while !i < n && !ri < n do
      a.(!i) <- pom_a.(!ri);
      i := !i + 1;
      ri := !ri + 1;
    done;
  );;

(* dla tablicy o nimalejacych wartosciach sprawdza czy wartosci |x_1|, ..., |x_n| *)
(* tworza ciag niemalejacy                                                        *)
let niemalejacy a =
  let n = Array.length a in
  if n < 2 then true else
    begin
      let wynik = ref true in
      for i = 0 to n-2 do
        wynik := !wynik && (abs (a.(i+1)) >= abs (a.(i)))
      done;
      !wynik
    end;;

let niemalejacy a =
  let n = Array.length a in
  let li = ref 0 in
  let ri = ref n in
  while !li + 1 < !ri do
    let mi = (!li + !ri) / 2 in
    if a.(mi) >= 0 then ri := mi else li := mi
  done; (* a.(!li) < 0 *)
  if !li >= 0 && a.(!li+1) = 0 then false else
    begin
      let wyn = ref true in
      for i = !li downto 0 do
        wyn := !wyn && (a.(!li) = a.(i))
      done;
      !wyn
    end;;


    
