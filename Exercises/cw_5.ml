let majority_easy l = List.nth (List.sort compare l) ((List.length l) / 2);;

(* sprawdzenie czy lista jest lista wiekszosciowa - jakis element jest ponad polowe razy *)
let majority l =
  let rec scan c k l =
    match l with
    | [] -> c
    | hd::tl ->
      if k = 0
      then scan hd 1 tl
      else if c <> hd
           then scan c (k-1) tl
           else scan c (k+1) tl
  in
  match l with
  | [] -> failwith "empty list"
  | hd::tl -> scan hd 1 tl;;

(* odracanie listy *)
let rev l =
  let rec scan l acc =
    match l  with
    | [] -> acc
    | hd::tl -> scan tl (hd::acc)
  in
  scan l [];;

(* zwraca najlepszy podzial listy w taki sposob, by zawierala permutacje indeksow *)
(* do danego miejsca w liscie numerujac ja od 1                                   *)
let partition l =
  let rec scan i x cl l =
    match l with
    | [] -> [rev cl]
    | hd::tl ->
      if i = x + 1
      then (rev cl)::(scan (i+1) hd [hd] tl)
      else scan (i+1) (max x hd) (hd::cl) tl
  in
  match l with
  | [] -> failwith "empty list"
  | hd::tl -> scan 1 hd [] l;;

(* sprawdzenie ktora trojka z listy spelnia nierownosc trojkata *)
let triple l =
  let rec loop_a la acc =
    match la with
    | [] -> acc
    | hda::tla -> let rec loop_b lb acc =
                    match lb with
                    | [] -> acc
                    | hdb::tlb -> let rec loop_c lc acc =
                                    match lc with
                                    | [] -> acc
                                    | hdc::tlc -> if hda + hdb > hdc
                                                  then loop_c tlc ((hda, hdb, hdc)::acc)
                                                  else acc
                      in
                      loop_c tlb acc
      in
      loop_b tla acc
  in
  loop_a l [];;
