(* tworzenie sygnatur modulow lokalnych *)
module type TYP =
sig
  type t (* cos jak interfejs *)
end

(* teraz ta sygnatura uzywana jest przy implementacji modulu *)
(* sygnatury sluza do "komunikacji" z funktorami             *)
module Int  : TYP =
struct
  type t = int
end

(* przyklad polgrupy z prawdziwego zdarzenia                 *)
module type POLGRUPA =
sig
  include TYP
  val jeden : t
  val ( * ) : t -> t -> t          (* mnozenie w tej grupie  *)
end

(* teraz bedziemy tworzyc polgrupe funkcji o elemencie       *)
(* neutralnym id i zlozeniem funkcji                         *)
module type POLGRUPA_FUNKCJI =
sig
  type dz (* typ dziedziny                                   *)
  include POLGRUPA with type t = dz -> dz
end

(* teraz nalezy zaimplementowac funktor z pewnej dziedziny   *)
module PolgrupaFunkcji (Dz : TYP)
  : (POLGRUPA_FUNKCJI with type dz := Dz.t) = (* ma byc with *)
struct
  type dz = Dz.t
  type t = dz -> dz
  let jeden = (fun (x : dz) -> x)
  let ( * ) (f : t) (g : t) x = f (g x)
end

(* implementacja macierzy 2x2 z wykorzystanie funktora      *)

(* bedziemy wykorzystywac skladanie macierzy przy skladaniu *)
(* funkcji                                                  *)
module type SKLADANIE =
sig
  include TYP
  val zloz : (t -> t) -> (t -> t) -> (t -> t)
end

(* implementacja polgrupy funkcji ze skladaniem            *)
module PolgrupaFunkcjiZeSkladaniem (Dz : SKLADANIE)
  : (POLGRUPA_FUNKCJI with type dz := Dz.t) =
struct
  include PolgrupaFunkcji (Dz)
  let ( * ) = Dz.zloz
end

(* zdefiniowanie sygantury polpierscienia                 *)
module type POLPIERSCIEN =
sig
  include POLGRUPA
  val zero : t
  val (+) : t -> t -> t
end

(* i nastepnie realizacja sygantury dla intow            *)
module Int : (POLPIERSCIEN with type t = int) =
struct
  type t = int
  let zero = 0
  let jeden = 1
  let (+) = (+)
  let ( * ) = ( * )
end

(*
| a b |   | 1 |   | a |
| c d | * | 0 | = | c |
*)
module Skladanie2x2 (P : POLPIERSCIEN)
  : (SKLADANIE with type t := P.t * P.t) =
struct
  type t = P.t * P.t
  let zloz f g =
    let open P in(
    let (a, c) = f (g (jeden, zero)) in
    let (b, d) = f (g (jeden, zero)) in
    (fun (x, y) -> (a*x + b*y, c*x + d*y)))
end

module SkladanieInt2x2 = Skladanie2x2 (Int)
   
module Int2x2 = PolgrupaFunkcjiZeSkladaniem (SkladanieInt2x2)

(* teraz potrzebujemy polgrupy, ktora bedzie miala szybkie *)
(* potegowanie                                             *)
module type POLGRUPA_Z_POTEGA =
sig
  include POLGRUPA
  val ( ** ) : t -> int -> t
end

(* i teraz implemetujemy te syganture                     *)
module PolgrupaZPotegaBinarna (G : POLGRUPA)
  : (POLGRUPA_Z_POTEGA with type t = G.t) =
struct
  include G
  let ( ** ) (a : t) n = (* liczymy tutaj a^n *)
    let rec pot a b n =
      if n = 0 then a else
        pot (if n mod 2 = 0 then a else a*b) (b*b) (n/2)
    in
    pot jeden a n
end

(* funktor do tworza polgrup z potega *)
module Polgrupa2x2ZPotegaBinarna (P : POLPIERSCIEN) =
  PolgrupaZPotegaBinarna (
    PolgrupaFunkcjiZeSkladaniem (
      Skladanie2x2(P)))

(* i teraz mozemy tworzyc latwo pierscienie             *)
module Int2x2Pot = Polgrupa2x2ZPotegaBinarna (Int)

(* 
teraz bedziemy mogli szybko liczyc liczby fibbonacciego
zauwazmy bowiem, ze
| 0 1 |   | F_n   |   | F_n+1 |
| 1 1 | * | F_n+1 | = | F_n+2 |
*)
let fib n =
  let f (x, y) = (y, x+y) in
  Int2x2Pot.( (f ** n) (zero, jeden) |> fst )

let fib_mod n p = (* F_n mod p *)
  let module P = Polgrupa2x2ZPotegaBinarna(
    struct
      include Int
      let (+) x y = (x + y) mod p
      let ( * ) x y = (x * y) mod p
    end)
  in
  P.(let f (x, y) = (y, x + y) in
    (f ** n) (0, 1) |> fst)
