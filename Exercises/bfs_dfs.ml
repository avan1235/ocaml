let print_matrix mt =
  let m = Array.length mt in
  let n = Array.length mt.(0) in
  for i = 0 to m-1 do
    (print_string "[";
    for j = 0 to n-2 do
      (print_int mt.(i).(j);
       print_string " | ";)
    done;
     print_int mt.(i).(n-1);
     print_string "]\n";
    )
    done;;

let print_hashtbl h =
  Hashtbl.iter (fun k v ->
      print_string "key: ";
      print_int k;
      print_string " - value: ";
      print_int v;
      print_newline ();
    ) h;
  print_newline ();;


let print_levels lp =
  let print_level l v =
    begin
      print_string "level ";
      print_int l;
      print_string ": ";
      print_int v;
      print_newline ();
    end
  in
  List.iter (fun (n, l) -> print_level l n)
    (List.sort (fun (a,b) (x,y) ->
         if compare b y = 0 then compare a x else compare b y) lp);
  print_newline ();;

(* bfs for list of pairs with connections *)
let bfs l s =
  let n = List.length l in
  let adj = Hashtbl.create n in
  List.iter (fun (u, l) -> Hashtbl.add adj u l
            ) l;
  let level =  Hashtbl.create n in
  Hashtbl.iter (fun k v ->
      if not (Hashtbl.mem level k) then Hashtbl.add level k (-1);
      List.iter (fun u
                  -> if not (Hashtbl.mem level u) then Hashtbl.add level u (-1))
        v) adj;
  let act_lvl = ref 0 in
  let stack = Stack.create () in
  Stack.push s stack;
  while not (Stack.is_empty stack) do(
    let act_list = ref [] in
    while not (Stack.is_empty stack) do
      begin
        let act_n = Stack.pop stack in
        Hashtbl.replace level act_n !act_lvl;
        act_list := act_n::(!act_list);
      end
    done;
    incr act_lvl;
    List.iter (fun w ->
        let children = if Hashtbl.mem adj w then Hashtbl.find adj w else [] in
        List.iter (fun u -> if (Hashtbl.find level u) = -1 then Stack.push u stack) children) !act_list;
  )
  done;
  Seq.fold_left (fun a v -> v::a) [] (Hashtbl.to_seq level);;

(* dfs for list of pair with connections *)
let dfs l s =
  let n = List.length l in
  let adj = Hashtbl.create n in
  List.iter (fun (n, l) -> Hashtbl.add adj n l) l;
  let level  = Hashtbl.create n in
  Hashtbl.iter (fun k v ->
      if not (Hashtbl.mem level k) then Hashtbl.add level k (-1);
      List.iter (fun u ->
        if not (Hashtbl.mem level u) then Hashtbl.add level u (-1)) v) adj;
  let stack = Stack.create () in
  Stack.push s stack;
  let act_lvl = ref 0 in
  while not (Stack.is_empty stack) do(
    let act_n = Stack.pop stack in
    Hashtbl.replace level act_n !act_lvl;
    incr act_lvl;
    List.iter (fun n -> if Hashtbl.find level n = -1 then Stack.push n stack)
      (if Hashtbl.mem adj act_n then Hashtbl.find adj act_n else [])
  )
  done;
  Seq.fold_left (fun a v -> v::a) [] (Hashtbl.to_seq level);;
  

let dir_graph = [(1,[2;3;4]); (2,[3]); (3,[5;6]); (6,[2;7])];;
print_levels (bfs dir_graph 1);;
print_levels (dfs dir_graph 1);;


(* bfs algorithm for the matrix of connections between nodes *)
let bfs_matrix m s =
  let s = s-1 in
  let stck = Stack.create () in
  Stack.push s stck;
  let n = Array.length m in
  let level = Array.make n (-1) in
  let act_lvl = ref 0 in
  while not (Stack.is_empty stck) do
    begin
      let act_stc = Stack.create () in
      while not (Stack.is_empty stck) do(
        let act_n = Stack.pop stck in
        level.(act_n) <- !act_lvl;
        Stack.push act_n act_stc;
      )
      done;
      incr act_lvl;
      Stack.iter (fun u ->
          for i = 0 to n-1 do
            if m.(u).(i) > 0 && level.(i) = -1 then Stack.push i stck
          done;
        ) act_stc;
    end
  done;
  Seq.fold_left (fun a (n, l) -> (n+1, l)::a) [] (Array.to_seqi level);;


let dfs_matrix m s =
  let s = s-1 in
  let n = Array.length m in
  let level = Array.make n (-1) in
  let stck = Stack.create () in
  let act_lvl = ref 0 in
  Stack.push s stck;
  while not (Stack.is_empty stck) do
    begin
      let act_n = Stack.pop stck in
      level.(act_n) <- !act_lvl;
      incr act_lvl;
      for i = 0 to n-1 do
        if m.(act_n).(i) > 0 && level.(i) = -1 then (Stack.push i stck; level.(i) <- (-2))
      done;
      print_newline ();
    end
  done;
  Seq.fold_left (fun a (n, l) -> (n+1, l)::a) [] (Array.to_seqi level);;
    

let graph = [|
  [|0;1;1;1;0;0;0|];
  [|1;0;1;0;0;1;0|];
  [|1;1;0;0;1;1;0|];
  [|1;0;0;0;0;0;0|];
  [|0;0;1;0;0;0;0|];
  [|0;1;1;0;0;0;1|];
  [|0;0;0;0;0;1;0|];
|];;

print_levels (bfs_matrix graph 1);;
print_levels (bfs_matrix graph 6);;

print_levels (dfs_matrix graph 1);;
print_levels (dfs_matrix graph 6);;
