(* przelewanka : (int * int) array → int, ktora majac dana tablice par liczb [|(x1, y1); (x2, y2); ...; (xn, yn)|] *)
(* wyznaczy minimalna liczbe czynnosci potrzebnych do uzyskania opisanej przez nie sytuacji.                       *)
(* Jezeli jej uzyskanie nie jest mozliwe, to poprawnym wynikiem jest -1.                                           *)
(* mozemy zalozyc, ze 0 ≤ n, oraz 0 ≤ yi ≤ xi dla i = 1, 2, ..., n                                                 *)

val przelewanka : (int * int) array -> int
