(*
 * PSet - Polymorphic sets
 * Copyright (C) 1996-2003 Xavier Leroy, Nicolas Cannasse, Markus Mottl
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version,
 * with the special exception on linking described in file LICENSE.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *)

(****************************************************************************************)
(********************************* MODUL INTERVAL SET ***********************************)
(****************************************************************************************)

(*--------------------------------------------------------------------------------------*)
(* Autor: Maciej Procyk                                                                 *)
(* Recenzujacy: Pawel Czajka                                                            *)
(*--------------------------------------------------------------------------------------*)

(*--------------------------------------------------------------------------------------*)
(* Reprezentacja drzewa przedzialow liczb calkowitych zaimplementowana na podstawie     *)
(* modulu pSet                                                                          *)
(* Przedzialy przechowywane sa w drzewie AVL w taki sposob, ze roznica koncow           *)
(* zadnch dwoch przedzialow nie jest rowna jeden - przedzialy np [1, 3] i [4, 6] sa     *)
(* automatycznie w drzeiwie zamieniane na przedzial [1, 6]                              *)
(*--------------------------------------------------------------------------------------*)


(****************************************************************************************)
(*********************************** DEKLARACJA TYPU ************************************)
(****************************************************************************************)

(* Typ pomocniczy przechowujacy przedzial liczb calkowitych [a, b] gdzie a <= b         *)
type intvl =
  int * int


(* Typ wlasciwy przechowujacy zbior przedzialow badz puste drzewo Empty                 *)
(* drzewo przechowywane w nastepujacej postaci                                          *)
(* Node(lewe_poddrzewo, przedzial intvl, prawe_poddrzewo, wysokosc_drzewa,              *)
(* liczba_elementow_calkowitych_drzewa)                                                 *)
(* maksymalna roznca wysokosci poddrzew to 2                                            *)
(* w lewym poddrzewie trzymane sa mniejsze wartosci, w prawym wieksze niz w przedziale  *)  
type t =
  | Empty
  | Node of t * intvl * t * int * int 


(****************************************************************************************)
(********************************* DEKLARACJA WYJATKOW **********************************)
(****************************************************************************************)

(* wyjatek uzywany gdy rozwazany przedzial jest zle zbudowany                           *)
exception Bad_interval

(* wyjatek wznoszony gdy do funkcji zostaly podane nieprawidlowe argumenty              *)
exception Bad_arguments


(****************************************************************************************)
(*************************************** FUNKCJE ****************************************)
(****************************************************************************************)

(* zwraca v1 + v2 lub max_int gdy suma przekracza max_int                               *)
(* dziala prawidlowo dla nieujemnych v1 v2                                              *)
let plus v1 v2 =
  if v1 + v2 >= 0 then v1 + v2 else max_int


(* bezpieczne zmniejszanie liczby o 1 - dla min_int wynikiem jest min_int               *)
let dec v =
  if v > min_int then v - 1 else min_int


(* bezpieczne zwiekszanie liczby o 1 - dla max_int wynikiem jest max_int                *)
let inc v =
  if v < max_int then v + 1 else max_int

(* wyznacza liczbe liczb calkowitych w przedziale                                       *)
let intvl_len (l, r) =
  if l > r then raise Bad_interval else
  if r - l < 0 then max_int else
  plus (r - l) 1


(* sprawdza czy n jest w przedziale [lv, rv] i mowi po ktorej stronie przedzialu  n     *)
(* jest gdy do przedzialu nie nalezy - zwraca 0 gdy n nalezy do przedzialu, -1 gdy jest *)
(* od niego mniejsze i 1 gdy jest od niego wieksze                                      *)    
let is_in_intvl n (lv, rv) =
  if n >= lv && n <= rv then 0 else
  if n < lv then -1 else
  1 (* if n > rv *)


(* zwraca licznosc (liczbe liczb calkowitych) zbioru podanego jako argument             *)
let cardlty = function
  | Node (_, _, _, _, c) -> c
  | Empty -> 0


(* zwraca wysokosc drzewa podanego jako argument                                        *)
let height = function
  | Node (_, _, _, h, _) -> h
  | Empty -> 0


(* make tworzy drzewo o wartosci w wezle k i odpowiednio zawieszonym lewym l i prawym r *)
(* poddrzewem                                                                           *)
(* by poprawnie dzialal wartosci w l musza byc mniejsze od tych w k, a wartosci w r     *)
(* wieksze oraz roznica wysokosci l i r nie moze byc wieksza niz 2                      *)
let make l k r = Node (l, k, r, inc (max (height l) (height r)),
                      plus (plus (cardlty l) (cardlty r)) (intvl_len k))


(* laczy drzewo l z drzewem r dodajac wartosc k ktora jest rozlaczna od wszystkich w    *)
(* danych poddrzewach                                                                   *)    
(* na wejciu dane wartosci w l musza byc mniejsze od tych w k, a w r wieksze            *)
(* maksymalna roznica wysokosci, jaka moze zostac poprawiona to 3                       *)    
let rec bal l k r =
  let hl = height l in
  let hr = height r in
  if hl > hr + 2 then
    match l with
    | Node (ll, lk, lr, _, _) ->
      if height ll >= height lr
      then make ll lk (bal lr k r)
        else(
          match lr with
          | Node (lrl, lrk, lrr, _, _) ->
              make (make ll lk lrl) lrk (bal lrr k r)
          | Empty -> assert false
        )
    | Empty -> assert false
  else if hr > hl + 2 then
    match r with
    | Node (rl, rk, rr, _, _) ->
      if height rr >= height rl
      then make (bal l k rl) rk rr
        else(
          match rl with
          | Node (rll, rlk, rlr, _, _) ->
              make (bal l k rll) rlk (bal rlr rk rr)
          | Empty -> assert false
        )
    | Empty -> assert false
  else make l k r


(* funkcja dodaje do seta przedzial, ktory na pewno nie laczy innych a jedynie jest     *)
(* mozliwe ze jest identyczny z innym przedzialem w secie                               *)
let rec add_sep ((lv, rv) as intvl) = function
  | Node (l, (nl, nr), r, h, c) ->
    if lv = nl && rv = nr then Node (l, intvl, r, h, c) else
    if rv < dec nl then bal (add_sep intvl l) (nl, nr) r else
    if lv > inc nr then bal l (nl, nr) (add_sep intvl r) else
      raise Bad_arguments
  | Empty -> make Empty intvl Empty


(* funkcja join dodaje v do drzewa l o wartosciach mniejszych od v i drzewa r o         *)
(* wartosciach wiekszych od v, dbajac o to, by wysokosci laczonych poddrzew roznily     *)
(* sie w wyniku maksymalnie o 2                                                         *)              
let rec join l v r =
  match l, r with
  | Empty, _ -> add_sep v r
  | _, Empty -> add_sep v l
  | Node(ll, lv, lr, lh, _), Node(rl, rv, rr, rh, _) ->
      if lh > rh + 2 then bal ll lv (join lr v r) else
      if rh > lh + 2 then bal (join l v rl) rv rr else
      make l v r


(* zwraca liczbe liczb calkowitych w drzewie s mniejszych rownych n                     *)
let below n s =
  let rec scan_subtree acc = function
    | Node (l, (lv, rv), r, _, _) ->
      if n >= lv && n <= rv then plus acc (plus (intvl_len (lv, n)) (cardlty l)) else
      if n < lv then scan_subtree acc l else
     (* if n > rv *)
        scan_subtree (plus (plus acc (intvl_len (lv, rv))) (cardlty l)) r
    | Empty -> acc
  in
  scan_subtree 0 s


(* zwraca minimalny przedzial z drzewa przedzialow                                      *)
let rec min_intvl = function
  | Node (Empty, k, _, _, _) -> k
  | Node (l, _, _, _, _) -> min_intvl l
  | Empty -> raise Not_found


(* zwraca maksymalny przedzial z drzewa przedzialow                                     *)
let rec max_intvl = function
  | Node (_, k, Empty, _, _) -> k
  | Node (_, _, r, _, _) -> max_intvl r
  | Empty -> raise Not_found


(* zwraca drzewo z usunietym minimalnym przedzialem                                     *)
let rec remove_min_intvl = function
  | Node (Empty, _, r, _, _) -> r
  | Node (l, k, r, _, _) -> bal (remove_min_intvl l) k r
  | Empty -> invalid_arg "ISet.remove_min_elt"


(* zwraca drzewo z usunietym maksymalnym przedzialem                                    *)
let rec remove_max_intvl = function
  | Node (l, _, Empty, _, _) -> l
  | Node (l, k, r, _, _) -> bal l k (remove_max_intvl r)
  | Empty -> invalid_arg "ISet.remove_min_elt"


(* zlacza dwa drzewa w jedno - roznica wysokosci drzew moze byc wieksza niz 2           *)
let merge t1 t2 =
  match t1, t2 with
  | Empty, _ -> t2
  | _, Empty -> t1
  | _ ->
      let k = min_intvl t2 in
      join t1 k (remove_min_intvl t2)


(* sprawdza czy x nalezy do ktoregos z przedzialow w set i zwraca true/false            *)
let mem x set =
  let rec loop = function
    | Node (l, k, r, _, _) ->
        let b = is_in_intvl x k in
        b = 0 || loop (if b < 0 then l else r)
    | Empty -> false in
  loop set


(* zwraca z danego setu nowy set zawierajacy jedynie wartosci mniejsze od n             *)
let less_set n set =
  let rec scan s acc =
    match s with
    | Node(l, (lv, rv), r, _, _) ->
      let old_l =
        if n < lv then scan l acc else
        if n > rv then scan r acc else
        acc
      in
      let new_l =
        if lv > n then old_l else
        if lv == n then merge old_l l else
        join l (lv, min (dec n) rv) old_l
      in new_l
    | Empty -> acc
  in
  scan set Empty


(* zwraca z danego setu nowy set zawierajcy jedynie wartosci wieksze od n               *)
let great_set n set =
  let rec scan s acc =
    match s with
    | Node(l, (lv, rv), r, _, _) ->
      let old_r =
        if n < rv then scan l acc else
        if n > rv then scan r acc else
        acc
      in
      let new_r =
        if rv < n then old_r else
        if rv == n then merge old_r r else
        join old_r (max lv (inc n), rv) r
      in new_r
    | Empty -> acc
  in
  scan set Empty


(* dla danego setu i n zwraca trojke w postaci (podset_o_wartosciach_mniejszych_niz_n, *)
(* czy_n_nalezy_do_seta, podset_o_wartosciach_wiekszych_niz_n)                         *)    
let split n set =
  (less_set n set, mem n set, great_set n set)


(* konstruktor pustego drzewa                                                           *)
let empty = Empty


(* sprawdza czy dane drzewo jest puste                                                  *)
let is_empty set = 
  set = Empty


(* dodawanie przedzialu intvl do drzewa danego w argumencie z wykorzystaniem operacji   *)
(* split, tak by szybko oddzielic powtarzajace sie liczby w przedziale i drzewie        *)
let add ((l, r) as intvl) set =
  let (ls, _, rs) = split l set in
  let rs = great_set r rs in
  let nl, ls =
    match not(is_empty ls) && mem (dec l) ls with
    | true -> fst (max_intvl ls), remove_max_intvl ls
    | false -> fst intvl, ls
  and nr, rs =
    match not(is_empty rs) && mem (inc r) rs with
    | true -> snd (min_intvl rs), remove_min_intvl rs
    | false -> snd intvl, rs
  in
  join ls (nl, nr) rs


(* usuwanie przedzialu [l, r] z seta                                                   *)
let remove (l, r) set =
  let (nl, _, temp_r) = split l set in
  let nr = great_set r temp_r in
  merge nl nr


(* stosuje funkcje f: intvl -> () dla kazdego przedzialu w kolejnosci rosnacej         *)
let iter f set =
  let rec loop = function
    | Empty -> ()
    | Node (l, k, r, _, _) -> loop l; f k; loop r in
  loop set


(* przechodzi przez drzewo przedzialow i dla kazdego przedzialu wykonuje f zapisujac   *)
(* wynik do akumulatora acc                                                            *)
let fold f set acc =
  let rec loop acc = function
    | Empty -> acc
    | Node (l, k, r, _, _) ->
          loop (f k (loop acc l)) r in
  loop acc set

(* zwraca liste przedzialow w drzewie w kolejnosci rosnacej                            *)
let elements set = 
  let rec loop acc = function
    | Empty -> acc
    | Node(l, k, r, _, _) -> loop (k :: loop acc r) l in
  loop [] set
