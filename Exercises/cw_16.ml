(* Implementacja drzew find-union.       *)
(* make_set : 'a -> 'a set               *)
(* find : 'a set -> 'a                   *)
(* equivalent : 'a set -> 'a set -> bool *)
(* union : 'a set -> 'a set -> unit      *)
(* n_of_sets : unit -> int               *)

module type FIND_UNION = sig
  (* Typ klas elementow typu 'a. *)
  type 'a set

  (* Tworzy nowa klase złozona tylko z danego elementu. *)
  val make_set : 'a -> 'a set

  (* Znajduje reprezentanta danej klasy. *)
  val find : 'a set -> 'a

  (* Sprawdza, czy dwa elementy sa rownowazne. *)
  val equivalent : 'a set -> 'a set -> bool 

  (* Scala dwie dane (rozlaczne) klasy. *)
  val union : 'a set -> 'a set -> unit

  (* Lista elementow klasy. *)
  val elements : 'a set -> 'a list

  (* Liczba wszystkich klas. *)
  val n_of_sets : unit-> int
end;;


module Find_Union : FIND_UNION = struct
  (* Typ klas elementów typu 'a. *)
  type 'a set = { 
    elem         : 'a;           (* Element klasy. *)
    up           : 'a set ref;   (* Przodek w drzewie find-union. *)
    mutable rank : int;          (* Ranga w drzewie find-union. *)
    mutable next : 'a set list   (* Lista potomków w pewnym drzewie rozpinającym klasę. *)
  }

  (* Licznik klas. *)
  let sets_counter = ref 0

  (* Liczba wszystkich klas. *)
  let n_of_sets () = !sets_counter

  (* Tworzy nowa klase zlozona tylko z danego elementu. *)
  let make_set x = 
    let rec v = { elem = x; up = ref v; rank = 0; next = [] }
    in begin
      sets_counter := !sets_counter + 1;
      v
    end

  (* Znajduje korzen drzewa, kompresujac sciezke. *)
  let rec go_up s = 
    if s == !(s.up) then s 
      else begin
	s.up := go_up !(s.up);
	!(s.up)
      end

  (* Znajduje reprezentanta danej klasy. *)
  let find s = 
    (go_up s).elem
	
  (* Sprawdza, czy dwa elementy sa rownowazne. *)
  let equivalent s1 s2 =
    go_up s1 == go_up s2

  (* Scala dwie dane (rozlaczne) klasy. *)
  let union x y = 
    let fx = go_up x 
    and fy = go_up y
    in
      if not (fx == fy) then begin
	if fy.rank > fx.rank then begin
	  fx.up := fy;
          fy.next <- fx :: fy.next
        end else begin
	  fy.up := fx;
	  fx.next <- fy :: fx.next;
	  if fx.rank = fy.rank then fx.rank <- fy.rank + 1
	end;
	sets_counter := !sets_counter - 1
      end
  
  (* Lista elementow klasy. *)
  let elements s = 
    let acc = ref []
    in
      let rec traverse s1 = 
	begin
	  acc := s1.elem :: !acc;
	  List.iter traverse s1.next
	end
      in begin
	traverse (go_up s);
	!acc
      end
end;;



(* dysponujemy skarbonkami o numerach od 0 do n-1, a w tablicy a na indeksie i *)
(* czyli w a.(i) znajduje sie numer skarbonki, do ktorej jest kluczyk w i-tej  *)
(* skarbonce - naszym celem jest znalezienie jak najmniejszej liczby skarbonek *)
(* potrzebnych do zbicia, aby otworzyć wszystkie skarbonki                     *)

let skarbonki a =
  let n = Array.length a in
  let s = Array.init n (fun i -> Find_Union.make_set i) in
  for i = 0 to n-1 do
    Find_Union.union s.(i) s.(a.(i));
  done;
  Find_Union.n_of_sets ();;


(* kolorowanie wiezy - jezeli dwie wieze znajduja sie w tym samym wierszu lub tej samej kolumnie *)
(* to musza zostac pokolorowane tym samym kolorem - pytamy ile maksymalnie kolorow mozemu uzyc   *)
(* w rozwiazaniu najpierw sortujemy dane po kolumnach i pozniej po wierszach i robimy unie       *)

let kolory l =
  let a = Array.of_list (List.map (fun x -> (x, (Find_Union.make_set x))) l) in
  let n = Array.length a in
  let pair_towers sel =
    Array.sort (fun a b -> compare (sel (fst a)) (sel (fst b))) a;
    for i = 0 to n-2 do
      if sel (fst (a.(i))) = sel (fst (a.(i+1)))
      then (Find_Union.union (snd (a.(i))) (snd (a.(i+1))));
    done;
  in
  pair_towers fst;
  pair_towers snd;
  Find_Union.n_of_sets ();;


(* rozwiazanie bardziej funkcujne problemu wiez *)
let kolory l =
  let l = List.map (fun x -> (x, (Find_Union.make_set x))) l in
  let rec pair_towers l sel =
    let l = List.sort (fun a b -> compare (sel (fst a)) (sel (fst b))) l in
    let rec scan l =
      match l with
      | (ta, sa)::(tb, sb)::_ ->
        if sel ta = sel tb
        then (Find_Union.union sa sb);
        scan (List.tl l);
      | _ -> ()
    in
    scan l
  in
  pair_towers l fst;
  pair_towers l snd;
  Find_Union.n_of_sets ();;

            
(* zadanie malpki polega na tym, ze na drzewie wisi n malpek, dane sa dwie tablice *)
(* ogonki.(i) numer malpki, ktora trzyma i-ta za ogon                              *)
(* puszczone.(t) - nr malpki puszczonej w chwili                                   *)
let malpki ogonki puszczone =
  let n = Array.length ogonki in
  let m = Array.length puszczone in
  let s = Array.init n (fun i -> Find_Union.make_set i) in
  let nie_puszczone = Hashtbl.create n in
  let czas = Array.make n (-1) in
  for i = 0 to n-1 do
    Hashtbl.add nie_puszczone i true
  done;
  for i = 0 to m-1 do
    Hashtbl.replace nie_puszczone i false
  done;
  Hashtbl.iter (fun k b -> if b then Find_Union.union s.(k) s.(ogonki.(k))) nie_puszczone;
  for t = n-2 to 0 do
    let i = puszczone.(t) in
    let j = ogonki.(i) in
    (
      if Find_Union.equivalent s.(0) s.(j)
      then List.iter (fun k -> czas.(k) <- t) (Find_Union.elements s.(i));
      Find_Union.union s.(i) s.(j)
    )
  done;
  czas
