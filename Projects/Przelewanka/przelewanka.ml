(********************************************************************************************)
(***************************************** PRZELEWANKA **************************************)
(********************************************************************************************)

(*------------------------------------------------------------------------------------------*)
(* Author: Maciej Procyk                                                                    *)
(* Reviewer: Marcel Opiatowski                                                              *)
(*------------------------------------------------------------------------------------------*)


(********************************************************************************************)
(****************************************** TYPES *******************************************)
(********************************************************************************************)

(* type to describe the current state by the configuration in array and sum of water        *)
type state = {vol: int array; (* amount of water in each cup in given configuration         *)
              sum: int}       (* total amount of water in all cups in given configuration   *)


(********************************************************************************************)
(**************************************** CONSTANTS *****************************************)
(********************************************************************************************)

(* constants used to change the performace of solving algorithm manipulating datastructure  *)
let min_hashtbl_init_size = 1000
let max_hashtbl_init_size = 10000000


(********************************************************************************************)
(*************************************** EXCEPTIONS *****************************************)
(********************************************************************************************)

(* exception raised when the final configuration has been found                             *)
exception Found_result of int


(********************************************************************************************)
(**************************************** FUNCTIONS *****************************************)
(********************************************************************************************)

(* greatest common divider for two numbers a >= 0 and b >=0                                 *)
let rec gcd a b =
  if b = 0 then a else gcd b (a mod b)


(* greatest common divisor for non-negative numbers in array                               *)
let array_gcd a =
  let n = Array.length a in
  if n = 0 then failwith "Empty array. Cannot give the gcd of its elemetns."
  else Array.fold_left (fun a v -> gcd a v) a.(0) a


(* returns false when it is imposible to end with given configuration because the greatest *)
(* common divisior of the capacities doesn't divide some goal capacity                     *)
let heur_gcd c g =
  let d = array_gcd c in
  Array.fold_left (fun b v -> b && (v mod d = 0)) true g 


(* returns false when it is imposible to end with given configuration because none of the  *)
(* goal capacities is zero nor any goal capacity is full                                   *)
let heur_end_state c g =
  let n = Array.length c in
  let res = ref false in
  for i = 0 to n-1 do
    if g.(i) = 0 || c.(i) = g.(i) then res := true;
  done;
  !res


(* check both heurisctics and returns true if the problem has to be solved using           *)
(* backtracking method                                                                     *)
let pass_heur c g =
  (heur_end_state c g) && (heur_gcd c g)


(* filter the capacities equal to zero in given configuration which are useless and create *)
(* smaller set of data in some cases                                                       *)
let init_filter a =
  (Array.fold_left (fun a (c, g) -> if c > 0 then (c, g)::a else a) [] a) |> Array.of_list


(* returns the sum of the elements in int array                                            *)
let sum_array a =
  Array.fold_left (+) 0 a


(* returns the product of the elements in int array                                        *)
let prod_array a =
  Array.fold_left ( * ) 1 a


(* checks if the given status is equal to goal status g                                    *)
let is_final sts g g_sum =
  (g_sum = sts.sum) &&
  begin
    let res = ref true in
    Array.iter2 (fun g v ->  res := (g = v) && !res) g sts.vol;
    !res
  end  


(* adds to queue q new possible states when pouring water out or in some cups              *)
(* every time checks the state if it is already final state - if yes then breaks the loop  *)
(* by raising the exception Found_result                                                   *)
let pour sts q fun_check fun_change_vol fun_change_sum n g g_sum levels lvl =
  for i = 0 to n-1 do
    if fun_check i  
    then
      begin
        let cur_sum = fun_change_sum i in
        let n_sts = {vol = Array.copy sts.vol; sum = cur_sum} in
        n_sts.vol.(i) <- fun_change_vol i;
        if is_final n_sts g g_sum then raise (Found_result lvl) else
        if not (Hashtbl.mem levels n_sts) then
          begin
            Hashtbl.add levels n_sts lvl;
            Queue.push n_sts q;           
          end
      end
  done;
  ()  
  

(* uses the pour function to simulate pouring water in the cups                           *)
(* for given current status sts add to the queue q new possible states                    *)
(* stops when the final configuration has been found                                      *)
let pour_in sts q n g g_sum c levels lvl =
  pour sts q
    (fun i -> c.(i) > sts.vol.(i))            (* fun_check                                *)
    (fun i -> c.(i))                          (* fun_change_vol                           *)
    (fun i -> sts.sum + c.(i) - sts.vol.(i))  (* fun_change_sum                           *)
    n g g_sum levels lvl


(* uses the pour function to simulate pouring water out of the cups                       *)
(* for given current status sts add to the queue q new possible states                    *)
(* stops when the final configuration has been found                                      *)
let pour_out sts q n g g_sum levels lvl =
  pour sts q
    (fun i -> sts.vol.(i) > 0)                (* fun_check                                *)
    (fun _ -> 0)                              (* fun_change_vol                           *)
    (fun i -> sts.sum - sts.vol.(i))          (* fun_change_sum                           *)
    n g g_sum levels lvl


(* tries to create new status by pouring water from one cup to another one                *)
(* for given current status sts add to the queue q new possible states                    *)
(* stops when the final configuration has been found                                      *)
let pour_out_in sts q n g g_sum c levels lvl =
  (* pour water from i to j if it is possible                                             *)
  for i = 0 to n-1 do
    for j = 0 to n-1 do
      if i <> j && sts.vol.(i) > 0 && sts.vol.(j) < c.(j) then
        begin
          let free_space = c.(j) - sts.vol.(j) in
          let n_sts = {vol = Array.copy sts.vol; sum = sts.sum} in
          n_sts.vol.(i) <- if sts.vol.(i) <= free_space then 0 else sts.vol.(i) - free_space;
          n_sts.vol.(j) <- if sts.vol.(i) <= free_space then sts.vol.(j) + sts.vol.(i) else c.(j);
          if is_final n_sts g g_sum then raise (Found_result lvl) else
          if not (Hashtbl.mem levels n_sts) then
            begin
              Hashtbl.add levels n_sts lvl;
              Queue.push n_sts q;
            end          
        end
    done; 
  done;
  ()


(* uses all methods of finding new configurations to add all possible states from state   *)
(* sts to the queue q                                                                     *)
(* raises exception Found_result when any function of pouring raise this exception so it  *)
(* found the final configuration                                                          *)
let create_new_children sts q n g g_sum c levels =
  let lvl = 1 + Hashtbl.find levels sts in
  pour_out    sts q n g g_sum levels lvl;
  pour_in     sts q n g g_sum c levels lvl;
  pour_out_in sts q n g g_sum c levels lvl;
  ()


let przelewanka a =
  let a = init_filter a in
  let n = Array.length a in
  let cap = Array.init n (fun i -> fst a.(i)) in
  let goal = Array.init n (fun i -> snd a.(i)) in
  if n = 0 then 0
  else if n = 1 then
    begin
      if goal.(0) = 0 then 0 else
      if goal.(0) = cap.(0) then 1 else -1
    end
  else if not (pass_heur cap goal) then -1
  else
    begin
      let cap_prod = prod_array cap in
      let goal_sum = sum_array goal in
      let levels = Hashtbl.create (max min_hashtbl_init_size
                                      (min max_hashtbl_init_size cap_prod)) in
      let init_sts = {vol = Array.make n 0; sum = 0} in
      let has_result = ref (is_final init_sts goal goal_sum) in
      let res = ref (if !has_result then 0 else -1) in
      let q = Queue.create () in
      Hashtbl.add levels init_sts 0;
      Queue.add init_sts q;
      while not (!has_result) && not (Queue.is_empty q) do
        begin
          let cur_sts = Queue.take q in
          try
            create_new_children cur_sts q n goal goal_sum cap levels
          with
          | Found_result (lvl) ->
            begin
              has_result := true;
              res := lvl;
            end      
        end
      done;
      !res
    end
