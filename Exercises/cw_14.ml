(*
dla danej listy [x_1, x_2, ... x_n] przyjmujemy ze x_0 = +oo
-> [k_1, k_2, ..., k_n] - k_i - najwieksze takie, że x_i = max(x_(i-k_i+1), ..., x_i) 
wzraca najwieksza dlugosc takiego spojnego podciagu wartosci w tablicy, ze ostatni element
tego podciagu jest najwiekszym elementem danego podciagu
*)

let katastrofy l =
  let rec pom l i s a =
    match l with
    | [] -> List.rev a
    | xi::tl ->
      let e = (i, xi) in
      match s with
      | [] -> pom tl (i+1) (e::s) (i::a)
      | (j, xj)::ts ->
        if xi < xj
        then pom tl (i+1) (e::s) ((i-j)::a)
        else pom l i ts a
  in
  pom l 1 [] [];;


(*
implementacja kolejki FIFO+LIFO z operacjami

    val back : 'a queue -> 'a
    val insert_front : 'a queue -> 'a -> 'a queue
    val remove_back : 'a queue -> 'a queue
    val empty : 'a queue
    val is_empty : 'a queue -> bool
    val insert : 'a queue -> 'a -> 'a queue
    val front : 'a queue -> 'a
    val remove : 'a queue -> 'a queue

funkcja potencjalu F = |length front - length back|
napisanie tej listy na podstawie wykładu 
*)

open List;;

module type QUEUE = 
  sig
    exception EmptyQueue
    type 'a queue
    val empty : 'a queue
    val is_empty : 'a queue -> bool
    val insert : 'a queue -> 'a -> 'a queue
    val front : 'a queue -> 'a
    val remove : 'a queue -> 'a queue
  end;;

module Lifo : QUEUE = 
  struct
    exception EmptyQueue
    type 'a queue = 'a list
    let empty = []
    let is_empty q = q = []
    let insert q x = x::q
    let front q = 
      if q = [] then raise EmptyQueue 
      else hd q
    let remove q =
      if q = [] then raise EmptyQueue 
      else tl q
  end;;
	  
module Fifo : QUEUE = 
  struct
    exception EmptyQueue

    (* Kolejka to trojka: przód, tyl, rozmiar. *)
    (* Jeżeli przód jest pusty, to kolejka jest pusta. *)
    type 'a queue = {front: 'a list; back: 'a list; size: int}

    let empty = {front=[]; back=[]; size=0}

    let size q = q.size

    let is_empty q = size q = 0

    let balance q = 
      match q with
	{front=[]; back=[]}        -> q |
	{front=[]; back=b; size=s} -> {front=rev b; back=[]; size=s} |
	_                          -> q

    let insert {front=f; back=b; size=n} x =
      balance {front=f; back=x::b; size=n+1}

    let front q = 
      match q with
	{front=[]}   -> raise EmptyQueue |
	{front=x::_} -> x
    
    let remove q =
      match q with
	{front=_::f} -> balance {front=f; back=q.back; size=q.size-1} |
	_            -> raise EmptyQueue

  end;;


module type BIQUEUE = 
  sig
    include QUEUE
    val back : 'a queue -> 'a
    val insert_front : 'a queue -> 'a -> 'a queue
    val remove_back : 'a queue -> 'a queue
  end;;

module FifoLifo : BIQUEUE =
  struct
    exception EmptyQueue

    (* Typ = przód, tył, rozmiar. *)
    type 'a queue = 'a list * 'a list * int

    (* Pusta kolejka *)
    let empty : 'a queue = ([], [], 0)
	
    (* Rozmiar *)
    let size ((_, _, n): 'a queue) = n

    (* Czy jest pusta? *)
    let is_empty q = size q = 0
 
    (* Wyrównanie listy, jeśli jedna z połówek jest pusta, a druga 
       zawiera więcej niż jeden element. *)
    let balance (q: 'a queue) = 
      (* Podział listy na połowy h1 i h2, length h1 <= lenght h2 *)
      let halve l = 
	let rec head l n a = 
	  if n = 0 then 
            (rev a, l)
	  else
            match l with 
              []   -> failwith "Zbyt krótka lista" |
              h::t -> head t (n-1) (h::a)
	in 
	head l (length l / 2) []
      in 
      match q with
	([], [], _)    -> q |
	([_], [], _)   -> q |
	([], [_], _)   -> q |
	(front, [], n) -> 
          let (h1, h2) = halve front
          in (h1, rev h2, n) |
	  ([], back, n)  -> 
            let (h1, h2) = halve back
            in (rev h2, h1, n) |
	    _           -> q 
		
    (* Wstawienie na koniec *)
    let insert ((front, back, n): 'a queue) x =
      ((balance (front, x::back, n+1)): 'a queue)

    (* Wstawienie na początek *)
    let insert_front ((front, back, n): 'a queue) x =
      ((balance (x::front, back, n+1)): 'a queue)

    (* Początek *)
    let front (q: 'a queue) = 
      match q with
	([], [x], _) -> x |
	([], _, _)   -> raise EmptyQueue |
	(x::_, _, _) -> x

    (* Koniec *)
    let back (q: 'a queue) = 
      match q with
	([x], [], _) -> x |
	(_, [], _)   -> raise EmptyQueue |
	(_, x::_, _) -> x
  
    (* Wyjęcie początku *)
    let remove (q: 'a queue) =
      match q with
	(_::front, back, n) -> balance (front, back, n-1) |
	([], _, _) -> raise EmptyQueue

    (* Wyjęcie końca *)
    let remove_back (q: 'a queue) =
      match q with
	(front, _::back, n) -> balance (front, back, n-1) |
	(_, [], _) -> raise EmptyQueue
    
  end;;

(*
k-max kolejki:
init: int -> 'a kmax_queue
put: 'a kmax_queue -> 'a -> 'a kmax_queue
get_max: 'a kmax_queue -> 'a (maksymalny element z k ostatnich elementow kolejki)
*)

open FifoLifo;;

type 'a kmax_queue = (int * 'a) queue * int * int;; (* dwa ostatnie to k i n*)

let init k = (empty, k, 0);;

let get_max (q, _, _) = snd (back q);;

let put (q, k, n) x =
  let n = n + 1 in
  let rec pom q =
    if is_empty q then q else
    if snd (front q) <= x then pom (remove q)
    else q
  in
  let q =
    if is_empty q then q else
    if fst (back q) < n-k then remove_back q
    else q
  in
  (insert (pom q) (n, x), k, n);;



