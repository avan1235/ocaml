(********************************************************************************************************)
(****************************************** TOPOLOGICAL SORTING *****************************************)
(********************************************************************************************************)

(*------------------------------------------------------------------------------------------------------*)
(* Author: Maciej Procyk                                                                                *)
(* Reviewer: Hubert Budzynski                                                                           *)
(*------------------------------------------------------------------------------------------------------*)


(********************************************************************************************************)
(************************************************ TYPES *************************************************)
(********************************************************************************************************)

(* help type when coloring all nodes of the graph when visiting then using DFS algorithm                *)
type color =
  | Not_visited
  | Processing
  | Visited


(********************************************************************************************************)
(********************************************** EXCEPTIONS **********************************************)
(********************************************************************************************************)

(* exception raised when the graph is cyclic                                                            *)
exception Cykliczne


(********************************************************************************************************)
(********************************************** FUNCTIONS ***********************************************)
(********************************************************************************************************)

(* create a map from node value to the list of it's children by scanning the input list                 *)
let create_graph l =
  List.fold_left (fun accMap (v, list) -> Hashtbl.add accMap v list; accMap)
                 (Hashtbl.create (List.length l))
                 l


(* init_nodes returns hashmap_of_nodes which contains the status Not_visited of each node               *)
let init_nodes l =
  let accMap = Hashtbl.create (List.length l) in
  List.iter (fun (v, list) ->
      List.iter (fun v ->
          Hashtbl.add accMap v Not_visited) list;
      Hashtbl.add accMap v Not_visited)
    l;
  accMap


(* getting list of all keys from map                                                                    *)
let keys_to_list map =
  Hashtbl.fold (fun v _ a -> v::a) map [] 


(* main algorithm using DFS and coloring of the nodes in graph for finding the topological order in     *)
(* graph defined in the given list of node and its children                                             *)
let topol l =
  let res_list = ref [] in
  let graph = create_graph l in
  let nodes_s = init_nodes l in
  let nodes_l = keys_to_list nodes_s in
  let rec scan_nodes l =
    match l with
    | node_v::tl ->
      (
        match Hashtbl.find nodes_s node_v with
        | Not_visited -> (*                                                   if not visited then visit *)
          (* color the current node as being processed                                                  *)
          Hashtbl.replace nodes_s node_v Processing;
          (* check if we can go to the children of the node - if yes then scan them                     *)
          if Hashtbl.mem graph node_v
          then scan_nodes (Hashtbl.find graph node_v);
          (* color the current node as Visited                                                          *)
          Hashtbl.replace nodes_s node_v Visited;
          (* add current node to the list of the topologically oredered list                            *)
          res_list := node_v::(!res_list);         
        | Processing -> raise Cykliczne (* if back again in the node that is being processed then error *)
        | Visited -> () (*                                           if already visited then do nothing *)
      );
      scan_nodes tl; (*                                              scan the rest of the list of nodes *)
    | [] -> ()
  in
  scan_nodes nodes_l;
  !res_list
    

(*
(* ---------------------------- SECOND VERSION SLOWER BECAUSE BASED ON PMAP --------------------------- *)
(********************************************************************************************************)
(****************************************** TOPOLOGICAL SORTING *****************************************)
(********************************************************************************************************)

(*------------------------------------------------------------------------------------------------------*)
(* Author: Maciej Procyk                                                                                *)
(* Reviewer: Hubert Budzynski                                                                           *)
(*------------------------------------------------------------------------------------------------------*)


(********************************************************************************************************)
(************************************************ TYPES *************************************************)
(********************************************************************************************************)

(* help type when coloring all nodes of the graph when visiting then using DFS algorithm                *)
type color =
  | Not_visited
  | Processing
  | Visited


(********************************************************************************************************)
(********************************************** EXCEPTIONS **********************************************)
(********************************************************************************************************)

(* exception raised when the graph is cyclic                                                            *)
exception Cykliczne


(********************************************************************************************************)
(********************************************** FUNCTIONS ***********************************************)
(********************************************************************************************************)

(* create a map from node value to the list of it's children by scanning the input list                 *)
let create_graph l =
  List.fold_left (fun accMap (v, list) -> PMap.add v list accMap)
                 (PMap.empty)
                 l


(* int node returns pmap_of_nodes where map contains the status Not_visited of each node                *)
let init_nodes l =
  List.fold_left (fun accMap (v, list) ->
      let accMap =
        List.fold_left (fun accMap v -> PMap.add v Not_visited accMap)
                       accMap
                       list
      in
      PMap.add v Not_visited accMap)
    PMap.empty
    l


(* getting list of all keys from PMap                                                                   *)
let keys_to_list map =
  PMap.foldi (fun key _ acc -> key::acc) map []


(* main algorithm using DFS and coloring of the nodes in graph for finding the topological order  in    *)
(* graph defined in the given list of node and its children                                             *)
let topol l =
  let res_list = ref [] in
  let graph = create_graph l in
  let nodes = init_nodes l in
  let nodes_l = keys_to_list nodes in
  let nodes = ref nodes in
  let rec scan_nodes l =
    match l with
    | node_v::tl ->
      (
        match (PMap.find node_v (!nodes)) with
        | Not_visited -> (*                                                   if not visited then visit *)
          (* color the current node as being processed                                                  *)
          nodes := PMap.add node_v Processing (!nodes);
          (* check if we can go to the children of the node - if yes then scan them                     *)
          if PMap.exists node_v graph
          then scan_nodes (PMap.find node_v graph);
          (* color the current node as Visited                                                          *)
          nodes := PMap.add node_v Visited (!nodes);
          (* add current node to the list of the topologically oredered list                            *)
          res_list := node_v::(!res_list);         
        | Processing -> raise Cykliczne (* if back again in the node that is being processed then error *)
        | Visited -> () (*                                           if already visited then do nothing *)
      );
      scan_nodes tl; (*                                              scan the rest of the list of nodes *)
    | [] -> ()
  in
  scan_nodes nodes_l;
  !res_list
*)
