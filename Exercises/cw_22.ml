let pref a =
  let n = Array.length a in
  let p = Array.make (n+1) 0 in
  let pj = ref 0 in
  for i = 2 to n do
    while (!pj > 0) && (a.(!pj) <> a.(i-1)) do
      pj := p.(!pj)
    done;
    if a.(!pj) = a.(i-1) then pj := !pj + 1;
    p.(i) <- !pj
  done;
  p;;

(* Wyszukiwanie wzorca w tekscie cwiczenia *)
(* rotacja slowa nazywamy takei przesuniecie slowa, ze [x1, ..., xn] -> [xk, ..., xn, x1, ..., xk-1] *)
(* wyznacz tak liczbe nietrywailnych rotacji, ze slowo przejdzie samo w siebie                       *)

(* znajdowanie okresu w slowie - prefikso sufiksy - w czasie liniowym *)
(* wyszukiwanie slowa w w slowie ww                                   *)



  
(* dla danych slow x i y znalezc najdluzszy prefix y ktory jest spojnym podciagiem w x                    *)
(* kmp wzorzec = p = [1, p0, p1, ..., pn] gdzie pi to dlugosc najdluzszego prefikso-sufiksu na pozycjach  *)
(* od 0 do i                                                                                              *)
(* tworzymy tablice prefikso-sufiksow dla slowa y$x i nastepnie przegladamy tablice p.(i) dla i wiekszych *)
(* niz dlugosc slowa y                                                                                    *)


(* okresem slowa nazywamy najmniejsza dodtnia liczbe calkowita k taka ze xi = x_i+k dla i = {0, ..., k}   *)
(* napisz funkcje okresy, ktora okresy [x1, ..., xn] zwroci [k1, ..., kn] gdzie ki jest okresem xi do xn  *)


(* zadanie szablon: dany dlugi napis ktory chcemy uzyskc za pomoca krotszego szablonu, szablon musi zawsze *)
(* miescic sie w slowie                                                                                    *)
(* szablon jest prefikso-sufiksem samego slowa!!!                                                          *)
(* a < b a i b sa prefikso-sufiksami x to b jest prefikso-sufiksem a                                       *)
(* prefikso-sufiks trywialny l0 = n, pozniej l1 = p.(l0), l2 = p.(l1)                                      *)


(* okeresem slowa x1,...,xn nazywamy takie k ze x1 = x1 + k ...                                            *)
(* wyznaczyc liste [k1; k2; ...; kn] gdzie k_l to okres xl, xl+1, ..., xl                                  *)
(* okres k <=> najdluzszy prefikso-sufiks ma dlugosc n-k !!!                                               *)
let okresy w =
  let n = Array.length w in
  let arev = fun a -> a |> Array.to_list |> List.rev |> Array.of_list in
  let w = arev w in
  let p = pref w in
  Array.mapi (fun i d -> i+1-d) p |> arev;;


(* tablice sufiksowe :                                                                                     *)
(* szukamy najdluzszego wsponego podslowa slow x i y                                                       *)
(* wykorzystujemy do tego tablice LCP dla sklejonych slow x i y                                            *)
let szukaj x y =
  let n = Array.length x in
  let m = Array.length y in
  let z = Array.init (n+m) (fun i -> if i < n then x.(i) else y.(i-n)) in
  let _, suf, lcp = algorytm_lcp z in
  let dmax = ref 0 in
  let odp = ref (0,0) in
  for i = 0 to m+n do
    if suf.(i) < n && suf.(i+1) >= n then
      (
        if lcp.(i) > !dmax then (dmax := lcp.(i); odp := (suf.(i), suf.(i+1)-n))
      )
    else if suf.(i) >= n && suf.(i+1) < n then
      (
        if lcp.(i) > !dmax then (dmax := lcp.(i); odp := (suf.(i+1), suf.(i)-n))
      )
  done;
  !odp;;

(* dla danej tablicy znakow [|c0, ..., cn-1|] w szukamy podslowa [|ci, ..., cj-1|] ktore wystepuje w w k razy     *)
(* ma ono wowczas punktacje k * (j-i) -> znalezcz slowo o najwiekszej punktacji                                   *)
(* slowo ktore wystepuje k razy bedzie prefiksem k kolejnych leksykograficznie sufiksow suf.(i), ..., suf.(i+k-1) *)
(* najdluzszy prefiks tych sufiksow ma dl min (lcp.(i), ..., lcp.(i+k-2))                                         *)
(* szukamy po 0<=a<=b<=n max {(b-a+2) * min (lcp.(i)) dla a<=i<=b }                                               *)
let podslowo x =
  let n = Array.length x in
  let _, suf, lcp = algorytm_lcp x in
  let max_punkt = ref n in
  let odp = ref (0, n) in
  let stos = ref [] in
  for i = 0 to n-1 do
    while !stos <> [] && lcp.(List.hd !stos) > lcp.(i) do
      let j = List.hd !stos in
      stos := List.tl !stos;
      let pkt = lcp.(j)*(i-j+1) in
      if pkt > !max_punkt then (max_punkt := pkt; odp := (suf.(j), suf.(j) + lcp.(j)))
    done;
    if lcp.(i) > 0 && !stos <> [] && lcp.(List.hd !stos) < lcp.(i) then stos := i::!stos;
  done;
  !odp;;



(* myszy i koty: [x0, ..., xn-1] na i-tym metrze harcuje x.(i) myszy                                            *)
(* kot postawiony do patrolowania metrow od i do j - kot wylapie max 0 (xi + ... + xj) - (j-i)^2                *)
(* ile maksymalnie maksymalnie mozna zlapac myszy majac do dyspozycji k kotow                                   *)
(* koty musza patrolowac na oddzielnych przedzialach                                                            *)
(* d.(j).(l) - maksymalna liczba zlapanych myszy z uzyciem l kotow na pierwszych j metrach                      *)
(* d.(j).(l) = max (d.(j-1).(l)) (max dla 0<=i<j zysk(i,j-1) + d.(i).(l-1))                                     *)
(* patrzymy dodatkowo kiedy zysk(i1,j-1) + d.(i1).(l-1) <= zysk.(i2,j-1) + d.(i2).(l-1)    dl i1<i2             *)
(* bedzie to oznaczac ze ustawienie l-tego kota na przedziale i2 do j-1 jest lepszeniz na przedziale i1 do j-1  *)
(* wtw. gdy j > p(i1, i2)                                                                                       *)


(* budujemy ciak w nastepujacy sposob x0 = 1, xk = xi + xj gdzie 0 <= i,j < k                                   *)
(* dla zadanego n jaki jest najkrotszy ciag taki ze ostatni element wynosi n                                    *)
let ciag n =
  let min_len = ref (n+1) in
  let x = Array.make !min_len 1 in
  let min_len = ref max_int in
  let rec bt len =
    if (1 lsbl (!min_len-len-1))*x.(len-1) >= then (
    if x.(len-1) = n then min_len := min len !min_len else
    for i = len-1 downto 0 do
      for j = len-1 downto 0 do
        x.(len) <- x.(i) + x.(j);
        if x.(len) <= n && x.(len) > x.(len-1) then bt (len+1)
      done;
    done;
    )
  in
  bt 1;;

