let print_matrix mt =
  let m = Array.length mt in
  let n = Array.length mt.(0) in
  for i = 0 to m-1 do
    (print_string "[";
    for j = 0 to n-2 do
      (print_int mt.(i).(j);
       print_string " | ";)
    done;
     print_int mt.(i).(n-1);
     print_string "]\n";
    )
    done;;


let rec fibTopDown n a =
  if n = 0 then 0 else
  if n = 1 then 1 else
  if a.(n) <> 0 then a.(n) else
    begin
      a.(n) <- (fibTopDown (n-1) a) + (fibTopDown (n-2) a);
      a.(n)
    end;;

let fibNum n =
  let a = Array.make (n+1) 0 in
  fibTopDown n a;;

let print_array a =
  let n = Array.length a in
  for i = 0 to n-1 do begin print_int a.(i); print_string " - "; end done;
  print_newline ();;

(* when the set of avaiable coins is given and the smallest one ofe them is 1  *)
(* we can easilly find the optimal way to change specific amount by dynamic    *)
(* programming - we have to find the solution for smaller amounts and use them *)
let coin_change amount coins =
  let coins = Array.of_list coins in
  Array.sort compare coins;
  let max_number_of_coins = amount in
  let request_amount_array = Array.init (max_number_of_coins + 1) (fun i -> i) in
  for i = 1 to max_number_of_coins do
    begin
      Array.iter (fun coin ->
          if coin <= i then request_amount_array.(i) <- min (request_amount_array.(i-coin) + 1) (request_amount_array.(i)))
        coins
    end
  done;
  print_array request_amount_array;
  request_amount_array.(max_number_of_coins);;


let count_all_paths n =
  let m = Array.make_matrix n n 1 in
  for cur_n = 2 to n do
    for i = 1 to cur_n-1 do
      begin
        m.(cur_n-1).(i) <- m.(cur_n-1).(i-1) + m.(cur_n-2).(i);
        m.(i).(cur_n-1) <- m.(i-1).(cur_n-1) + m.(i).(cur_n-2);
      end
    done;
  done;
  m.(n-1).(n-1);;

count_all_paths 3;;
count_all_paths 33;;

(* rod problem for dynamic programming *)
(* T(n) = O(n^2) and M(n) = O(n)       *)
let rod_problem p =
  let n = Array.length p in
  let best_price = Array.init (n+1) (fun i -> if i = 0 then 0 else p.(i-1)) in
  for len = 2 to n do
    for i = 0 to len-1 do
      best_price.(len) <- max (best_price.(len)) (best_price.(i) + best_price.(len-i))
    done;
  done;
  best_price.(n);;

rod_problem [|2;3;7;8;9|];;

let print_logic_matrix m =
  print_newline ();
  for i = 0 to Array.length m -1 do
    print_string "[";
    for j = 0 to Array.length m.(0) - 2 do
      print_int (if m.(i).(j) then 1 else 0);
      print_string " | ";
    done;
    print_int (if m.(i).(Array.length m.(0) -1) then 1 else 0);
    print_string "]\n";
  done;;


(* subset sum problem solved by dynamic programming *)
(* T(n) = O(n*s)                                    *)
let subset_sum set s =
  let n = 1 + Array.length set in
  let set = (fun i -> if i = 0 then 0 else set.(i-1)) in
  let elements_reachedSum = Array.make_matrix n (s+1) false in
  for elem_ind = 0 to n-1 do
    elements_reachedSum.(elem_ind).(0) <- true
  done;
  for elem_ind = 1 to n-1 do
    for cur_sum = 1 to s do
      begin
        let add_ind = cur_sum - (set elem_ind) in
        if cur_sum = (set elem_ind) || elements_reachedSum.(elem_ind-1).(cur_sum) || (add_ind > -1 && add_ind < s+1 && elements_reachedSum.(elem_ind-1).(add_ind))
        then elements_reachedSum.(elem_ind).(cur_sum) <- true;
      end
    done;
  done;
  print_logic_matrix elements_reachedSum;
  elements_reachedSum.(n-1).(s);;

let subset_sum_another_direction set s =
  let n = Array.length set in
  let usedElements_reachedSum = Array.make_matrix (n+1) (s+1) false in
  for i = 0 to n do
    usedElements_reachedSum.(i).(0) <- true
  done;
  for cur_sum = 1 to s do
    for elem_indx = 1 to n do
      usedElements_reachedSum.(elem_indx).(cur_sum) <- usedElements_reachedSum.(elem_indx-1).(cur_sum);
      if cur_sum >= set.(elem_indx-1) then
        usedElements_reachedSum.(elem_indx).(cur_sum) <-
          usedElements_reachedSum.(elem_indx).(cur_sum) || usedElements_reachedSum.(elem_indx-1).(cur_sum-set.(elem_indx-1));
    done;
  done;
  print_logic_matrix usedElements_reachedSum;
  usedElements_reachedSum.(n).(s);;

subset_sum [|3;2;7;1|] 6;;
subset_sum [|3;34;4;12;5;2|] 9;;
subset_sum_another_direction [|3;34;4;12;5;2|] 9;;

(* finding longest increasing subsequence in array by finding the result for shorter *)
(* parts of array and using the results to compute the next result in time O(n^2)    *)
let longest_increasing_subsequence a =
  let n = Array.length a in
  let sub_lenghts = Array.make n 1 in
  for act_len_indx = 1 to n-1 do
    for i = 0 to act_len_indx do
      if a.(act_len_indx) > a.(i)
      then sub_lenghts.(act_len_indx) <- max (sub_lenghts.(act_len_indx)) (sub_lenghts.(i) + 1)
    done;
  done;
  let max_v = ref sub_lenghts.(0) in
  for i = 1 to n-1 do
    max_v := max (!max_v) (sub_lenghts.(i))
  done;
  !max_v;;

longest_increasing_subsequence [|1; 12; 7; 0; 23; 11; 52; 31; 61; 69; 70; 2|];;

(* number of minimum jumps through the table in recursive way *)
let rec_minimum_jumps a =
  let n = Array.length a in (* last index is n-1 *)
  if n < 2 then 0 else
    if n < 3 then 1 else
      begin
        let rec jump act_i = (* returns the minimal number of jumps from current position *)
          let v = a.(act_i) in
          if act_i + v > n-2 (* act_i + v >= n-1 *)
          then 1 else
            begin
              let min_jumps = ref max_int in
              for jump_len = 1 to v do
                min_jumps := min !min_jumps (jump (act_i+jump_len))
              done;
              !min_jumps + 1
            end
        in
        jump 0
      end;;


let minimum_jumps a =
  let n = Array.length a in
  if  n < 2 then 0 else if n < 3 then 1 else
    begin
      let map = Hashtbl.create n in
      let rec jump  act_i =
        let v = a.(act_i) in
        if act_i + v > n-2 then 1 else if Hashtbl.mem map act_i then Hashtbl.find map act_i else
          begin
            let min_jumps = ref max_int in
            for jump_len = 1 to v do
              min_jumps := min !min_jumps (jump (act_i+jump_len))
            done;
            !min_jumps + 1
          end
      in
      jump 0
    end;;


minimum_jumps [|1; 3; 5; 3; 3;1;1;1;1;1;1;1;1;4|];;

(* finding the longest palindromic subsequence of some string by solving the problem for *)
(* shorter strings and the result is sotred in the matrix long_sub.(i).(j) which has the *)
(* result for the substring from i to j                                                  *)
(* for i = j we have long_sub.(i).(j) = 1 because one letter word is palindrom           *)
(* for i + 1 = j we have long_sub.(i).(j) = 2 if string.(i) = string.(j)                 *)
(* and when a.(i) = a.(j) then then long_sub.(i).(j) = 2 + long_sub.(i+1).(j-1)          *)
(* and in the other case we state that long_sub.(i).(j) = max long_sub.(i+1).(j)         *)
(* long_sub.(i).(j-1) *)
let longest_palindromic_subsequence s =
  let string_to_array s =
    let l = Seq.fold_left (fun a v -> v::a) [] (String.to_seq s) |> List.rev in
    let n = List.length l in
    let a = Array.init n (fun i -> List.nth l i) in a
  in
  let a = string_to_array s in
  let n = Array.length a in
  let long_sub = Array.make_matrix n n 0 in
  for i = 0 to n-1 do
    long_sub.(i).(i) <- 1
  done;
  for sublen = 2 to n do
    begin
      for i = 0 to n-sublen do
        begin
          let j = i + sublen - 1 in
          if sublen = 2 && a.(i) = a.(j) then long_sub.(i).(j) <- 2 else
          if a.(i) = a.(j) then long_sub.(i).(j) <- long_sub.(i+1).(j-1) + 2 else
          long_sub.(i).(j) <- max (long_sub.(i+1).(j)) (long_sub.(i).(j-1))
        end
      done;
    end
  done;
  print_matrix long_sub;
  long_sub.(0).(n-1);;

  
longest_palindromic_subsequence "AABCDEBAZ";;

let max_subarray_sum_kadane a =
  let n = Array.length a in
  let max_sum = ref a.(0) in
  let max_so_far = ref a.(0) in
  for i = 0 to n-1 do
    max_so_far := max a.(i) (!max_so_far + a.(i));
    max_sum := max !max_sum !max_so_far
  done;
  !max_sum;;

max_subarray_sum_kadane [|5;-1;-4;4;6;-2;7|];;

let given_subarray_sum a n =
    let m = Array.length a in
    let dp = Array.make_matrix (m+1) (n+1) (false, []) in
    for i = 0 to m do
      dp.(i).(0) <- (true, [])
    done;
    for i = 1 to m do(
      for j = 1 to n do(
        if j < a.(i-1) then dp.(i).(j) <- dp.(i-1).(j) else
          if j >= a.(i-1) then(
            if fst (dp.(i-1).(j)) then dp.(i).(j) <- dp.(i-1).(j) else
              if fst (dp.(i-1).(j - a.(i-1))) then dp.(i).(j) <- (true, (a.(i-1))::(snd (dp.(i-1).(j-a.(i-1)))));
          )
      )done;
    )done;
    snd dp.(m).(n);;
                                
      
given_subarray_sum [|3;34;4;12;5;2|] 9;;

let partition_problem a =
  let s = Array.fold_left (+) 0 a in
  if given_subarray_sum a (s/2) = [] || s mod 2 = 1 then false else true;;


type pora = Wiosna | Lato | Jesien | Zima;;

let pory a =
  let a = Array.map (fun p ->
              match p with
              | Wiosna -> 0
              | Lato -> 1
              | Jesien -> 2
              | Zima -> 3) a in
  let a1 = a in
  let a2 = Array.map (fun v -> (v+1) mod 4) a in
  let a3 = Array.map (fun v -> (v+2) mod 4) a in
  let a4 = Array.map (fun v -> (v+3) mod 4) a in  
  let n = Array.length a in
  let czy_jest a i j =
    let w  = ref (-1) in
    for x = i to j do
      if !w + 1 = a.(x) then w := !w + 1;
    done;
    !w = 3
  in  
  let len = ref max_int in
  let check a =
    for i = 3 to n-1 do
      for j = 0 to i-3 do
        if czy_jest a j i then len := min !len (i-j+1)
      done;
    done;
  in
  check a1;
  check a2;
  check a3;
  check a4;
  !len;;


pory [|Lato; Wiosna; Zima; Jesien; Lato; Zima; Wiosna; Jesien; Lato|];;

