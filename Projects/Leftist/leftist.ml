(****************************************************************************************)
(******************************** MODUL DRZEWA LEWICOWE *********************************)
(****************************************************************************************)

(*--------------------------------------------------------------------------------------*)
(* Autor: Maciej Procyk                                                                 *)
(* Recenzujacy: Marcin Wierzbicki                                                       *)
(*--------------------------------------------------------------------------------------*)

(*--------------------------------------------------------------------------------------*)
(* Reprezentacja drzew lewicowych jako drzewa binarnego z odpowiednio zaimplementowana  *)
(* funkcja laczenia drzew. Struktura sklada sie ze wskaznikow na inne struktury,        *)
(* wartosci w danym wezle oraz prawej wysokosci bedacej odlegloscia danego wezla od     *)
(* liscia przy przejsciu za kazdym razem po prawej sciezce.                             *)
(* Kolejka moze zawierac elementy, dla ktoruch mozliwe jest porownywanie za pomoca      *)
(* operatora logicznego "<="                                                            *)
(*--------------------------------------------------------------------------------------*)


(****************************************************************************************)
(*********************************** DEKLARACJA TYPU ************************************)
(****************************************************************************************)

(* Typ zlozony z krotki (wskaznik_L, wartosc, prawa_wysokosc, wskaznik_P) lub liscia,   *)
(* dla ktorego prawa_wysokosc jest zawsze rowna 0                                       *)
type 'a queue =
  | Node of 'a queue * 'a * int * 'a queue
  | Leaf


(****************************************************************************************)
(********************************* DEKLARACJA WYJATKOW **********************************)
(****************************************************************************************)

(* Wyjatek wznoszony w momencie proby operacji na pustej kolejce                        *)
exception Empty

(* Wyjatek wzoszony gdy sprawdzane lista i kolejka priorytetowa roznia sie              *)
exception Different_values

(****************************************************************************************)
(************************************* KONTRUKTORY **************************************)
(****************************************************************************************)

(* Tworzenie  kolejki z jedna wartoscia w korzeniu - prawa_wysokosc rowna 1             *)
let new_single v =
  Node(Leaf, v, 1, Leaf)                  

(* Tworzenie pustej kolejki                                                             *)
let empty = Leaf


(****************************************************************************************)
(********************************** FUNKCJE POMOCNICZE **********************************)
(****************************************************************************************)

(* Dla danego wezla zwraca jego prawa wysokosc                                          *)
let height t =
  match t with
  | Leaf -> 0
  | Node(_, _, h, _) -> h
  

(****************************************************************************************)
(********************************* OPERACJE NA KOLEJCE **********************************)
(****************************************************************************************)

(* Operacja laczenia kolejek q1 q2 poprzez rekurencyjne wywolywanie funkcji join        *)
(* Przyjete oznaczenia: lq, rq - odpowiednio lewei prawe poddrzewo                      *)
(*                      v - wartosc w danym wezle drzewa                                *)
(*                      h - prawa wysokosc w danym wezle drzewa                         *)
let rec join (q1 : 'a queue) (q2 : 'a queue) =
  match q1, q2 with
  | Leaf, q -> q
  | q, Leaf -> q
  | Node(lq1, v1, _, rq1), Node(_, v2, _, _) -> if v1 <= v2
  (* wartosc w 1 poddrzewie jest mniejsza *)    then(
  (* wiec tworzymy nowe drzewo zawieszone *)      let new_q = join rq1 q2 in
  (* w danej wartosci i rekurencyjnie     *)      let h_new = height new_q
  (* tworzymy podrzewa, jednak przed ich  *)      and h_left = height lq1 in
  (* przylaczeniem porownujemy ich prawe  *)      if  h_new < h_left
  (* wysokosci tak by zawsze ta mniejsza  *)      then Node(lq1,   v1, h_new + 1, new_q)
  (* byla po prawej stronie nowego drzewa *)      else Node(new_q, v1, h_left + 1,  lq1)
  (* ------------------------------------ *)    )
  (* zamieniamy poddrzewa w przeciwnym    *)    else join q2 q1
  (* wypadku (gdy v1 > v2)                *)

(* Dodawanie wartosci v do kolejki q poprzez utworzenie pojedynczej kolejki i polacznie *)
(* starej kolejki z nowa                                                                *)
let add (v : 'a) (q : 'a queue) =
  let new_q = new_single v in
  join q new_q 

(* Otrzymanie z kolejki q najmniejszej wartosci z korzenia drzewa i polaczenie jego     *)
(* poddrzew - zwraca pare (najmniejszy_element, nowa_kolejka)                           *)
let delete_min (q : 'a queue) =
  match q with
  | Leaf -> raise Empty                     (* wznoszony wyjatek gdy kolejka jest pusta *)
  | Node(lq, v, _, rq) -> let new_q = join lq rq in
                          (v, new_q)

(* Sprawdzenie czy kolejka jest pusta, czyli drzewo sklada sie z samego liscia          *)
let is_empty (q : 'a queue) =
  match q with
  | Leaf -> true
  | _ -> false

(****************************************************************************************)
(**************************************** TESTY *****************************************)
(****************************************************************************************)

(* Testy operacji add, join, delete_min i konstruktora empty z wykorzystaniem list      *)
(* 
let list_to_queue l =
  List.fold_left (fun a x -> (add x a)) empty l

let test_add_delete no l =
  let q = list_to_queue l
  and l = List.sort compare l in
  let rec scan q l =
    let qv = fst (delete_min q)
    and lv = List.hd l in
    if lv <> qv
    then raise Different_values
    else scan (snd (delete_min q)) (List.tl l)
  in
  try
    scan q l
  with
  | Empty -> ("TEST " ^ (string_of_int no) ^ " OK\n")
  | Different_values -> ("TEST " ^ (string_of_int no) ^ " ERROR\n")

let rec create_big_list size f mx =
  if size = 0
  then []
  else (f mx)::(create_big_list (size-1) f mx)

let random_string l =
  let rec help a l =
    if l = 0
    then a
    else help ((Char.escaped (Char.chr (65 + (Random.int 26)))) ^ a) (l-1)
  in help "" l


let int_list_2 = create_big_list 100000 Random.int 3
let int_list_max = create_big_list 100000 Random.int 100000
;;
print_string "TESTING INTEGER QUEUES\n";
print_string (test_add_delete 1 int_list_2);
print_string (test_add_delete 2 int_list_max);
;;
let float_list_2 = create_big_list 100000 Random.float 3.
let float_list_max = create_big_list 100000 Random.float 100000.
;;
print_string "TESTING FLOAT QUEUES\n";
print_string (test_add_delete 3 float_list_2);
print_string (test_add_delete 4 float_list_max);
;;
let string_list_2 = create_big_list 100000 random_string 2
let string_list_max = create_big_list 100000 random_string 100
;;
print_string "TESTING STRING QUEUES\n";
print_string (test_add_delete 5 string_list_2);
print_string (test_add_delete 6 string_list_max);
;;  

(* Testy funkcji is_empty                                                               *)
let q = empty;;
let r  = empty;;
print_string "TESTING is_empty\n";
print_string (if (is_empty (join q r)) then "TEST 7 OK\n" else "TES 7 ERROR\n");;       *)
