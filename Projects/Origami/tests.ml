(***********************************************************************************************)
(******************************************** TESTS ********************************************)
(***********************************************************************************************)

open Origami;;

(* function for printing test result                                                           *)
let print_test_res no b =
  print_string "TEST ";
  print_int no;
  print_string (if b then " OK\n" else " ERROR\n");;


(* function for comparing values to test                                                       *)
let test_value no v exp =
  print_test_res no (v = exp);;


(* tests of rectangles                                                                         *)
print_string "RECTANGLE TESTS\n\n";;

let rect = prostokat (-10.,-10.) (10., 10.);;
test_value 1 (rect (0., 0.)) 1;;
test_value 2 (rect (0., 10.)) 1;;
test_value 3 (rect (0., -10.)) 1;;
test_value 4 (rect (10., 0.)) 1;;
test_value 5 (rect (-10., 0.)) 1;;
test_value 6 (rect (10., 10.)) 1;;
test_value 7 (rect (-10., 10.)) 1;;
test_value 8 (rect (10., -10.)) 1;;
test_value 9 (rect (-10., -10.)) 1;;
test_value 10 (rect (10., 10.1)) 0;;

let rect = zloz (5., 0.) (5., 1.) rect;;
let rect = zloz (-5., 1.) (-5., 0.) rect;; 
test_value 11 (rect (0., 0.)) 3;;

let rect = zloz (1., 5.) (-1., 5.) rect;;
let rect = zloz (-1., -5.) (1., -5.) rect;;
test_value 12 (rect (0., 0.)) 9;;

let rect = prostokat (-100.,-100.) (100., 100.);;
let rect = zloz (100., 50.) (50., 100.) rect;;
test_value 13 (rect (70., 70.)) 2;;
let rect = zloz (-50., 100.) (-100., 50.) rect;;
test_value 14 (rect (-70., 70.)) 2;;
let rect = zloz (-100., -50.) (-50., -100.) rect;;
test_value 15 (rect (-70., -70.)) 2;;
let rect = zloz (50., -100.) (100., -50.) rect;;
test_value 16 (rect (70., -70.)) 2;;
let rect = zloz (0., 1.) (0., -1.) rect;;
test_value 17 (rect (70., 70.)) 4;;
test_value 18 (rect (0., 0.)) 1;;
test_value 19 (rect (1., 0.)) 2;;
test_value 20 (rect (-1., 0.)) 0;;
let rect = zloz (0., 0.) (1., 0.) rect;;
test_value 21 (rect (1., -1.)) 0;;
test_value 22 (rect (-1., 1.)) 0;;
test_value 23 (rect (-1., -1.)) 0;;
test_value 24 (rect (0., 0.)) 1;;
test_value 25 (rect (35., 35.)) 4;;
test_value 26 (rect (70., 70.)) 8;;
test_value 27 (rect (100., 50.)) 4;;
test_value 28 (rect (50., 100.)) 4;;
test_value 29 (rect (100., 100.)) 0;;
test_value 30 (rect ((3./.4.*.100.*.sqrt(2.))/.sqrt(2.), (3./.4.*.100.*.sqrt(2.))/.sqrt(2.))) 4;;

(* test of circles                                                                             *)
print_string "\n\nCIRCLE TESTS\n\n";;

let circ = kolko (0., 0.) 10.;;
test_value 31 (circ (0., 0.)) 1;;
test_value 32 (circ (0., 10.)) 1;;
test_value 33 (circ (-10., 0.)) 1;;
test_value 34 (circ (0., 10.)) 1;;
test_value 35 (circ (0., -10.)) 1;;
test_value 36 (circ (10., 10.)) 0;;
test_value 37 (circ (-10., 10.)) 0;;
test_value 38 (circ (-10., -10.)) 0;;
test_value 39 (circ (10., -10.)) 0;;
test_value 40 (circ (10./.sqrt(2.), 10./.sqrt(2.))) 1;;
let circ = zloz (1., 5.) (-1., 5.) circ;;
let circ = zloz (-1., -5.) (1., -5.) circ;;
let circ = zloz (5., -1.) (5., 1.) circ;;
let circ = zloz (-5., 1.) (-5., -1.) circ;;
test_value 41 (circ (0., 0.)) 5;;
test_value 42 (circ (2., 2.)) 3;;
test_value 43 (circ (-2., 2.)) 3;;
test_value 44 (circ (-2., -2.)) 3;;
test_value 45 (circ (2., -2.)) 3;;
test_value 46 (circ (1., 1.)) 3;;
test_value 47 (circ (5., 0.)) 1;;
test_value 48 (circ (-5., 0.)) 1;;
test_value 49 (circ (0., 5.)) 1;;
test_value 50 (circ (0., -5.)) 1;;
test_value 51 (circ (5., 5.)) 1;;
test_value 52 (circ (-5., 5.)) 1;;
test_value 53 (circ (-5., -5.)) 1;;
test_value 54 (circ (5., -5.)) 1;;
test_value 55 (circ (0., 0.)) 5;;
let circ = zloz (5., 0.) (0., 5.) circ;;
let circ = zloz (0., -5.) (5., 0.) circ;;
let circ = zloz (-5., 0.) (0., -5.) circ;;
let circ = zloz (0., 5.) (-5., 0.) circ;;
test_value 56 (circ (0., 0.)) 9;;
test_value 57 (circ (1., 1.)) 7;;
test_value 58 (circ (-1., 1.)) 7;;
test_value 59 (circ (-1., -1.)) 7;;
test_value 60 (circ (1., -1.)) 7;;


(* test of skladaj function                                                                   *)
print_string "\n\nSKLADAJ TESTS\n\n";;

let rect = ref (prostokat (0., 0.) (1000., 1000.));;

for i = 1 to 10 do
  let x = 1000. /. (2.**(float_of_int i)) in
  rect := zloz (x, 0.) (x, 1.) !rect;
done;

test_value 61 (!rect (0.000001, 0.)) 1024;;
test_value 62 (!rect (0., 0.)) 513;;
