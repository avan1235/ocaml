(*
*6 zadań programistycznych:
*Pisanie testów do programów i dobieranie się w pary w celu robienia review
*Review -> 2 pkt
*Program -> 5 pkt
*Pary nie mogą się powtarzać

Materiały: moodle.mimuw.edu.pl

OCaml jest językiem mocno typowanym
Ctrl+D -> znak końca strumienia/pliku
 *)


(* Przykłay *)

let f x = x*x;;
f 5;;

let ma co = "Ala ma "^co;;
ma "kota";;


(* typy z apostrofami 'a to typy ogólne *)

let id x = x;;
id 5;;
id "kot";;


(* operator kontaktenacji oczekuje dwóch argumentów typu string *)

(* let g x = (f x) ^ (f x);; *)

let h x = f (f (x));;
h 7;;
h (f (7));;

   
(* int jest 63 bitowy -> bo każdy obiekt jest wskaźniekiem -> jeden bit rozróżnia wskaźnik od inta *)

max_int;;


(* funkcje rekurencyjne wymagają dopisu rec *)

let rec silnia n =
  if n=0
  then 1
  else n*silnia(n-1);;

silnia 5;;

let rec powtorz n s =
  if n=0
  then ""
  else s^(powtorz (n-1) s);;

powtorz 5 "la ";;

let rec fib n =
   if n < 2
   then n
   else fib (n-1) + fib (n-2);;

fib 5;;
   

(* Wszystkie funkcje są naprawdę jednoargumentowe -> powtorz jest funkcją z inta na string a później ze stringa w string*)
  
let powt3 = powtorz 3;;
powt3 "la ";;

let powt_ala n = powtorz n "ala ";;
powt_ala 25;;

(* fun - deklarowanie lambdy*)
(* let f = fun a -> (fun b -> wyrażenie a i b)
   to to samo co let f a b = wyrażenie a b *)

(* DYREKTYWY *)
(*
  #use "plik.ml";;
*)
