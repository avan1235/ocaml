(* ################################### LISTY #################################### *)

(* lista prefiksow listy zakonczonych zerem *)
let prefiksy l =
  List.fold_left (fun (ll, al) x ->
      let nl = x::al in
      if x = 0
      then ((List.rev nl)::ll, nl)
      else (ll, nl)
    )
    ([], []) l |> fst |> List.rev;;

(* podzial na liste list tego samego znaku *)
let podzial l =
  match l with
  | [] -> [[]]
  | hd::tl -> let sgn x = if x > 0 then 1 else if x = 0 then 0 else -1 in
    let ((ll, ol), _) = List.fold_left (fun ((ll, al), ls) x ->
                        if (sgn x) = ls
                        then ((ll, x::al), sgn x)
                        else (((List.rev al)::ll, [x]), sgn x))
                        (([], [hd]), sgn hd) tl
    in
    match ol with
    | [] -> List.rev ll
    | _ -> List.rev (ol::ll);;

(* kompresja listy liczb do listy liczb gdzie i powtorzen k liczby reprezentowane *)
(* jest jako (2^(i-1))*(2k - 1)                                                   *)
let kompresja l =
  match l with
  | [] -> []
  | hd::tl -> let ((i2, lx), rl) = List.fold_left (fun ((i2, lx), rl) x ->
                                                 if x = lx
                                                 then ((i2*2, x), rl)
                                                 else ((1, x), (i2*(2*lx-1))::rl)
    ) ((1, hd), []) tl in
    List.rev ((i2*(2*lx-1))::rl);;

(* ################################### DRZEWA ################################### *)

(* definicja typu drzewa *)
type 'a tree =
    Node of 'a * 'a tree list;;

let rec fold_tree f (Node(x, l)) =
  f x (List.map (fold_tree f) l);;

let rosnaca t =
  let lista (l, _) = l in
  let dlug (_, d) = d in
  let (ogolnie, _) =
    fold_tree (fun x lista_par ->
        match lista_par with
        | [] -> (([x], 1), ([x], 1))
        | [(ogol, kon)] -> let (o, k) = if List.hd (lista kon) > x
                                        then (ogol, (x::(lista kon), 1 + dlug kon))
                                        else (ogol, kon) in
                           let (o, k) = if dlug k > dlug o
                                        then (k, k)
                                        else (o, k) in
                           (o, k)
        | hd::tl -> let nowy_kon = List.fold_left (fun a (_, kon) ->
                                       if List.hd (lista kon) > x && (1 + dlug kon > dlug a)
                                       then (x::(lista kon), 1 + dlug kon)
                                       else a
                                     ) ([x], 1) lista_par in
                    let nowy_ogolnie = List.fold_left (fun a (og, _) ->
                                           if dlug og > dlug a
                                           then og
                                           else a
                                         ) (fst hd) tl in
                    let nowy_ogolnie = if dlug nowy_kon > dlug nowy_ogolnie
                                       then nowy_kon
                                       else nowy_ogolnie
                    in
                    (nowy_ogolnie, nowy_kon)
      ) t in
  lista ogolnie;;


let prawie l =
  let max_listy l =
    match l with
    | [] -> failwith "empty list"
    | hd::tl -> List.fold_left max hd tl
  in
  let ll =
    match l with
    | [] -> [[]]
    | hd::tl -> let (ll, ostl) = List.fold_left (fun (ll, al) x ->
               if List.hd al = 0 && x = 0
               then (al::ll, [x])
               else (ll, x::al)
                                   ) ([], [hd]) tl in
                (ostl::ll)
  in
  let prawie_pom l =
    let l = List.map (fun x -> if x > 0 then 1 else if x = 0 then 0 else -1) l in
    let f l =
      match l with
      | [] -> 0
      | [_] -> 1
      | hd::tl -> let (m, _, _, _, _) =
      List.fold_left (fun (maks_dl, akt_dl, odl_od_zera, ost_war, bylo_zero) x ->
          if x*ost_war = -1
          then (max maks_dl (1+akt_dl), 1+akt_dl, 1+odl_od_zera, x, bylo_zero)
          else if x*ost_war = 1
          then (max maks_dl 1, 1, 1, x, false)
          else if x = 0 && not bylo_zero
          then (max maks_dl (1+akt_dl), 1+akt_dl, 0, 0, true)
          else if ost_war = 0
          then (max maks_dl (1+akt_dl), 1+akt_dl, 1, x, bylo_zero)
          else (* x = 0 && bylo_zero *)
            (max maks_dl (1+odl_od_zera), 1 + odl_od_zera, 0, 0, true)
        ) (1, 1, 0, hd, hd = 0) tl
        in
        m
    in
    f l
  in
  let maxy1 = List.map (fun l -> prawie_pom l) ll
  and maxy2 = List.map (fun l -> prawie_pom (List.rev l)) ll in
  max (max_listy maxy1) (max_listy maxy2);;


(* fold_tree przechodzi drzewo od dolu i przekazuje wyniki w gore *)
let rec fold_tree f (Node(x, l)) =
  f x (List.map (fold_tree f) l);;

(* liczenie liczby wezlow w drzewie *)
let size t =
  fold_tree (fun _ l -> List.fold_left (fun a x -> a + x) 1 l) t;;

(* czy drzewo jest zrownowazone - jego liscie sa na tej samej glebokosci *)
let zrownowazone t =
  fold_tree (fun _ l ->
      match l with
      | [] -> (true, 0)
      | [(b, d)] -> (b, d + 1)
      | (b, d)::tl -> let (rb, rd) =
                        List.fold_left (fun (ab, ad) (b, d) ->
                            (ab && b && (ad = d), max ad d)) (b, d) tl
        in
        (rb, rd+1))      
     t;;

(* odpowiednik funkcji map dla drzewa *)
let map_tree t f =
  fold_tree (fun x l -> Node(f x, l)) t;;
  
(* przechodzenie drzewa w kolejnosci preorder *)
(* najpierw wierzcholek pozniej od lewej do prawej strony *)
let pre_order t =
  (fold_tree (fun x fun_list ->
    fun a -> x::(List.fold_left (fun a f -> f a) a (List.rev fun_list))) t) [];;

(* przechodzenie drzewa w kolejnosci postorder *)
(* napierw synowie od lewej do prawej pozniej wierzcholek *)
let post_order t =
  List.rev ((fold_tree (fun x fun_list ->
    fun a -> x::(List.fold_left (fun a f -> f a) a fun_list )) t) []);;

(* drzewo regularne - wszystkie wierzcholki maja tyle samo dzieci z wyjatkiem lisci *)
let regularne t =
  let r = fold_tree (fun _ l ->
      match l with
      | [] -> None
      | _ -> let deg = List.length l in
        let cb = List.fold_left (fun ab res ->
                                 match res with
                                 | None -> ab
                                 | Some(b, d) -> (ab && b && d = deg))
                 true l in
        Some(cb, deg)
    ) t in
  match r with
  | None -> false
  | Some(b, _) -> b;;


(* deklaracja typu drzewa binarnego *)
type 'a bin_tree = Node of 'a bin_tree * 'a * 'a bin_tree | Leaf;;

(* fold tree dla drzewa binarnego przechodzi drzewo od lisci i przekazuje wynik w gore *)
let rec fold_bin_tree f a t =
  match t with
  | Leaf -> a
  | Node(l, v, r) -> f v (fold_bin_tree f a l) (fold_bin_tree f a r);;

(* liczba wezlow w drzewie binarnym *)
let size_bin t =
  fold_bin_tree (fun _ lr rr -> (lr + rr + 1)) 0 t;;

(* najwieksza glebokosc drzewa binarnego *)
let depth_bin t =
  fold_bin_tree (fun _ lr rr -> (max lr rr) + 1) (-1) t;;

(* srednica drzewa binarnego *)
(* jest to najwieksza doleglosc miedzy dwoma Nodami *)
let srednica_bin t =
  fold_bin_tree (fun _ (ld, ls) (rd, rs) -> ((max ld rd) + 1, max (ld + rd + 2) (max ls rs))) (-1, 0) t |> snd;;

(* sprawdznie czy drzewo jest drzewem bst *)
(* czy w kazdym wezle jest tak, ze wartosci na lewo od niego sa od niego mniejsze, a na prawo wieksze *)
(* przypadek lisci wymagal innego rozpatrzenia wiec funkcja jest rozbudowana *)
let is_bst t =
  let f x (lv, lb) (rv, rb) =
    let b = lb && rb in
    let b, cmin =
      match lv with
      | None -> b, x
      | Some(lmin, lmax) -> ((lmax <= x) && b), lmin
    in
    let b, cmax =
      match rv with
      | None -> b, x
      | Some(rmin, rmax) -> ((rmin >= x) && b), rmax
    in
    (Some(cmin, cmax), b)
  in
  fold_bin_tree f (None, true) t |> snd;;

(* jako wynik z kazdego drzewa dostajemy bool czy dane poddrzewo jest drzewem bst *)
(* oraz dwie funkcje, ktore zwracaja jako wynik np. flmin x zwraca wartosc minimalna *)
(* z lewego poddrzewa i wartosci x, a pozostale analogicznie *)
(* podobnie jako wynik dawana jest funkcja gdyz do min / max jest dawany tylko *)
(* jeden argument, wiec te funkcje "czekaja" na drugi z nich, gdy zostana wywolane "wyzej" *)
let is_bst_fun t =
  let id x = x in 
  fold_bin_tree (fun x (bl, (flmin, flmax)) (br, (frmin, frmax)) ->
      (bl && br && x = flmax x && x = frmin x, (min (flmin x), max (frmax x)))
    )
    (true, (id, id))
    t |> fst;;

(* sprawdzanie czy drzewo binarne jest drzewem avl tzn cyz roznica jego prawej i lewej *)
(* wysokosci jest niemniejsza niz jeden dla kazdego Noda *)
let avl_shaped t =
  fold_bin_tree (fun _ (lb, lh) (rb, rh) ->
      (lb && rb && (abs (lh - rh)) <= 1, (max lh rh) + 1))
    (true, -1)
    t |> fst;;

(* odpowiednik funkcji map dla drzewa binarnego *)
let map_bin_tree f t =
  fold_bin_tree (fun x lt rt -> Node(lt, f x, rt)) Leaf t;;

(* przejscie drzewa binarnego w kolejnosci infiksowej z wykorzystaniem funkcji akumulatora *)
(* ktora staje sie funkcje doklejajaca do ostatecznego akumulatora wodpowiedniej kolejenosci Nody *)
let infix_bin t =
  let id x = x in
  (fold_bin_tree (fun x fl fr ->
      fun a -> fl (x::(fr a)))
    id t) [];;

(* analogicznie jak infix_tree ale zamieniona kolejnosc doklejania *)
let preorder_bin t =
  let id x = x in
  (fold_bin_tree (fun x fl fr ->
      fun a -> x::(fl (fr a)))
    id
    t) [];;

(* analogicznie jak wyzej w kolejnosci postorder - pozniej konieczne odwrocenie listy *)
let postorder_bin t =
  let id x = x in
  (fold_bin_tree (fun x fl fr ->
       fun a -> x::(fr (fl a)))
      id
      t) [] |> List.rev;;

(* znajdowanie w drzewie binarnym najdluzszej sciezki od korzenia do liscia i zapisanie jej do listy *)
let path t =
  let id x = x in 
  ((fold_bin_tree (fun x (dl, fl) (dr, fr) ->
       if dl > dr
       then (dl + 1, fun a -> x::(fl a))
       else (dr + 1, fun a -> x::(fr a))
     )
      (-1, id)
      t
  ) |> snd) [];;

(* zwraca liste list kolejnych poziomow drzewa od gory. Dziala w nastepujacy sposob: *)
(* jako wynik tworzy funkcje sklejajaca poziomy poddrzew *)
(* kluczowe jest to, ze funkcja wynikowa wywoluje sie na glowie i OGONIE akumulatora *)
(* a wiec funkcja bedaca funkca beda funkcja bedaca funkcja ... wykona sie na ogonie *)
(* ogona ogona ogona ... dzieki czemu bedzie to "schodzic" w dol za kazdym razem     *)
(* stosujac jedynie operacje ::  *)
let levels t =
  (fold_bin_tree (fun x fl fr ->
       fun a -> let a = if a = [] then [[]] else a in
                (x::(List.hd a))::(fl (fr (List.tl a))))
      (fun a -> a)
      t
  ) [];;


(* funkcja widoczne sprawdza ile wezlow w drzewie jest widocznych, to znaczy ze na *)
(* drodze od korzenia do danego wezla nie ma wartosci wiekszej niz wartosc w danym *)
(* wezle - jako wynik dostajemy funkcje, ktora po podaniu wartosci zwraca liczbe   *)
(* wezlow widocznych w drzewie podczepionym przy tej wartosci *)
let widoczne t =
  (fold_bin_tree (fun x fl fr ->
      fun v -> if x < v then fl v + fr v else 1 + fl x + fr x)
    (fun _ -> 0) t) (-1);; 


let rec fold_bin_tree f a t =
  match t with
  | Leaf -> a
  | Node(l, x, r) -> f x (fold_bin_tree f a l) (fold_bin_tree f a r);;

let map_bin_tree f t =
  fold_bin_tree (fun x lt rt -> (Node(lt, f x, rt))) Leaf t;;

let rec map_path t d =
  match t with
  | Leaf -> Leaf
  | Node(lt, x, rt) -> let nlt = map_path lt (d+1)
                       and nrt = map_path rt (d+1)
                       in
                       Node(nlt, (x, d), nrt);;


let map_depth t =
  fold_bin_tree (fun x (lt, ld) (rt, rd) ->
    (Node(lt, (x, (1 + (max ld rd))), rt), 1 + (max ld rd))) (Leaf, -1) t;;

let check ((_, u), v) = u = v;;

let srednie t =
  let nt = fst (map_depth (map_path t 0)) in (* najpierw mapuje drzewo wysokosciami i glebokosciami *)
  (fold_bin_tree (fun x fl fr ->
      fun a -> if check x then (fst (fst x))::(fl (fr a)) else fl (fr a)
     ) (fun a -> a) nt) [];;


let sadzwka a (jy, jx) =
  let n = Array.length a in
  let m = Array.length a.(0) in
  let max_czas = ref (-1) in
  let czas = Hashtbl.create (n*m) in
  for i = 0 to n-1 do
    for j = 0 to m-1 do
      Hashtbl.add czas a.(i).(j) (i,j);
      max_czas := max !max_czas a.(i).(j);
    done;
  done;
  let moze_uciec pekniete =
    if pekniete.(jx).(jy) = true then false else(
      let q = Stack.create () in
      let visited = Array.make_matrix n m false in
      visited.(jy).(jx) <- true;
      Stack.push (jx, jy) q;
      let res = ref false in
      while not (Stack.is_empty q) && not !res do
        (
          let (lx, ly) = Stack.pop q in
          if lx = 0 || lx = n-1 || ly = 0 || ly = m-1 then res := true else
            (
              let visit_new x y =
                if x > -1 && x < n && y > -1 && y < m && not visited.(x).(y) && not pekniete.(x).(y) then Stack.push (x, y) q; visited.(x).(y) <- true;
              in
              visit_new (lx+1) ly;
              visit_new (lx-1) ly;
              visit_new lx (ly+1);
              visit_new lx (ly-1);
            )
        )
      done;
      !res
    )
  in
  let pekniety = Array.make_matrix n m false in
  let wynik = ref (-1) in
  for t = 1 to !max_czas do(
    if !wynik = -1 then(
      if Hashtbl.mem czas t then (
        let nowe = Hashtbl.find_all czas t in
        List.iter (fun (x, y) -> pekniety.(x).(y) <- true) nowe;
        if not (moze_uciec pekniety) then wynik := t;
      )
    )
  )
  done;
  !wynik;;


let przebuduj t =
  let rec buduj t szer =
    match t with
    | Leaf -> Leaf
    | Node (lt, _, rt) ->
       let nl = buduj lt (szer-1) in
       let nr = buduj rt (szer+1) in
       Node(nl, szer, nr)
  in
  buduj t 0;;

let szerokosc t =
  let t = przebuduj t in
  let (mn, mx) = fold_bin_tree (fun v (lmn, lmx) (rmn, rmx) -> (min (min lmn rmn) v, max (max lmx rmx) v)) (max_int, min_int) t in
  (mx-mn);;

let t = Node(Node(Node(Node(Leaf, 3, Leaf), 4, Node(Node(Node(Node(Leaf, -2, Leaf), -1, Leaf), 0, Leaf), 2, Node(Leaf, 1, Leaf))), 5, Leaf), 6, Node(Node(Leaf, 8, Leaf), 7, Node(Node(Leaf, 10, Leaf), 9, Leaf)));;

let _ = szerokosc t;;

                                
