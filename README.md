# Introduction to Programming at Uniwersity of Warsaw

Basic programs written for introduction to (functional) programming in OCaml at first year of Computer Science Studies at University of Warsaw.

## Projects

Six projects on differnt topics to improve data structures and algorithms knowledge of students titled:
1. Arytmetyka (arithemtics of intervals)
2. Leftist (leftist data structure implementation)
3. iSet (tree of intervals data structure implementation)
4. Topol (topological sort)
5. Origami (functionl programming practice)
6. Przelewanka (bactracking problem)

## Excercises

Excersises from the activities at Warsaw University and written solutions to some exams proglem to practices before the exams