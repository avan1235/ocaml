(****************************************************************************************)
(******************* REPREZENTACJA KOLEJKI JAKO DRZEWA LEWICOWEGO ***********************)
(****************************************************************************************)

type 'a queue =
  | Node of 'a queue * 'a * int * 'a queue
  | Leaf


(****************************************************************************************)
(********************************* DEKLARACJA WYJATKOW **********************************)
(****************************************************************************************)

(* Wyjatek wznoszony w momencie proby operacji na pustej kolejce                        *)
exception Empty

(* Wyjatek wzoszony gdy sprawdzane lista i kolejka priorytetowa roznia sie              *)
exception Different_values

(****************************************************************************************)
(************************************* KONTRUKTORY **************************************)
(****************************************************************************************)

(* Tworzenie  kolejki z jedna wartoscia w korzeniu - prawa_wysokosc rowna 1             *)
let new_single v =
  Node(Leaf, v, 1, Leaf)                  

(* Tworzenie pustej kolejki                                                             *)
let empty = Leaf


(****************************************************************************************)
(********************************** FUNKCJE POMOCNICZE **********************************)
(****************************************************************************************)

(* Dla danego wezla zwraca jego prawa wysokosc                                          *)
let height t =
  match t with
  | Leaf -> 0
  | Node(_, _, h, _) -> h
  

(****************************************************************************************)
(********************************* OPERACJE NA KOLEJCE **********************************)
(****************************************************************************************)

(* Operacja laczenia kolejek q1 q2 poprzez rekurencyjne wywolywanie funkcji join        *)
(* Przyjete oznaczenia: lq, rq - odpowiednio lewei prawe poddrzewo                      *)
(*                      v - wartosc w danym wezle drzewa                                *)
(*                      h - prawa wysokosc w danym wezle drzewa                         *)
let rec join (q1 : 'a queue) (q2 : 'a queue) =
  match q1, q2 with
  | Leaf, q -> q
  | q, Leaf -> q
  | Node(lq1, v1, _, rq1), Node(_, v2, _, _) -> if v1 <= v2
  (* wartosc w 1 poddrzewie jest mniejsza *)    then(
  (* wiec tworzymy nowe drzewo zawieszone *)      let new_q = join rq1 q2 in
  (* w danej wartosci i rekurencyjnie     *)      let h_new = height new_q
  (* tworzymy podrzewa, jednak przed ich  *)      and h_left = height lq1 in
  (* przylaczeniem porownujemy ich prawe  *)      if  h_new < h_left
  (* wysokosci tak by zawsze ta mniejsza  *)      then Node(lq1,   v1, h_new + 1, new_q)
  (* byla po prawej stronie nowego drzewa *)      else Node(new_q, v1, h_left + 1,  lq1)
  (* ------------------------------------ *)    )
  (* zamieniamy poddrzewa w przeciwnym    *)    else join q2 q1
  (* wypadku (gdy v1 > v2)                *)

(* Dodawanie wartosci v do kolejki q poprzez utworzenie pojedynczej kolejki i polacznie *)
(* starej kolejki z nowa                                                                *)
let add (v : 'a) (q : 'a queue) =
  let new_q = new_single v in
  join q new_q 

(* Otrzymanie z kolejki q najmniejszej wartosci z korzenia drzewa i polaczenie jego     *)
(* poddrzew - zwraca pare (najmniejszy_element, nowa_kolejka)                           *)
let delete_min (q : 'a queue) =
  match q with
  | Leaf -> raise Empty                     (* wznoszony wyjatek gdy kolejka jest pusta *)
  | Node(lq, v, _, rq) -> let new_q = join lq rq in
                          (v, new_q)


let get_min (q : 'a queue) =
  fst (delete_min q)


let pop_min (q : 'a queue) =
  snd (delete_min q)

(* Sprawdzenie czy kolejka jest pusta, czyli drzewo sklada sie z samego liscia          *)
let is_empty (q : 'a queue) =
  match q with
  | Leaf -> true
  | _ -> false


(* znalezienie w grafie liczby wierzcholkow zdominowanych                        *)
(* wierzcholek jest zdominowany, gdy istnieje inny ktory go dominuje             *)
(* wierzcholek v dominuje w jezeli dla kazdego wierzcholka u, z ktorego istnieje *)
(* sciezka do w, istniej sciezka z v do u                                        *)
let zdominowani e =
  let n = Array.length e in
  let indeg v =
    let wyn = ref 0 in
    for u = 0 to n-1
    do
      if e.(u).(v) then incr wyn
    done;
    !wyn
  in
  let visited = Array.make n 0 in
  let rec dfs v =
    if visited.(v) < 2 then
      begin
        visited.(v) <- visited.(v) + 1;
        for u = 0 to n-1
        do
          if e.(v).(u) then dfs u
        done;
      end;
  in
  for v = 0 to n-1
  do
    if indeg v = 0 then dfs v
  done;
  Array.fold_left (fun a x -> if x <= 1 then a+1 else a) 0 visited;;


(* Dana jest tablica (int, bool) array array ktora opisuje uksztalowanie terenu *)
(* na ktorego czesci znajduje sie miasto. Opisuje ona wysokosci oraz to, czy    *)
(* teren przynalezy do miasta. Ile pomp potrzeba by odpompowac zalane miasto?   *)
(* Gdyby miasto bylo na calym terenie to musielibysmy postawic pompe na         *)
(* punkcie i zobaczyc co odpompuje i brac kolejne takie punkty, ktore nie       *)
(* zostaly jeszcze odpomopwane. Natomiast w przypadku, kiedy do miasta nalezy   *)
(* tylko czesc terenu, to minimum wybieramy tylko z miasta, poniewaz dziury     *)
(* poza miastem moga zostac zalane                                              *)
let powodz teren =
  let m = Array.length teren in
  let n = Array.length teren.(0) in
  let l = ref [] in
  for i = 0 to m-1 do
    for j = 0 to n-1 do
      if snd (teren.(i).(j)) then l := (fst teren.(i).(j), (i,j))::!l
    done;
  done;
  l := List.sort compare !l;
  let spompowane = Array.make_matrix m n false in
  let kolejka = ref empty in
  let pompy = ref [] in
  while !l <> [] do
    if is_empty !kolejka || fst (get_min !kolejka) > fst (List.hd !l) then
      begin
        let (wys, (i, j)) = List.hd !l in
        l := List.tl !l;
        if not (spompowane.(i).(j)) then
          begin
            pompy := (i,j)::!pompy;
            kolejka := add (wys, (i, j)) !kolejka
          end
      end
    else
      begin
        let (wys, (i,j)) = get_min !kolejka in
        kolejka := pop_min !kolejka;
        if not (spompowane.(i).(j)) then
          begin
            spompowane.(i).(j) <- true;
            let splywaj i' j' wys =
              if not (snd teren.(i').(j')) || fst (teren.(i').(j')) >= wys
              then kolejka := add ((max wys (fst (teren.(i').(j')))), (i',j')) !kolejka
            in
            if i > 0 then splywaj (i-1) j wys;
            if i < m-1 then splywaj (i+1) j wys;
            if j > 0 then splywaj i (j-1) wys;
            if j < n-1 then splywaj i (j+1) wys;
          end
      end
  done;
  !pompy;;
