(* nalezy znalezc sciezka dla skoczka na szachownicy w taki sposob, by wszystkie pola zostaly *)
(* zajete z wykorzystaniem metody bactrackingu (m i n to wymiasry szachownicy)                *)

let print_matrix a =
  let x = Array.length a in
  let y = Array.length a.(0) in
  for i = 0 to x-1 do
    print_string "\n| ";
    for j = 0 to y-1 do
      if a.(i).(j) < 10 then print_string " ";
      print_int a.(i).(j);
      print_string " | ";
    done;
  done;;

let is_final a =
  let x = Array.length a in
  let y = Array.length a.(0) in
  let res = ref true in
  for i = 0 to x-1 do
    for j = 0 to y-1 do
      res := !res && (a.(i).(j) > -1)
    done;
  done;
  !res

let skoczek m n =
  let d = [|(2,1);(2,-1);(-2,1);(-2,-1);(1,2);(1,-2);(-1,2);(-1,-2)|] in
  let a = Array.make_matrix m n (-1) in
  let rec bt x y k = (* k - numer ruchu *)
    if x >= 0 && x < m && y >= 0 && y < n then
      begin
        if k = m*n && is_final a then failwith "Yeah"
        else
          begin
            if a.(x).(y) < 0 then
              begin
                a.(x).(y) <- k;
                Array.iter (fun (dx, dy) -> bt (x+dx) (y+dy) (k+1)) d;
                a.(x).(y) <- (-1);
              end;
          end
        end
  in
  try
    (bt 0 0 0);
    a
  with _ -> a;;


print_matrix (skoczek 8 8);;


(* problem wydanie reszty n z wykorzystaniem monet zawartych na liscie l - najmniejsza liczba monet *)
(* rozwiazanie wykorzystujace programowanie dynamicze z techniką spamietywania                      *)
let reszta n l =
  let a = Array.init (n+1) (fun i -> if i = 0 then 0 else n+1) in
  List.iter (fun m ->
      for cur_sum = m to n-1 do a.(cur_sum) <- min a.(cur_sum) (a.(cur_sum-m)+1) done) l;
  a.(n);;


(* problem analogiczny wyszukujacy liczby sposobow wydania danej reszty za pomoca dnego zestawu monet *)
let sposobny n l =
  let a = Array.init (n+1) (fun i -> if i = 0 then 1 else 0) in
  List.iter (fun m ->
      for cur_sum = m to n-1 do a.(cur_sum) <- a.(cur_sum) + a.(cur_sum - m) done) l;
  a.(n);;


let klej l =
  let x = Array.of_list l in
  let n = Array.length x in
  let s = Array.make (n+1) 0 in (* tablica prefiksow dlugosci poszczegolnych czesci kija*)
  for i = 1 to n do
    s.(i) <- s.(i-1) + x.(i-1);
  done;
  let dl a b = s.(b+1) - s.(a) in
  let m = Array.make_matrix n n max_int in
  for a = 0 to n-1 do m.(a).(a) <- 0 done;
  for d = 1 to n-1 do (* rozwazamy rozne dlugosci kija d *)
    for a = 0 to n-d-1 do
      let b = a+d in
      for i = a to b-1 do
        m.(a).(b) <- min m.(a).(b) (m.(a).(i) + m.(i+1).(b) + max (dl a i) (dl (i+1) b));
      done;
    done;
  done;
  m.(0).(n-1);;


(* zadanie kino: o kazdym filmie wiadomo kiedy sie konczy i kiedy zaczyna - lista par (p,k) *)
(* chcemy obejrzec jak najwiecej seansow, takich ktore nie nachodza na siebie i sie nie     *)
(* stykaja ze soba - zwrocic jak najwieksza liczbe seansow                                  *)
let kinoman l  =
  (* obserwacja - zawsze mozemy isc na seans ktory sie najwczesniej konczy                  *)
  List.sort (fun (_, k1) (_, k2) -> compare k1 k2) l |>
    List.fold_left (fun acc (p, k) ->
        match acc with
        | [] -> (p, k)::acc
        | (_, k')::_ -> if k' < p then (p, k)::acc else acc
      ) [] |>
    List.rev;;



(* zadanie firma: firma x podjela sie zrobienia zadan (nazwa,  liczba_dni, kara_za_dzien_zwloki) *)
(* dostajemy liste takich zobowizan w czasie, w ktorym zaczyna sie juz czas kary                 *)
(* kolejnosc czynosci, by kara byla jak najmniejsza                                              *)
(*dwa zlecenia (a, d_a, k_a) i (b, d_b, k_b) -> czy po czasie t mamy wykonac najpierw a pozniej b*)
(* czy odwrotnie                                                                                 *)
(* (t+d_a)*k_a + (t+d_a+d_b)*k_b <?> (t+d_b)*k_b + (t+d_b+d_a)*k_a                               *)
(*                       d_a*k_b <?> d_b*k_a                                                     *)
(*                       d_a/k_a <?> d_b/k_b                                                     *)
let firma l =
  List.sort (fun (_, da, ka) (_, db, kb) -> compare (da*kb) (db*ka)) l |>
    List.map (fun (u, _, _) -> u);;

(* zadanie wieze: mamy szachownice nxn rozstawic n wiez tak, by sie nie szachowaly              *)
(* mamy jednak podane ograniczenia w postaci prostokatow, na ktorych mozemy postawic dane wieze *)
(* obserwacja - mozemy rozwazac rzuty prostokatow na osie x i y i szukamy takich liczb w danych *)
(* przedzialach tak zeby kazdemu przedzialowi przyporzadkowac liczbe od 1 do n w taki sposob ze *)
(* {r1,..., rn}={1,...,n}                                                                       *)
let wieze w = (* ((int*int)*(int*int)) array *)
  let n = Array.length w in
  let repr prz = (* (int*in) array -> int array zwraca tablice reprezentantow                    *)
    let kubelki = Array.make n [] in
    for i = 0 to n-1 do
      let pocz = fst prz.(i) in
      kubelki.(pocz) <- (snd prz.(i), i)::kubelki.(pocz)
    done;
    let open Batteries.Heap in
    let r = Array.make n 0 in
    let wrzuc l q =
      List.fold_left (fun q e -> insert q e) q l
    in
    let q = ref empty in
    for p = 0 to n-1 do
      q := wrzuc kubelki.(p) !q;
      let (k, indx) = find_min !q in
      if k < p then failwith "no way" else
        (
          r.(i) <- p;
          q := del_min !q
        )
    done;
    r
  in
  try
    let x = Array.map fst w |> repr in
    let y = Array.map snd w |> repr in
    Array.map2 (fun x y -> (x,y)) x y
  with _ -> Array.make 0 (0, 0);;


(* zadanie most: most podzielona na odcinki, z ktorych kazdy ma swoja wytrzymalosc              *)
(* most mozemy dzielic podporami, wytrzymaloscia jest minimum wytrzymalosci na danym przedziale *)
(* miedzy przeslami                                                                             *)
(* przez most przejezdza ciezarka o ciezaze d mniejszym od kazdej z wytrzymalosci               *)
(* zachowuja one miedzy soba odstepy o odleglosci c                                             *)
(* dla zadanej liczby podpor k (most dzielimy na k+1 czesci) jaka jest minimalna odleglosc c    *)
(* miedzy ciezarowkami zeby most sie nie zawalil                                                *)
(* wyszukiwanie binarne po c, zachlannie dla zadaanego c ile minimalnie podpor trzeba *)
